<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['middleware' => 'isAppAuthorized'], function () {

    // Authentications

Route::post('/auth/login','Api\LoginController@login'); // Login Verification
Route::post('/auth/register','Api\RegisterController@register'); // Register Verification

    // Get Eula
Route::get('/eula','Api\MiscController@getEula');
    
   // Forgot Password
Route::post('/forgot/password','Api\ForgotPasswordController@sendForgotMail');


// Minimum Shopping & Delivery Amount
Route::get('/min_amnt','Api\MiscController@getMinAmnt');





// Cities 
Route::get('/cities','Api\CityController@allCities'); // Get all Cities
Route::get('/cities/{id}','Api\CityController@cityById'); // Get City By Id 

// Suburbs
Route::get('/suburbs','Api\SuburbController@allsuburbs'); // Get all Suburbs
Route::get('/suburbs/{id}','Api\SuburbController@suburbById'); // Get Suburb By Id
Route::get('/suburbs/city/{id}','Api\SuburbController@suburbByCityId'); // Get all Suburbs By City Id

// User Profile & Password
Route::get('/profile/{id}','Api\ProfileController@profile'); // Get all profile Information
Route::post('/profile/update','Api\ProfileController@updateProfile'); // Updates The Profile
Route::post('/profile/update/password','Api\ProfileController@updatePassword'); // Updated Profile Passwords
// Products 

Route::get('/products','Api\ProductController@products');

Route::get('/products/{id}','Api\ProductController@productById');
Route::get('/products/category/{id}','Api\ProductController@productByCatId');

// Categories   

Route::get('/categories','Api\CategoryController@categories');
Route::get('/categories/{id}','Api\CategoryController@categoryById');
Route::get('/category/products/{id}','Api\CategoryController@categoryProductsById');

// Selling Units

Route::get('/selling_units','Api\SellingUnitController@getAll');
Route::get('/selling_units/{id}','Api\SellingUnitController@getById');

// Service Options

Route::post('/service_types','Api\ServiceTypeController@getByIds');

// Sliders / Gallery Images

Route::get('/gallery','Api\SliderController@get_all_sliders');

// Checkout 

Route::post('/checkout','Api\CheckoutController@checkout');





// FOR OLDER VERSION OF ANDROID APP PREMIUMMEAT.CO.NZ
Route::get('/new/products','Api\ProductController@products_new');
Route::get('/new/products/category/{id}','Api\ProductController@productByCatIdNew');


});

