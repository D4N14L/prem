<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceType extends Model
{
    protected $table = "service_type";

    public function getCategory()
    {
      return \App\ProductCategory::where('id','=',$this->cat_id)->first();
    }
    
    public function services()
    {
        return $this->belongsTo(ServiceType::class,'groups_services','service_id','group_id');
    }

  }

