<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
  protected $table = "orders";


  public function User()
  {
    return \App\User::where('id','=',$this->customer_id)->first();
  }

  public function OrderInfo()
  {
    return \App\OrderInfo::where('order_id','=',$this->id)->get();
  }

  public function getTimeSlot()
  {
    return \App\TimeSlot::where('id','=',$this->timeslotsSelection)->first();
  }












}
