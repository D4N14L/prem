<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use PDF;
class OrderCharged extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;


    public $customer;
    public $pdf;
    public $path;
    public $meta_attachment;
    public $isdelivery;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer,$order_id)
    {
        $pdf_name = now().'-'.$customer->email.'.'.'pdf';
        $this->customer = $customer;
        $this->path = public_path().'/invoices/'.$pdf_name;
        $this->isdelivery = \App\Order::find($order_id)->isDelivery == 1;
        $this->pdf = PDF::loadview('orders.invoice',compact('customer','order_id'))->save($this->path);
        $this->meta_attachment = [
          'as' => $pdf_name,
          'mime' => 'application/pdf'
        ];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return $this->view('mail.OrderCharged')
               ->subject('Your Order Charged Successfully & Dispatched')
               ->from('info@premiummeat.co.nz','Premium Meats')
               ->attach($this->path,$this->meta_attachment)
               ->with('user',$this->customer)
               ->with('is_delivery',$this->isdelivery);

    }
}
