<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    public $body;
    public $custom_subject;
    public $attachment;
    /**
     * 
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$body,$subject,$attachment = 0)
    {
        $this->user = $user;
        $this->body = $body;
        $this->custom_subject = $subject;
        $this->attachment = $attachment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->attachment == 0){
            return $this->view('mail.custom')->subject($this->custom_subject)->with([
                'user' => $this->user,
                'body' => $this->body
            ]);
        }else 
        {
            return $this->view('mail.custom')
                    ->subject($this->custom_subject)
                    ->attach($this->attachment,[
                        'as' => 'Invoice',
                        'mime' => 'pdf'
                    ])->with([
                        'user' => $this->user,
                        'body' => $this->body
                        ]);
        }
    }
}
