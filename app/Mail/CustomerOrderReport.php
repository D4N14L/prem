<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Order;
use Voyager;
use App\User;
use App\Suburb;

class CustomerOrderReport extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $customer;
    public $order_id;
    public $logo;
    public $customerSubrub;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($id)
    {
      $order = Order::where('id','=',$id)->first();
      $this->customer = User::where('id','=',$order->customer_id	)->first();
      $this->order_id = $id;
      $admin_logo_img = Voyager::setting('site.logo');
      $extension = pathinfo($admin_logo_img,PATHINFO_EXTENSION);
      $data = file_get_contents(public_path().'/storage/'.$admin_logo_img);
      $this->logo = 'data:image/'.$extension.';base64,'.base64_encode($data);
      $this->customerSubrub = Suburb::where('id','=',$this->customer->suburb_id)->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.orderReport');
    }
}
