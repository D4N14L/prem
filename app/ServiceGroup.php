<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceGroup extends Model
{
    protected $table = "service_groups";
    protected $guarded = [];
    public function services()
    {
        return $this->hasMany(ServiceType::class,'groups_services','group_id','service_id');
    }
}
