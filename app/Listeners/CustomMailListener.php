<?php

namespace App\Listeners;
use App\Events\CustomMail;
use App\Mail\CustomMail as CustomMailMailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
class CustomMailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CustomMail $event)
    {
        Mail::to($event->user->email)->queue(new CustomMailMailer($event->user,
                                                                $event->body,
                                                                $event->custom_subject,
                                                                $event->attachment ));
    }
}
