<?php

namespace App\Listeners;

use App\Events\SendOrderReportMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Mail\CustomerOrderReport;
use Mail;
class SendOrderReportMailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  SendOrderReportMail  $event
     * @return void
     */
    public function handle(SendOrderReportMail $event)
    {
      Mail::to($event->customer_email)->queue(new CustomerOrderReport($event->order_id));
    }
}
