<?php

namespace App\Listeners;

use App\Events\SendVerficationMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Mail;
use App\Mail\RegisterVerification;
class SendVerficationMailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendVerficationMail  $event
     * @return void
     */
    public function handle(SendVerficationMail $event)
    {
    Mail::to($event->user->email)->queue(new RegisterVerification($event->user));
    }
}
