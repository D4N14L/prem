<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendOrderReportMail
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $order_id;
    public $customer_email;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($order_id,$customer_mail)
    {
        $this->order_id =  $order_id;
        $this->customer_email = $customer_mail;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
