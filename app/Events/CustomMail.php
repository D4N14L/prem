<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CustomMail
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $user;
    public $body;
    public $custom_subject;
    public $attachment;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user,$body,$subject,$attachment = NULL)
    {
        $this->user = $user;
        $this->body = $body;
        $this->custom_subject = $subject;
        $this->$attachment = $attachment;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
