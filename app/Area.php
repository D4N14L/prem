<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = "areas";
    protected $fillable = ['name','id'];

    public function suburbs(){
      return $this->hasMany(\App\Suburb::class,'areas_id');
    }
}
