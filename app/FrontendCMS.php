<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FrontendCMS extends Model
{
  protected $table = "frontend_cms";
}
