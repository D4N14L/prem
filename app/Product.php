<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ProductCategory;
use App\SellingUnit;
use App\Inventory;
class Product extends Model
{
    protected $table = "products";
    public $timestamps = false;
    public function category()
    {
      return $this->belongsTo(ProductCategory::class,'cat_id');
    }

    public function sellingUnit()
    {
      return $this->belongsTo(SellingUnit::class,'sellingUnit_id');
    }

    public function timeslot()
    {
      return \App\TimeSlot::where('id','=',$this->timeslot)->first();
    }

    public function inventory()
    {
      return $this->hasOne(Inventory::class,'product_id');
    }

    public function outofstock()
    {
      if($this->attributes['is_available'] == 0)
      {
          $this->attributes['name'] = $this->attributes['name'].' '.'(Out of stock)';
      }
      return $this;
    }

}
