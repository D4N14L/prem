<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceGroupOptions extends Model
{
    protected $table = "groups_services";
    protected $guarded = [];
    public $timestamps = false;
}
