<?php

namespace App\Http\Middleware;

use Closure;

class AppTokenAuthorized
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        return $next($request);
            if(isset(getallheaders()['App-Token']) && getallheaders()['App-Token']=='$2y$10$jlaLIu5hBcep.DsojgOPBuZCxSwg22KRYNWFkrQEHK7UF4E2b2xq2') {
        return $next($request);                                                     
    }else{
        return response()->json(['status' => 400,'message' => "Invalid Request"]);
    }
    }
}
