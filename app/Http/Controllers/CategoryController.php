<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory;
use URL;
use Toastr;
use File;
class CategoryController extends Controller
{
  public function create()
  {
    $category = ProductCategory::orderBy('arrange','asc')->paginate(100);

    return view('admin.category.create')->withCategory($category)
                                        ->with(['message' => "Viewing Top 10 Categories", 'alert-type' => 'success']);
  }

  public function store(Request $request)
  {

    $isEdit = $request->isEdit;
    try {
      if($isEdit == "0")
      {

        $category = new ProductCategory();
        $category->timestamps = false;
        $category->name = $request->name;
        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        if($image->getClientOriginalExtension() != "svg")
        {
          $newImage = \Image::make($request->file('image'));
          $newImage->resize(1366,850);  
          $category->image = $imageName;
          $newImage->save(public_path('assets/category'.$imageName),10);  
        }else 
        {
          $category->image = $imageName;
          $image->move(public_path('/assets/category/'),$imageName);
        }
        $category->save();
      }
      else
      {

        $category = ProductCategory::findOrFail($request->productId);
        $category->timestamps = false;
        $category->name = $request->name;
        if($request->has('image'))
        {

          $image = $request->file('image');
          $imageName = time().'.'.$image->getClientOriginalExtension();
          $category->image = $imageName;
          if($image->getClientOriginalExtension() != "svg")
          {
            $newImage = \Image::make($request->file('image'));
            $newImage->resize(1366,850);  
            $category->image = $imageName;
            $newImage->save(public_path('assets/category/'.$imageName),10);  
          }else           {
            $category->image = $imageName;
            $image->move(public_path('/assets/category/'),$imageName);
            }  
        }
        $category->save();

      }
      return redirect()->back()->with(['message' => "Category Created Succesfully", 'alert-type' => 'success']);

    } catch (\Exception $e) {
      Toastr::error("There was a problem while creating or editing this category");
      return redirect()->back()->with(['message' => "Failed To Add Category, System Error ", 'alert-type' => 'error']);
    }

  }



  public function delete($id)
  {

    try{
      $category = ProductCategory::findOrFail($id);
      $product = \App\Product::where('cat_id','=',$category->id)->delete();
      $category->delete();
      return redirect()->back()->with(['message' => "Deleted Successfully", 'alert-type' => 'success']);  
    }catch(\Exception $e){
      return redirect()->back()->with(['message' => "Failed To Delete", 'alert-type' => 'error']);  
    }

    // try{
        //
        // }
        // catch(\Exception $e)
        // {
        //   Toastr::error('Category Deleted Successfully!');
        //   session()->flash('failed','Category Deleted Successfully!');
        //   return redirect()->back();
        //
        // }

  }

  public function managePosition(Request $request)
  {
    
    try {
      foreach ($request->positions as $value) { 
        \App\ProductCategory::where('id',$value[0])->update(['arrange' => $value[1]]);
      }
        return response()->json(['status' => '200']);
    } catch (\Exception $e) {
      return response()->json(['status' => '500']);
    }


  }








}
