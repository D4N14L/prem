<?php

namespace App\Http\Controllers;

use App\Mail\CustomerOrderReport;
use App\Events\SendOrderReportMail;
use App\Mail\OrderCharged;
use Illuminate\Http\Request;
use Cartalyst\Stripe\Stripe;
use App\OrderInfo;
use App\Order;
use App\Suburb;
use Voyager;
use Toastr;
use Mail;
use PDF;
class OrderController extends Controller
{
    // List All Of The Orders

    public function index()
    {
      $order = Order::where('order_status','=',0)->where('isDiscard','=',0)->orderBy('id','desc')->paginate(20);
      return view('admin.order.index')->with('order',$order);
    }

    // list charged orders

     public function chargedOrders(){

      $orders = Order::where('order_status','=',1)->orderBy('id','desc')->latest();
      $sum = 0;
      foreach($orders->get() as $order)
      {
        if($order->delivery_charges == 0)
        {
          $sum += $order->total_actual_amount + 5; 
        }
        else 
        {
          $sum += $order->total_actual_amount + $order->delivery_charges;
        }
        
      }
 
  
      $orders = $orders->paginate(100);
      return view('admin.order.charged')
             ->with('order',$orders)
             ->with('sum',$sum);
    }

    // list discarded orders
    public function discardedOrders(){
      $order = Order::where('isDiscard','=',1)->orderBy('id','desc')->latest()->paginate(20);
      return view('admin.order.discard')->with('order',$order);
    }



    // list Orders Details

    public function details($id)
    {
      $Order = Order::findOrFail($id);
      $Order->isRead = 1;
      $Order->save();
      $OrderInfo = OrderInfo::where('order_id','=',$id)->get();
      return view('admin.order.detail')->with('OrderInfo',$OrderInfo);
    }

    // list Orders Mark Up

    public function markStatus($id)
    {
      $order = Order::findOrFail($id);
      $order->order_status = 1;
      $order->save();
      return redirect()->back();
    }


    // Create Customer Charge

     public function customerCharge($customer_stripe_token,$amount,$order_id,$customer_email)
    {

      // $invoice = PDF::loadview('orders.invoice',compact('customer','order_id'))->output();

      // sk_live_1ahfqHGkgIegMmSvBcSR379x
      try {
        $stripe = new Stripe(env('STRIPE_SK'));
        $charge = $stripe->charges()->create([
            'customer' => $customer_stripe_token,
            'currency' => 'NZD',
            'amount' => (float) $amount
        ]);
      $customer = \App\User::where('email',$customer_email)->first();
      // $invoice = PDF::loadview('orders.invoice',compact('customer'))->output();
      Mail::to($customer)->send(new OrderCharged($customer,$order_id));
        if($charge['status'] == "succeeded"){
          // Send Order Report To Customer
          // Update Order Status To `Charged`
          $order = Order::findOrFail($order_id);
          $order->order_status = 1;
          $order->save();
          session()->flash('success','Amount '.$amount.' Has been charged Successfully from customer!');
          return redirect()->back();
        }
      } catch (\Exception $e) {
          session()->flash('failed',$e->getMessage());
          return redirect()->back();
      }

    }

    // Manage Orders Details

    public function manageDetails(Request $request)
    {
        try {
          $OrderInfo = OrderInfo::findOrFail($request->order_detail_id);
          $OrderInfo->actual_quantity = (float) $request->actualQuantity;
          $OrderInfo->extra_quantity  =  (float) $request->extraQuantity;
          $OrderInfo->extra_payment   =   (float) $request->totalExtraAmount;
          $OrderInfo->save();
          return response()->json(['status' => 200]);
        } catch (\Exception $e) {
          return response()->json(['status' => 500]);
        }

    }

    public function manageAmountToBePaidByTheUser(Request $request){
      try {
        $order = Order::findOrFail($request->order_id);
        $order->total_actual_amount = $request->totalActualAmount;
        $order->save();
        return response()->json(['status' => 200]);
      } catch (\Exception $e) {
        return response()->json(['status' => 500]);
      }

    }

    // Create Charged Details

    public function chargedDetails($id)
    {
      $OrderInfo = OrderInfo::where('order_id','=',$id)->get();
      return view('admin.order.after_charged_details')->with('OrderInfo',$OrderInfo);
    }

    // Generate Reports

    public function generateReport($id)
    {
        $order = Order::where('id','=',$id)->first();
        $customer = \App\User::where('id','=',$order->customer_id	)->first();
        $admin_logo_img = Voyager::setting('site.logo');
        $extension = pathinfo($admin_logo_img,PATHINFO_EXTENSION);
        $data = file_get_contents(public_path().'/storage/'.$admin_logo_img);
        $logo = 'data:image/'.$extension.';base64,'.base64_encode($data);
        $customerSubrub = \App\Suburb::where('id','=',$customer->suburb_id)->first();
        $html = view('admin.order.report')->with('customer',$customer)->with('order_id',$id)->with('logo',$logo)->with('customerSubrub',$customerSubrub)->render();
        return PDF::load($html)->filename($customer->name.'.'.'pdf')->download();
    }

    // Print view
    public function printReport($id)
    {
      $order = Order::where('id','=',$id)->first();
      $customer = \App\User::where('id','=',$order->customer_id	)->first();
      $admin_logo_img = Voyager::setting('site.logo');
      $extension = pathinfo($admin_logo_img,PATHINFO_EXTENSION);
      $data = file_get_contents(public_path().'/storage/'.$admin_logo_img);
      $logo = 'data:image/'.$extension.';base64,'.base64_encode($data);
      $customerSubrub = \App\Suburb::where('id','=',$customer->suburb_id)->first();
      return view('admin.order.print')->with('customer',$customer)->with('order_id',$id)->with('logo',$logo)->with('customerSubrub',$customerSubrub)->render();

    }

    // Discard Order
    public function discardOrder($id){
      $order = Order::findOrFail($id);
      $order->isDiscard = 1;
      $order->save();
      return redirect()->back();
    }

    public function addServiceCharges(Request $request)
    {
      try {
        $order = Order::findOrFail($request->order_id);
        $order->service_charges = (float) $request->service_charges;
        $order->save();
        return response()->json(['status' => 200]);
      } catch (\Throwable $th) {
        return response()->json(['status' => 500]);
      }
    }

    public function addAdminDescription(Request $request)
    {
      try {
        $order = Order::findOrFail($request->order_id);
        $order->service_description = $request->description;
        $order->save();
        return response()->json(['status' => 200]);
      } catch (\Throwable $th) {
        return response()->json(['status' => 500]);
      }
    }



    // Prepare Order
    public function prepareIndex(Request $request)
    {
      




        $pluck = [
          "users.name as customer_name",
          "users.email as customer_email",
          "users.landline as customer_landline",
          "products.name as product_name",
          "orders_info.quantity as quantity",
          "products.price as product_price",
          "products.avg_weight as product_average_weight",
          "SellingUnits.name as selling_unit",
          "orders.order_date as order_date",
          "orders_info.service_type_option as service_type_options",
          "orders_info.additional_information as additional_information",

        ];

        if(!($request->has('prepartion_by') && $request->has('order_date')))
        {
          $new_orders  = Order::
                          where('order_status','=',0)
                         ->where('isDiscard','=',0)
                         ->orderBy('orders.id','desc')
                         ->join('orders_info','orders.id','=','orders_info.order_id')
                         ->join('products','orders_info.product_id','=','products.id')
                         ->join('users','orders.customer_id','=','users.id')
                         ->join('SellingUnits','products.sellingUnit_id','SellingUnits.id')
                         ->select($pluck)
                         ->get()
                         ->toArray();

        }
        else if ($request->prepartion_by != "all" && $request->order_date == "null")
        {
          $new_orders  = Order::where('order_status','=',0)
                        ->where('isDiscard','=',0)
                        ->orderBy('orders.id','desc')
                        ->join('orders_info','orders.id','=','orders_info.order_id')
                        ->where('orders_info.product_id','=',$request->prepartion_by)
                        ->join('products','orders_info.product_id','=','products.id')
                        ->join('users','orders.customer_id','=','users.id')
                        ->join('SellingUnits','products.sellingUnit_id','SellingUnits.id')
                        ->select($pluck)
                        ->get()
                        ->toArray();

        }
        else if($request->prepartion_by == "all" && $request->order_date != "null")
        {
                  $new_orders  = Order::where('order_status','=',0)
                    ->where('isDiscard','=',0)
                    ->where('order_date',$request->order_date)
                    ->orderBy('orders.id','desc')
                    ->join('orders_info','orders.id','=','orders_info.order_id')
                    ->join('products','orders_info.product_id','=','products.id')
                    ->join('users','orders.customer_id','=','users.id')
                    ->join('SellingUnits','products.sellingUnit_id','SellingUnits.id')
                    ->select($pluck)
                    ->get()
                    ->toArray();

        } else 
        {


          $new_orders  = Order::where('order_status','=',0)
                         ->where('isDiscard','=',0)
                        ->where('order_date',$request->order_date)
                        ->orderBy('orders.id','desc')
                        ->join('orders_info','orders.id','=','orders_info.order_id')
                        ->where('orders_info.product_id','=',$request->prepartion_by)
                        ->join('products','orders_info.product_id','=','products.id')
                        ->join('users','orders.customer_id','=','users.id')
                        ->join('SellingUnits','products.sellingUnit_id','SellingUnits.id')
                        ->select($pluck)
                        ->get()
                        ->toArray();


        }


       $get_unique_products = Order::where('order_status','=',0)->where('isDiscard','=',0)->orderBy('products.name','asc')
                              ->join('orders_info','orders.id','=','orders_info.order_id')
                              ->join('products','orders_info.product_id','=','products.id')
                              ->select(['name','products.id'])
                              ->distinct()
                              ->get()
                              ->toArray();

      $get_unique_order_dates = Order::where('order_status','=',0)->where('isDiscard','=',0)->orderBy('order_date','asc')
      ->distinct()
      ->select('order_date')
      ->get()
      ->pluck('order_date')
      ->toArray();

      $new_orders = $this->reshape($new_orders);

      ksort($new_orders);
      
      return view('admin.order.prepare')
              ->with('new_orders',$new_orders)
              ->with('unique_products',$get_unique_products)
              ->with('unique_orders',$get_unique_order_dates)
              ->with('prepartion_by',$request->has('prepartion_by') ? $request->prepartion_by : 'all' )
              ->with('selected_order_date',$request->has('order_date') ? $request->order_date : 'null');
    }

    public function reshape($new_orders)
    {
      $result = array();
      foreach ($new_orders as $element) {
          $result[$element['product_name']][] = $element;
      }     
      return $result;
    }









}
