<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Toastr;
use Carbon\Carbon;
class CartController extends Controller
{
    public function addToCart(Request $request)
    {
      try {

            if($request->quantity <= 0 && $request->quantity > 100)
            {
              return response()->json(['status' => '420']);
            }
            $prod = Product::find($request->product_id);
            if($prod->is_available == 0)
            {
              return response()->json(['status' => '402','message' => $prod->name.'is out of stock']);
            }

            $service_ids = "0";
            if($request->serviceOptionsSelection != "0")
            {              
              $service_ids = \App\ServiceGroupOptions::whereIn('service_id',$request->serviceOptionsSelection)->get()->pluck('group_id')->toArray();
              if(count($service_ids) !== count(array_unique($service_ids)))
              {
                return response()->json(['status' => '400','message' => 'only 1 service type is allowed from each group']);
              }

            }
              session()->push('cart',array($request->product_id,'quantity' => $request->quantity,'serviceOption' =>  $request->serviceOptionsSelection,'description' => $request->additionalDescription,'timeslotSelection' => $request->timeslotsSelection,'priceMade' => $request->priceMade));
              return response()->json(['status' => '200','cartSize' => count(session()->get('cart'))]);
          } catch (\Exception $e) {
                  return response()->json(['status' => '500']);
              }

    }

    public function checkout()
    {
	       if(count(session()->get('cart')) <= 0 )
	      {
          session()->flash('failed','The cart is empty');
		      return redirect('/#products');
	       }

      $cart = session()->get('cart');
      $total = (float) 0.00;
      foreach ($cart as $item) {
        $total +=  $item['priceMade'];
      }




      return view('orders.checkout')
      ->with('cart',$cart)
      ->with('total',$total);

    }


    public function deleteSingleOrder(Request $request)
    {
      try {
        $cart = session()->get('cart');
        foreach ($cart as $key => $value) {
          if($request->product_id == $value[0])
          {
              unset($cart[$key]);
              break;
          }
        }
        session()->put('cart',$cart);
        return response()->json(['status' => '200','cartSize' => count(session()->get('cart'))]);
      } catch (\Exception $e) {

        return response()->json(['status' => '404']);

      }

    }
public function errCheckout(){
  if(count(session()->get('cart')) <= 0 )
 {
   session()->flash('failed','The cart is empty');
   return redirect()->route('welcome');
  }

}

}
