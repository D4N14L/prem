<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use URL;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */


    // protected $redirectTo = '/home';

    protected function authenticated(Request $request, $user)
    {

      if(session()->has('atTheCheckout'))
      {
        session()->forget('atTheCheckout');
        return redirect()->route('payment.pay.after.login');
      }
      if($user->hasRole('admin'))
      {
        Auth::logout();
        return redirect()->route('login')->withErrors(['email' => 'wrong credentials','password' => 'wrong credentials']);
      }
      if($user->is_blocked)
      {
        Auth::logout();
        session()->flash('login_failed','Account is blocked by admin, please contact via <br/> info@premiummeat.co.nz');
        return redirect()->back();
      }
      // elseif(!$user->isVerified)
      // {  
      //   Auth::logout();
      //   session()->flash('login_failed','Please verify your email');
      //   return redirect()->back();
      // }

      if(is_countable(session()->get('cart')) && count(session()->get('cart')) > 0)
      {
        return redirect()->route('checkout');
      }
      else
      {
        session()->flash('error','Your cart is empty');
        return redirect()->route('user.getUserManager');

      }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
