<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Toastr;
use App\Events\SendVerficationMail;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        
        if($data['address'])
        {
          $data['address'] = strtolower(trim($data['address']));
        }
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'suburb_id' => 'required',
            'address' => 'required|max:255|unique:users',
            'landline' => 'required|max:12',
            'password_confirmation' => 'required|string|min:6',
            'password' => 'required|string|min:6|confirmed',
            'isAgreementSigned' => 'required',
            'ethnic' => 'required',
        ],[
          'suburb_id.required' => 'Choose Your City & Suburb',
          'isAgreementSigned.required' => 'Agree To Our Terms & Conditions'
        ]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = new \App\User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->address = $data['address'];
        $user->api_token = Hash::make($data['email']);
        if(!isset($data['suburb_id'])){
          $user->suburb_id = \App\Suburb::first()->id;
        }else {
          $user->suburb_id = $data['suburb_id'];
        }
        $user->landline = $data['landline'];
        $user->ethnic = $data['ethnic'];
        $user->referral_link = Hash::make($data['email'].$data['name']);
        if($data['isAgreementSigned'] == "on")
        {
          $user->isAgreementSigned = 1;
        }
        else
        {
          $user->isAgreementSigned = 0;
        }

      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $randstring = '';
      for ($i = 0; $i < 40; $i++) {
        $randstring .= $characters[rand(0, strlen($characters) - 2)];
      }
        $user->verification_token = $randstring;
        $user->save();


          // Fire Send Mail Event
        event(new SendVerficationMail($user));

        return $user;
    }
}
