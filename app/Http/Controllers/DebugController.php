<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events\NotifyAdminNewOrder;

class DebugController extends Controller
{
    public function testRealtime()
    {
      event(new NotifyAdminNewOrder('danial'));
      return 'event fired!';
    }
}
