<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Validator;
use App\Inventory;
use App\ServiceType;
use App\ProductCategory;
use App\OrderInfo;
use App\ProductPieces;
use App\ServiceGroup;
use App\ServiceGroupOptions;
use Toastr;
class ProductController extends Controller
{

    public function byCategory($id)
    {
      $products = Product::where('cat_id','=',$id)->orderBy('arrange','asc')->get();
      $cat = ProductCategory::where('id','=',$id)->first();
      return view('products.products')->with('products',$products)->with('cat',$cat);
    }

    /*
    |--------------------------------------------------------------------------
    | Admin Side Code
    |--------------------------------------------------------------------------
    |This is admin side code
    |
    */

    public function chickenCategory()
    {
      return view('products.chicken');
    }

    public function greenChickenCategory()
    {
      return view('products.greenChicken');
    }

    public function browse()
    {
      $products = Product::where('cat_id','=',\App\ProductCategory::first()->id)->orderBy('arrange','asc')->get();
      return view('admin.products.browse')->with('products',$products)->with('cat',\App\ProductCategory::first()->name);
    }

    public function browseByCategory($category)
    {
      $products = Product::where('cat_id','=',$category)->orderBy('arrange','asc')->get();
      $category = ProductCategory::findOrFail($category);
      return view('admin.products.browseByCategory')->withProducts($products)->with('cat',$category);
    }

    public function addToSpecial($product_id)
    {
      $category = ProductCategory::where('name','LIKE','Special')->get()->first();
      $product = Product::where('name','=',$product_id)->where('cat_id','=',$category->id)->get()->first();
      if($product && $product->count() > 0)
      {
        Toastr::error('Product Already Exists In Special Category!');
        return redirect()->back();
      }
      $product = Product::where('name','=',$product_id)->get()->first()->toArray();
      $new_product = new Product();
      foreach($product as $key => $p)
      {
        if($key != "id")
        {
          $new_product[$key] = $p;
        }
      }
      $new_product->cat_id = $category->id;
      $new_product->save();
      Toastr::success('Product Added To Special Category!');
      return redirect()->back();

    }

    public function removeFromSpecial($product_id)
    {
      $category = ProductCategory::where('name','LIKE','Special')->get()->first();
      $product = Product::where('name','=',$product_id)->where('cat_id','=',$category->id)->get()->first();
      if($product && $product->count() > 0)
      {
        $product->delete(); 
        Toastr::success('Product Removed From Special Category!');
        return redirect()->back();
      }
      
      Toastr::error('Could not found product in Sepcial Category!');
      return redirect()->back();

    }

   
    public function create()
    {
      return view('admin.products.create');
    }

    public function manageStatus(Request $request)
    {
        try {
          $product = Product::findOrFail($request->product_id);
          $tosend = "";
          if($request->status == 0)
          {
            $product->status = 1;
            $tosend = 1;
          }
          if($request->status == 1)
          {
            $product->status = 0;
            $tosend = 0;
          }
           $product->save();
          return response()->json(['message' => '200','status' => $tosend]);
        } catch (\Exception $e) {
          return response()->json(['message' => '404','status' => $tosend]);
        }

    }

    public function store(Request $request)
    {

      /**
      * Check Valiadtion For Incoming POST Request Using ::Validation
      */




     $validator = Validator::make($request->all(),[
         'name'   => 'required',
          'cat_id' => 'required',
          'image'  => 'required',
          'description' => 'required',
          'sellingUnit_id' => 'required',
          'timeslot' => 'required',
          'price' => 'required'
      ]);

      /**
      * If Validation Test Passess
      */



     if(!($validator->fails()))
     {
       try {

         /**
         * Add New Product On Validation Pass
         */

         $product = new Product();
         $product->is_available = 1;
         $product->name = $request->name;
         $product->description = $request->description;
         $product->cat_id = $request->cat_id;
         $product->sellingUnit_id = $request->sellingUnit_id;
         $product->timeslot = $request->timeslot;
         $product->service_groups = is_null($request->service_groups) ? '0' : implode(',',$request->service_groups);
         $serviceOptionsEnabled = [];
         if($product->service_groups != '0')
         {
          $serviceOptionsEnabled = ServiceGroupOptions::whereIn('group_id',$request->service_groups)->get()->pluck('service_id')->toArray();
          $product->serviceOptionsEnabled = is_null($serviceOptionsEnabled) ? '0' : implode(',',$serviceOptionsEnabled);
          }

         $product->price = (float) $request->price;
         
           if($request->has('image'))
           {
             $image = $request->file('image');
             $imageName = time().'.'.$image->getClientOriginalExtension();
             $newImage = \Image::make($request->file('image'));
             $newImage->resize(1366,850);
             $newImage->save(public_path('assets/products/'.$imageName),10);
             $product->image = $imageName;
           }
           if($request->has('avg_weight_piece')){
             $product->avg_weight = $request->avg_weight_piece;
           }
         $product->save();


         /**
         * Create A New Instance Of Inventory
         */


         $inventory = new Inventory();
         $inventory->product_id = $product->id;
         $inventory->save();

         /**
         * Product Pieces Average Weight Add If Avialable
         */


         /**
         * Return Success Toaster On Product Success To Be Added
         */

         Toastr::success('Product Added Successfully!');
         return redirect()->route('products.browse');


        } catch (\Exception $e) {

          /**
          * Return Error Toaster On Product Failed To Be Added
          */

          Toastr::error('Product Add Failed, Please Try Again.');
          return redirect()->back();

        }

      }
      else
     {
       Toastr::error($validator->messages()->first());
       return redirect()->back();

     }

    }
    public function edit($id)
    {
          $product = Product::findOrFail($id);
          return view('admin.products.edit')->withProduct($product);
    }

    public function update(Request $request)
    {

      $validator = Validator::make($request->all(),[
          'name'   => 'required',
           'cat_id' => 'required',
           'description' => 'required',
           'sellingUnit_id' => 'required',
           'timeslot' => 'required',
           'price' => 'required'
       ]);
       if($validator->fails())
       {
         Toastr::error($validator->messages()->first());
         return redirect()->back();
       }
          $product = Product::findOrFail($request->product_id);
          $product->name = $request->name;
          $product->description = $request->description;
          $product->cat_id = $request->cat_id;
          $product->sellingUnit_id = $request->sellingUnit_id;
          // $product->selling_unit_amount = $request->selling_unit_amount;
          $product->service_groups = is_null($request->service_groups) ? '0' : implode(',',$request->service_groups);
          $serviceOptionsEnabled = [];
          if($product->service_groups != '0')
          {
           $serviceOptionsEnabled = ServiceGroupOptions::whereIn('group_id',$request->service_groups)->get()->pluck('service_id')->toArray();
           $product->serviceOptionsEnabled = is_null($serviceOptionsEnabled) ? '0' : implode(',',$serviceOptionsEnabled);
           }
 
          $product->timeslot = $request->timeslot;
          $product->price = $request->price;
          if($request->has('image'))
          {
            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $newImage = \Image::make($request->file('image'));
            $newImage->resize(1366,850);
            $newImage->save(public_path('assets/products/'.$imageName),10);
            // $image->move(public_path('assets/products'),$imageName);
            $product->image = $imageName;
          }

          if($request->has('avg_weight_piece')){
            $product->avg_weight = $request->avg_weight_piece;
          }

          $product->save();
          Toastr::success('Product Updated Successfully');
          return redirect()->route('products.browse');
    }

    public function delete($id)
    {
          try {
            $product = Product::findOrFail($id);
            $inventory = Inventory::where('product_id','=',$product->id)->delete();
            $OrderInfo = OrderInfo::where('product_id','=',$product->id)->delete();
            $product->delete();
            Toastr::success('Product Deleted Successfully!');
            return redirect()->route('products.browse');

          } catch (\Exception $e) {
            Toastr::error('Undexpected Error Ocuured While Deleting This Product!');
            return redirect()->back();

          }

    }

    public function managePosition(Request $request)
    {

      try {
        foreach ($request->positions as $value) {
          \App\Product::where('id',$value[0])->update(['arrange' => $value[1]]);
        }
        return response()->json(['status' => '200']);
      } catch (\Exception $e) {
        return response()->json(['status' => '500']);
      }


    }

    public function fetchServices(Request $request)
    { 
      $cat_id = $request->cat_id;
      $groups = ServiceGroup::where('service_groups.cat_id','=',$cat_id)
                ->get()
                ->toArray();
      // $services = ServiceGroupOptions::whereIn('group_id',$groups)->get()->map->only(['group_id','']);
      return response()->json($groups);

    }

    public function outofstock($id)
    {
      try {
        $product = Product::find($id);
        $product->is_available = 0;
        $product->save();
        Toastr::success('Product Update Succesfully!');
        return redirect()->back();  
      } catch (\Throwable $th) {
        Toastr::error('Product Update Failed!');
        return redirect()->back();  
      }
    }

    public function instock($id)
    {
      try {
        $product = Product::find($id);
        $product->is_available = 1;
        $product->save();
        Toastr::success('Product Update Succesfully!');
        return redirect()->back();  
      } catch (\Throwable $th) {
        Toastr::error('Product Update Failed!');
        return redirect()->back();  
      }

    }











}
