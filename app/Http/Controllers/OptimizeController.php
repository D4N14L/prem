<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Image as OptimizeImage;
use App\Product;
use App\Slider;
use App\ProductCategory;
use Toastr;
use App\ServiceType;
use Artisan;
class OptimizeController extends Controller
{

  /*
  Optimization
              Wrapper
  */


  public function Optimize()
  {
    try {

      OptimizeController::OptimizeProductImages();
      OptimizeController::OptimizeCategoriesImages();
      OptimizeController::OptimizeSlidersImages();
      Toastr::success('Website Optimized Successfully');
      Artisan::call('config:clear');
      Artisan::call('cache:clear');
      Artisan::call('view:cache');
      Artisan::call('route:cache');
    } catch (\Exception $e)
    {
      Toastr::error('Unexpected Error Occured');
    }

    return redirect()->back();
  }

    /*
    Products
            Optimize
    */

    public static function OptimizeProductImages()
    {
      $Images = Product::pluck('image')->toArray();
      foreach ($Images as $image) {
        $path = pathinfo(public_path('assets/products/'.$image));
        OptimizeController::OptimizeHelper($path);
      }
    }

    /*
    Categories
            Optimize
    */

    public static function OptimizeCategoriesImages()
    {

        $Images = ProductCategory::pluck('image')->toArray();
        foreach ($Images as $image)
        {
          $path = pathinfo(public_path('assets/category/'.$image));
          OptimizeController::OptimizeHelper($path);
        }
    }

    /*
      Slides
             Optimize
    */

    public static function OptimizeSlidersImages()
    {
      $Images = Slider::pluck('image')->toArray();
      foreach ($Images as $image)
      {
        $path = pathinfo(public_path('sliders/'.$image));
        OptimizeController::OptimizeHelper($path);
      }
    }


    /*
      Validate
             Images
    */

    public static function isValidExtension($extension){
      $extension = strtolower($extension);
      if (
          $extension !== "jpg" ||
          $extension !== "png" ||
          $extension !== "gif" ||
          $extension !== "webp"
        )
        {
          return false;
        }
          return true;
    }

    /*
      Helper
             Optimize
    */

    public static function OptimizeHelper($path)
    {
      if(OptimizeController::isValidExtension($path['extension'])){
        $path = $path['dirname'].'/'.$path['basename'];
        $Optimized = OptimizeImage::make($path);
        $Optimized->resize(1366,850);
        $Optimized->save($path,100);
      }
    }

    public function fillProductsWithEnableOptions(){
      foreach (\App\Product::all() as $product)
      {
      $serviceOption =  \App\ServiceType::where('cat_id','=',$product->cat_id)->get();
      $ids = [];
      foreach ($serviceOption->all() as $so) {
        $ids[] = $so->id;
      }

     $product->serviceOptionsEnabled = implode(',',$ids);
     $product->save();
      }
    }

    public function enableJavascript()
    {
      return view('enable-javascript');
    }

  }
