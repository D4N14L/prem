<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceType;
use App\ServiceTypeOptions;
use Validator;
use Toastr;
class ServiceTypeController extends Controller
{
    public function browser()
    {
      //
      $serviceTypes = ServiceType::orderBy('id','desc')
                                                       ->paginate( 10 );
      //
      return view('admin.serviceType.browse')
                                            ->with('serviceTypes',$serviceTypes);
    }

    public function store(Request $request)
    {

      $validator = Validator::make($request->all(),[
        'name' => 'required | min:4',
        // 'cat_id' => 'required'
      ],[
        'name.required' => 'Please Enter Service Option Name',
        'name.min' => 'Name Must Be Of Minimum 4 Characters',
      ]);
    if($validator->fails()){
      Toastr::error($validator->messages()->first());
      return redirect()->back();
    }


      if($request->is_edit == 0)
      {
        //
        $serviceType = new ServiceType( );
        //
        $serviceType->name = $request->name;
        //
        $serviceType->cat_id = 0;
        //
        $serviceType->timestamps = false;
        //
        $serviceType->save();
        //
        return redirect()
                        ->route('serviceType.browse');
      }
      else
      {
        //
        $serviceType = ServiceType::find($request->service_id);
        //
        $serviceType->name = $request->name;
        //
        $serviceType->cat_id = 0;
        //
        $serviceType->timestamps = false;
        //
        $serviceType->save();
        //
        return redirect()
                        ->route('serviceType.browse');

      }
    }

    public function destroy($id)
    {
      $serviceTypes = ServiceType::find($id);
      $order = \App\OrderInfo::where('service_type_option','=',$serviceTypes->id)->update(['service_type_option' => 0]);;
      $serviceTypes->delete();
      $serviceTypesOptions = ServiceTypeOptions::where('service_type_id','=',$serviceTypes->id)->delete( );

      return redirect()->route('serviceType.browse');
    }
}
