<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderInfo;
use App\Product;
use Auth;
use App\Events\NotifyAdminNewOrder;
use App\Events\CustomMail;
use Toastr;

use Cartalyst\Stripe\Stripe;
use App\Notifications\NotifyNewOrder;
use Identify;
class StripeController extends Controller
{








      //Payment From Request

      public function getPayForm(Request $request)
      {

        if(!$request->has('delivery'))
        {
          session()->flash('failed','<b style="word-wrap:break-word;font-size:12px">Please Choose Shipment Method</b>');
          return redirect()->back();
        }
        if(!$request->orderDate)
        {
          session()->flash('failed','<b style="word-wrap:break-word;font-size:12px">Please Choose Order Date</b>');
          return redirect()->back();
        }
        $cart = session()->get('cart');
        $total = (float) 0.00;
        foreach ($cart as $item) {
          $total +=  $item['priceMade'];
        }
          if($request->delivery == 1){
            $minimumAmount = (float) \App\Miscellaneous::first()->minimumAmount;
            if($total < (float) $minimumAmount )
            {
              session()->flash('failed','Please Shop More Than <b>$'.$minimumAmount.'</b>');
              return redirect()->back();
            }
          }

          // Check if user has selected TimeSlot

          session()->put('current_timeslot_for_order',\App\TimeSlot::first()->id);
        // Check if user has selected Order Date


        if(!$request->has('orderDate')){
          Toastr::error('<b style="word-wrap:break-word;font-size:12px">Please, Select Date to Deliver</b>');
          session()->flash('failed','<b style="word-wrap:break-word;font-size:12px">Please, Select Date to Deliver</b>');
          session()->flash('deliverySelected',$request->delivery);
          return redirect()->back();
        }else {
          session()->put('orderDate',$request->orderDate);
        }


        // Sum up all the delivery charges


        $total = $request->only('total')['total'];
        session()->put('total',(float) $total);
        
        session()->put('delivery',(int) $request->delivery);

        if(Auth::guest())
        {
          session()->flash('failed','<b style="word-wrap:break-word;font-size:12px">Please login or register for payment</b>');
          session()->put('atTheCheckout',true);
          return redirect()->route('login');
        }
          return view('orders.paymentForm');
      }

      public function getPayFormAfterLogin()
      {
        return view('orders.paymentForm');
      }

      // Payment Request From Stipe Api

      public function paymentRequest(Request $request)
      {
          // sk_live_1ahfqHGkgIegMmSvBcSR379x
        $outofstock_products = [];
        $is_delivery = session()->get('delivery');
          // try {
          $cart = session()->get('cart');
          $total = (float) 0.00;
          foreach ($cart as $item) {
            $total +=  $item['priceMade'];
          }

            if($is_delivery == 1){
              $minimumAmount = (float) \App\Miscellaneous::first()->minimumAmount;
              if($total < (float) $minimumAmount )
              {
                session()->flash('failed','Please Shop More Than <b>$'.$minimumAmount.'</b>');
                session()->flash('deliverySelected',$request->delivery);
                return redirect()->back();
              }

            }






            try {
              $card = str_replace(' ','',(string) $request->number);
              $stripe = new Stripe(env('STRIPE_API_KEY'));
              $token = $stripe->tokens()->create([
                'card' => [
                  'number' => $card ,
                  'exp_month' => (int)explode('/',$request->expiry)[0],
                  'exp_year' => (int)explode('/',$request->expiry)[1],
                  'cvc' => $request->cvc,
                ],
              ]);
          
                if(!isset($token['id']))
                {
                     Toastr::error("Something's Not Right, Please Verify Your Credit Card Credentials");
                     return redirect()->route('checkout');   
                }
      
              //   if($user->isCustomer == 0) {
                $customer_id = "";

                $customer = $stripe->customers()->create([
                  'email' => Auth::user()->email,
                  'source' => $token['id']
                ]);
                $customer_id = $customer['id'];
                $user = \App\User::find(Auth::user()->id);
                $user->isCustomer = 1;
                $user->stripeCustomerID = $customer_id;
                $user->save();
               }
              catch (Cartalyst\Stripe\Exception\BadRequestException $e) {
                 session()->flash('failed',$e->getMessage());
                   return redirect()->route('checkout');   
                }
              catch (Cartalyst\Stripe\Exception\UnauthorizedException $e) {
      
                 session()->flash('failed',$e->getMessage());
                 return redirect()->route('checkout');   
                }
              catch(Cartalyst\Stripe\Exception\InvalidRequestException $e){
      
                 session()->flash('failed',$e->getMessage());
                 return redirect()->route('checkout');   
                } catch(Cartalyst\Stripe\Exception\NotFoundException $e){
      
                  session()->flash('failed',$e->getMessage());
                  return redirect()->route('checkout');   
                } catch (Cartalyst\Stripe\Exception\CardErrorException $e) {
      
                  session()->flash('failed',$e->getMessage());
                  return redirect()->route('checkout');   
            }
              catch (Cartalyst\Stripe\Exception\ServerErrorException $e) {
                   session()->flash('failed',$e->getMessage());
                   return redirect()->route('checkout');   
                }
              catch (\Exception $e)
              {
                   session()->flash('failed','Reverify your card credentials  ');
                   return redirect()->route('checkout');   
              }


            $order = new Order();
            $delivery_charges_by_admin = (float) \App\Suburb::where('id','=',$request->suburb_id)->first()->delivery_charges;
            $order->customer_id = Auth::user()->id;
            $order->order_status = 0;
            $order->timeslotsSelection = session()->get('current_timeslot_for_order');
            $order->order_date = session()->get('orderDate');
            $order->suburb = \App\Suburb::where('id','=',$request->suburb_id)->first()->suburbName;
            $order->city = \App\Area::where('id','=',$request->city_id)->first()->cityName;
            if($is_delivery == 0)            {
              $order->total_actual_amount = (float) session()->get('total');
              $order->total_amount = (float) session()->get('total') ;
            }
            else {
              $order->delivery_charges = $delivery_charges_by_admin;
              $order->total_actual_amount = (float) session()->get('total') -  $delivery_charges_by_admin;
              $order->total_amount = (float) session()->get('total') - $delivery_charges_by_admin;
            }
            $order->operating_system = Identify::os()->getName();
            if($request->has('streetaddress'))
            {
              $order->address = $request->streetaddress;
            }else {
              $order->address = Auth::user()->address;
            }
            $order->isDelivery = session()->get('delivery');
            $order->save();

            foreach (session()->get('cart') as $item) {
              $product = \App\Product::where('id','=',$item[0])->first();
              if(!$product->is_available)
              {
                  $outofstock_products[] = $product->name;
              }
              $orderInfo = new OrderInfo();
              $orderInfo->product_id = $item[0];
              $orderInfo->order_id = $order->id;
              $orderInfo->amount = $item['priceMade'];
              $orderInfo->actual_quantity = $product->avg_weight || 0;
              $orderInfo->quantity = (float) $item['quantity'];
              $orderInfo->service_type_option = !is_array($item['serviceOption']) ? '0': json_encode($item['serviceOption']);
              $orderInfo->additional_information = $item['description'] ;
              $orderInfo->save();
            }

              session()->forget('cart');
              session()->forget('current_timeslot_for_order');
              session()->forget('delivery');
              session()->forget('total');
              session()->flash('success',"Your Order Has Been Placed,  &nbsp;Thanks For Dropping By!");
              // $user = User::where('email',Auth::user()->email)->get()->first();


              $notification = "";
              if(count($outofstock_products) > 0)
              {
                  $notification = '<br/><br/>
                    <b> Due to the current situation, The following product(s) are out of stock in your order. </b>
                  <ul>
                  '.
                  implode('<br/>',$outofstock_products  )
                  .
                  '</ul>';

              }


              if($is_delivery == 1)
              {
                event(new CustomMail(Auth::user() ,'Thanks for placing an order with Premium meats. Your order will be delivered on the selected date before 5pm. Please be assured that someone is present at the provided address to receive the order.'.$notification.'<br>In the event of failure of any receiver at the given address, customer has to arrange a pick up within 24 hours. After 24 hours company will hold no responsibilty for that particular order.','Thanks For Dropping By'));

              }else
              {
                event(new CustomMail(Auth::user() ,'Thanks for placing an order with premium meats. We will let you know by email once your order is ready to be collected.'.$notification,'Thanks For Dropping By'));
              }
              event(new NotifyAdminNewOrder('hello danielox'));

              return redirect()->to('/#products');
        //   }catch (\Exception $e) {
        //     session()->flash('failed','Payment Failed! Try Later');
        //     return redirect()->route('checkout');
        // } catch(\Cartalyst\Stripe\Exception\CardErrorException $e){
        //   session()->flash('failed',"Unable to find card for the current customer");
        //   return redirect()->route('checkout');
        // } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e){
        //   session()->flash('failed',$e);
        //   return redirect()->route('checkout');
        // }
      }

}
