<?php 

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use DB;
use \App\Product;
class ProductController extends Controller
{

    protected $out_of_stock = '( Out of Stock )';
    protected $all_products_query = "
        SELECT * FROM products order by name ASC
    ";
    protected $products_by_id = 
    "
        SELECT * FROM products WHERE id = ?
    ";
    protected $products_by_cat_id = 
    "
        SELECT * FROM products WHERE cat_id = ?
    ";

    public function products()
    {
        $products = collect(DB::select($this->all_products_query));       
        foreach($products as $product)
        {        
          if(!$product->is_available)
          {
            $product->name = $product->name.' (out of stock)';
          }
        } 
        return response()->json(['status' => 200,'products' => $products]);
    }



    public function products_new()
    {
        $products = Product::orderBy('name','asc')->get();       
        // foreach($products as $product)
        // {        
        //   if(!$product->is_available)
        //   {
        //     $product->name = $product->name.' (out of stock)';
        //   }
        // }

        
        foreach($products as $product)
        {
          $exploded = array_filter(explode(",",$product['service_groups']));          

          if(count($exploded) > 0)
          {
            
            $groups = \App\ServiceGroup::whereIn('id', $exploded)->get();
            $group_wise_services = [];
            foreach($groups as $group)
            {
              $services = \App\ServiceGroupOptions::where('group_id',$group->id)->get()->pluck('service_id')->toArray();
              $services = \App\ServiceType::whereIn('id',$services)->get()->map->only(['id','name'])->toArray();
              $group_wise_services[] = ['id' => $group->id,'name' => $group->name,'nestedOptions' => $services];

            }
            $product['serviceOptionsEnabled'] = $group_wise_services;      

          }else {

            $product['serviceOptionsEnabled'] = NULL;      
          }        
          
        }
        return response()->json(['status' => 200,'products' => $products]);
    }




    public function productById($id)
    {
        $products = collect(DB::select($this->products_by_id,[$id]))->first();
        foreach($products as $product)
        {        
          if(!$product->is_available)
          {
            $product->name = $product->name.' (out of stock)';
          }
        } 
        return response()->json(['status' => 200, 'product' => $products]);
    }

    public function productByCatId($id)
    {
        $products = Product::where('cat_id','=',$id)->get();        
        foreach($products as $product)
        {        
          if(!$product->is_available)
          {
            $product->name = $product->name.' (out of stock)';
          }
        } 
        return response()->json(['status' => 200, 'products' => $products]);
    }


    public function productByCatIdNew($id)
    {
        $products = Product::where('cat_id','=',$id)->get();        
        // foreach($products as $product)
        // {        
        //   if(!$product->is_available)
        //   {
        //     $product->name = $product->name.' (out of stock)';
        //   }
        // } 
        foreach($products as $product)
        {
          $exploded = array_filter(explode(",",$product['service_groups']));          

          if(count($exploded) > 0)
          {
            
            $groups = \App\ServiceGroup::whereIn('id', $exploded)->get();
            $group_wise_services = [];
            foreach($groups as $group)
            {
              $services = \App\ServiceGroupOptions::where('group_id',$group->id)->get()->pluck('service_id')->toArray();
              $services = \App\ServiceType::whereIn('id',$services)->get()->map->only(['id','name'])->toArray();
              $group_wise_services[] = ['id' => $group->id,'name' => $group->name,'nestedOptions' => $services];

            }
            $product['serviceOptionsEnabled'] = $group_wise_services;      

          }else {

            $product['serviceOptionsEnabled'] = NULL;      
          }        
          
        }
        return response()->json(['status' => 200, 'products' => $products]);
    }



}