<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Events\NotifyAdminNewOrder;
use Illuminate\Http\Request;
use Cartalyst\Stripe\Stripe;
use App\Events\CustomMail;
use DB;
use App\User;
use App\Area;
use App\Suburb;
use App\Order;
use App\OrderInfo;
use Identify;
class CheckoutController extends Controller
{

    // \App\Miscellaneous::first()->deliveryCharges

    public function checkout(Request $request)
    {


        $validator = Validator::make($request->all(),[
            'api_token' => 'required',
            'timeslot' => 'required',
            'order_date' => 'required',
            'suburb' => 'required',
            'city' => 'required',
            'total_amount' => 'required',
            'address' => 'required',
            'isDelivery' => 'required',
            'payment_information' => 'required',
            'order_items' => 'required',
            "payment_information.name" => 'required',
            'payment_information.card_number' => 'required',
            'payment_information.expires' => 'required',
            'payment_information.ccv' => 'required'
        ]);

        if($validator->fails())
        {
            return response(['status' => 400, 'message' => $validator->messages()->first()]);
        }
        // User Check If Api_Token Exists

        $user = User::where('api_token',$request['api_token'])->get()->first();
        if(!$user)
        {
            return response(['status' => 400, 'message' => "Unable Find User With That Api Token"]);
        }

        // Stripe Customer Token

        try {
        $card = $request['payment_information'];
        $stripe = new Stripe(env('STRIPE_API_KEY'));
        $exp_ = explode('/',$card['expires']);
        $token = $stripe->tokens()->create([
            'card' => [
              'number' => $card['card_number'] ,
              'exp_month' => $exp_[0] ,
              'exp_year' => $exp_[1],
              'cvc' => $card['ccv'],
            ],
          ]);

          if(!isset($token['id']))
          {
            return response()->json(['status' => 400, 'message' => "Something's Not Right, Please Verify Your Credit Card Credentials"]);
          }

        //   if($user->isCustomer == 0) {
            $customer = $stripe->customers()->create([
                'email' => $user->email,
                'source' => $token['id']
              ]);
              $customer_id = $customer['id'];
              $user->stripeCustomerID = $customer_id;
              $user->isCustomer = 1;
        //   }
          $user->save();
        }
        catch (Cartalyst\Stripe\Exception\BadRequestException $e) {
            return response()->json(['status' => $e->getCode(), 'message' => $e->getMessage()]);
        }
        catch(\Exception $e)
        {
            return response()->json(['status' => $e->getCode(), 'message' => $e->getMessage()]);
        }
        catch (Cartalyst\Stripe\Exception\UnauthorizedException $e) {

            return response()->json(['status' => $e->getCode(), 'message' => $e->getMessage()]);
        }
        catch(Cartalyst\Stripe\Exception\InvalidRequestException $e){

           return response()->json(['status' => $e->getCode(),'message' => $e->getMessage()]);
        } catch(Cartalyst\Stripe\Exception\NotFoundException $e){

           return response()->json(['status' => $e->getCode(),'message' => $e->getMessage()]);
        } catch (Cartalyst\Stripe\Exception\CardErrorException $e) {

           return response()->json(['status' => $e->getCode(), 'message' => $e->getMessage()]);
        }
        catch (Cartalyst\Stripe\Exception\ServerErrorException $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
        catch (\Exception $e)
        {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }
        try {

            $outofstock_products = [];

            $order = new Order();

            $suburb = \App\Suburb::where('id','=',$request['suburb'])->first();

            $order->customer_id = $user->id;
            $order->order_status = 0;
            $order->timeslotsSelection = $request['timeslot'];
            $order->order_date = $request['order_date'];
            $order->suburb = $suburb->suburbName;
            $order->city = \App\Area::where('id','=',$request['city'])->first()->cityName;
            $order->total_amount = (float) $request['total_amount'];
            $order->total_actual_amount = (float) $request['total_amount'];
            $order->address = $request['address'];
            $order->isDelivery = $request['isDelivery'];
            $order->operating_system = "Handset";
            if($request['isDelivery'])
            {
                $delivery_amount = $suburb->delivery_charges;
                $order->delivery_charges = $delivery_amount;
                $order->total_actual_amount = ((float) $request['total_amount']);
            }
            $order->save();

            foreach ($request['order_items'] as $item) {
                $product = \App\Product::where('id','=',$item['product_id'])->first();
                if(!$product->is_available)
                {
                    $outofstock_products[] = $product->name;
                }
                $orderInfo = new OrderInfo();
                $orderInfo->product_id = $item['product_id'];
                $orderInfo->order_id = $order->id;
                $orderInfo->amount = $item['amount'];
                $orderInfo->actual_quantity = $product->avg_weight || 0;
                $orderInfo->quantity = (float) $item['quantity'];
                $orderInfo->service_type_option = is_null($item['service_type_option']) ? '0': json_encode(explode(',',$item['service_type_option']));
                $orderInfo->additional_information = $item['additional_information'] ;
                $orderInfo->save();

              }


              $notification = "";
              if(count($outofstock_products) > 0)
              {
                  $notification = '<br/><br/>
                    <b> Due to the current situation, The following product(s) are out of stock in your order. </b>
                  <ul>
                  '.
                  implode('<br/>',$outofstock_products  )
                  .
                  '</ul>';

              }


              if($request['isDelivery'])
              {
                event(new CustomMail($user ,'Thanks for placing an order with Premium meats. Your order will be delivered on the selected date  before 5pm. Please be assured that someone is present at the provided address to receive the order.'.$notification.'	     <br> In the event of failure of any receiver at the given address, customer has to arrange a pick up within 24 hours. After 24 hours company will hold no responsibilty for that particular order. 
                <br> Thanks','Thanks For Dropping By'));

              }else
              {
                event(new CustomMail($user ,'Thanks for placing an order with premium meats. We will let you know by email once your order is ready to be collected.'.$notification,'Thanks For Dropping By'));
              }

              event(new NotifyAdminNewOrder('hello danielox'));
              return response()->json(['status' => 200, 'message' => "Your Order Has Been Placed,Thanks For Dropping By!"]);

        }
        catch (\Exception $e) {
            return response()->json(['status' => 500, 'message' => $e->getMessage()]);
        }

    }







}
