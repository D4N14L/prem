<?php 

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use DB;
class CategoryController extends Controller
{

    protected $all_categories_query = "
    SELECT * FROM `product-category`
    ";
    protected $categories_by_id = 
    "
        SELECT * FROM `product-category` WHERE id = ?
    ";
    protected $categories_by_product = 
    "
        SELECT * FROM products WHERE cat_id = ?
    ";

    public function categories()
    {
        $products = collect(DB::select($this->all_categories_query));
        return response()->json(['products' => $products]);
    }

    public function categoryById($id)
    {
        $products = collect(DB::select($this->categories_by_id,[$id]))->first();
        return response()->json($products);
    }

    public function categoryProductsById($id)
    {
        $products = collect(DB::select($this->categories_by_product,[$id]));
        return response()->json($products);
    }




}