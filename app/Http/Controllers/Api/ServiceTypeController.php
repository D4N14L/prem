<?php 

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\ServiceType;
class ServiceTypeController extends Controller
{
    protected $getByIds = "
    SELECT id,name,cat_id as for_category FROM `service_type` JOIN  (:IDS)
    ";

    public function getByIds(Request $request)
    {  
        $ids = $request['service_types'];
        $service_types = ServiceType::findMany(explode(',',$ids));  
        return response()->json(['status' => 200, 'Service_types' => $service_types]);
    }

    // public function getServiceOptionsId(Request $request)
    // {
        // \App\ServiceGroupOptions::where('service_id',$request->)
    // }





}






















































