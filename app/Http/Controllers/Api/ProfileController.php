<?php 

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Events\SendVerficationMail;
use DB;
use Hash;
use App\User;

class ProfileController extends Controller
{
   protected $general_info_query = "
   SELECT users.id, 
   users.name, 
   users.referral_link, 
   users.address, 
   users.email,
   users.landline,
   users.suburb_id AS suburb_id,
   (
    SELECT areas.id FROM areas WHERE areas.id IN 
      (
        SELECT areas_id FROM suburbs WHERE suburbs.id = ?
      )
   ) as city_id ,    
   Count(orders.id) AS total_orders, 
   (SELECT Count(referal.id) 
    FROM   referal 
    WHERE  referal.referer_id = users.id
    ) 
    AS total_referals,

   (SELECT Count(orders.id) 
    FROM   orders 
    WHERE  orders.customer_id = users.id 
           AND orders.order_status = 0)   AS pending_orders, 
   (SELECT Count(orders.id) 
    FROM   orders 
    WHERE  orders.customer_id = users.id 
           AND orders.order_status = 1)   AS delivered_orders 
    FROM   users 
    JOIN orders 
    ON users.id = orders.customer_id 
    WHERE  users.id = ?
     ";
    protected $orders_info_query = "
    SELECT  
    order_status, 
    isdelivery, 
    Date(created_at) as placed_on,
    total_amount                              AS estimated_amount, 
    (SELECT miscellaneous.deliverycharges 
     FROM   miscellaneous)                    AS deliveryCharges, 
    (SELECT Sum(orders_info.extra_payment) 
     FROM   orders_info 
     WHERE  orders_info.order_id = orders.id) AS extra_amount, 
    (SELECT round(extra_amount + estimated_amount))  AS actual_amount, 
    (SELECT round(actual_amount + deliverycharges))  AS final_amount 
    FROM   orders 
    WHERE  customer_id = ? 
    ";

    protected $referals_info_query = "
    SELECT name, 
       email, 
       Date(referal.created_at)  AS 'joined_on', 
       (SELECT created_at 
        FROM   orders 
        WHERE  orders.customer_id = users.id) AS 'last_order' 
        FROM   users 
       JOIN referal 
         ON users.id = referal.referal_id 
       WHERE  referal.referer_id = ?
    ";
    public function profile($email)
    {
        $user = $this->check_cred($email,'email');
        if(!$user)
        {
          return response(['status' => 400, 'message' => "Unable Find User With That Email Address"]);
        }
        $id = $user->id;
        $info = collect(DB::select($this->general_info_query,[$user->suburb_id,$id]))->first();
        $orders= collect(DB::select($this->orders_info_query,[$id]));
        $referals = collect(DB::select($this->referals_info_query,[$id]));
        return response()->json(['status' => 200,'user_info' => $info,'orders_history' => $orders,'referals' => $referals ]);
    }

    public function updateProfile(Request $request)
    {
      $user = $this->check_cred($request['api_token'],'api_token');
      if(!$user)
      {
        return response(['status' => 400, 'message' => "Unable Find User With That Api Token"]); 
      }

      if($this->up_profile_strict_c($request))
      {

        $user->fill($request->only(['email','address','suburb_id','landline','name','isAgreementSigned']));
        $user->save();
        return response(['status' => 200, 'api_token' => $user->api_token]);

      }
      else 
      {
        return response(['status' => 400, 'message' => "Insufficient Parameters"]); 

      }

    }

    
    public function check_cred($val,$col)
    {
      return User::where($col,$val)->get()->first();
    }

    public function up_profile_strict_c($args)
    {
      if(
        $args->has('email') ||
        $args->has('address') ||
        $args->has('suburb_id') ||
        $args->has('landline') ||
        $args->has('name') ||
        $args->has('isAgreementSigned')
      ){
        return true;
      }
      else
      {
        return false;
      }
    }










    public function updatePassword(Request $request)
    {
      $user = $this->check_cred($request['api_token'],'api_token');
      if(!$user)
      {
        return response(['status' => 400, 'message' => "Unable Find User With That Api Token"]); 
      }
      if($this->up_password_check_strict($request))
      {
        if(!Hash::check($request['old_password'],$user->password))
        {
          return response(['status' => 401, 'message' => "Unauthorized Access"]); 
        }
        if(Hash::check($request['new_password'],$user->password))
        {
          return response(['status' => 400, 'message' => "Old and New Password Cannot Be Same"]); 
        }
        $old_pwd = $user->password;
        $user->password = Hash::make($request['new_password']);
        $user->save();
        return response(['status' => 200, 'api_token' => $user->api_token,'email' => $user->email,'old_password' =>  $old_pwd,  'new_password' => $request['new_password'] ]);

      }
      else 
      {
        return response(['status' => 400, 'message' => "Insufficient Parameters"]); 
 
      }

    }











    public function up_password_check_strict($args)
    {
      if(
        $args->has('new_password') ||
        $args->has('old_password')

        ){
          return true;
        }
      else {
        return false;
      }
    }
}