<?php 

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Suburb;
class SuburbController extends Controller
{

    public function allSuburbs()
    {
        return response()->json(['suburbs' => Suburb::select('id','suburbName as suburb','areas_id as city_id','delivery_charges as delivery_charges')->get()]);
    }

    public function suburbById($id)
    {
        return response()->json(['status' => 200,'suburb' => Suburb::select('id','suburbName as suburb','areas_id as city_id','delivery_charges as delivery_charges')->where('id',$id)->get()->first()]);
    }

    public function suburbByCityId($id)
    {
        return response()->json(['status' => 200,'suburbs' => Suburb::select('id','suburbName as suburb','areas_id as city_id','delivery_charges as delivery_charges')->where('areas_id',$id)->get()]);
    }










}