<?php 

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Password;
use App\User;
class ForgotPasswordController extends Controller
{

    public function sendForgotMail(Request $request)
    {   
        
        $validator = Validator::make($request->all(),[
            'email' => 'required|email',
        ]);

        if($validator->fails())
        {
            return response()->json(['status' => 400, 'message' => $validator->messages()->first()]);
        }

        $user = User::where('email','=',$request['email'])->get()->first();
        
        if(!$user)
        {
            return response()->json(['status' => 400, 'message' => "User Doesnot Exist"]);
        }

        try 
        {
            $token = Password::getRepository()->create($user);
            $user->sendPasswordResetNotification($token);    
            return response()->json(['status' => 200, 'message' => "Mail Has Been Sent"]);
 
        } catch (\Throwable $th) 
        {
            return response()->json(['status' => 400, 'message' => "Failed To Send Email",'th' =>$th->getMessage()]);
        }
        



    }
}