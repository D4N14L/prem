<?php 

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Events\SendVerficationMail;
use \App\User;
use Hash;
class RegisterController extends Controller
{
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(),[
        'name'  => 'required|max:255',
        'email' => 'required|email|unique:users|max:255',
        'password' => 'required|max:255',
        'address' => 'required|max:255|unique:users',
        'suburb_id' => 'required|max:255',
        'landline' => 'required|max:15',
      ]);      
        
      if($validator->fails())
      {
        return response()->json(['status' => 400, 'message' => $validator->messages()->first()]);
      }

      $user = User::create($validator->valid());
      $user = User::find($user->id);
      $user->ethnic = "null";
      $user->referral_link = Hash::make($user->email.$user->name);
      $user->isAgreementSigned = 0;
      $user->verification_token = $this->rand_str();
      $user->api_token =  Hash::make($user->email);
      $user->save();
      event(new SendVerficationMail($user));
      return response()->json(['status' => 200, 'api_token' => $user->api_token,'message' => 'a verification email has been sent to'.$user->email]);
      
    }

    public function rand_str(){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 40; $i++) {
          $randstring .= $characters[rand(0, strlen($characters) - 2)];
        }
        return $randstring;
    }









}