<?php 

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Area;
class CityController extends Controller
{

    public function allCities()
    {
        return response()->json(['status' => 200,'cities' => Area::select('id','cityName as city')->get()]);
    }

    public function cityById($id)
    {
        return response()->json(['status' => 200, 'area' => Area::select('id','cityName as city')->where('id',$id)->get()->first()]);
    }





}