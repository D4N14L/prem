<?php 

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Area;
use App\Portions;
use App\Miscellaneous;
class MiscController extends Controller
{

    public function getEula()
    {

        try{

            return response()->json(['status' => 200, 'eula' => Portions::where('title','=','EULA')->get()->first()->body]);
        }catch(\Exception $e)
        {
            return response()->json(['status' => 400, 'message' => 'An error occured!']);
        }
    }

    public function getMinAmnt()
    {
        try{
            $misc = Miscellaneous::get()->first();
            return response()->json(['status' => 200, 
                                     'minAmnt' => $misc->minimumAmount,
                                     'minDelivery' => $misc->deliveryCharges
                                     ]);
        }catch(\Exception $e)
        {
            return response()->json(['status' => 400, 'message' => 'An error occured']);
        }
    }




}