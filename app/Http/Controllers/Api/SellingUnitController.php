<?php 

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use DB;
class SellingUnitController extends Controller
{   
    protected $getById = "
        SELECT * FROM SellingUnits where id = ?
    ";
    protected $getAll = "
        SELECT * FROM SellingUnits
    ";
    public function getById($id)
    {   
        $sellingUnit = collect(DB::select($this->getById,[$id]))->first();
        return response()->json(['status' => 200, 'selling_unit' => $sellingUnit]);
    }

    public function getAll()
    {
        $sellingUnit = collect(DB::select($this->getAll));
        return response()->json(['status' => 200, 'selling_units' => $sellingUnit]);
    }








}