<?php 

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use DB;
class SliderController extends Controller
{
    protected $slider_query = "
        SELECT image,arrange as sort_order FROM sliders
    ";

    public function get_all_sliders()
    {
        $sliders = collect(DB::select($this->slider_query));
        return response(['status' => 200, 'gallery' => $sliders]);
    }



}