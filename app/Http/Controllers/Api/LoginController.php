<?php 

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use \App\User;

class LoginController extends Controller
{
    public function login(Request $request)
    {
      $validator = Validator::make($request->all(),[
        'email' => 'required|email|max:255',
        'password' => 'required|max:255'
      ]);      
    
      if($validator->fails())
      {
        return response()->json(['status' => 400, 'message' => $validator->messages()->first()]);
      }
      
      if(!auth()->attempt($validator->valid()))
      {
        return response()->json(['status' => 400, 'message' => 'Invalid user name and password']);
      }
      else
      {

        $user = User::where('email','=',$request['email'])->get()->first();
        if($user->is_blocked)
        {
          return response()->json(['status' => 405, 'message' => 'Account is blocked by admin, please contact via info@premiummeat.co.nz']);
        }
        // elseif(!$user->isVerified)
        // {
        //   return response()->json(['status' => 405, 'message' => 'Please verify your email before logging in']);
        // }
        // elseif (!$user->is_blocked && $user->isVerified) {
        //   return response()->json(['status' => 200,'api_token' => auth()->user()->api_token,'message' => 'Verified']);
        // }
        elseif (!$user->is_blocked) {
          return response()->json(['status' => 200,'api_token' => auth()->user()->api_token,'message' => 'Verified']);
        }

      }
      
      
    }











}