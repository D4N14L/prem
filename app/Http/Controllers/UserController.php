<?php

namespace App\Http\Controllers;

use App\Events\SendVerficationMail;
use Illuminate\Http\Request;
use App\User;
use App\Suburb;
use Auth;
use Hash;
use Validator;
use Toastr;
class UserController extends Controller
{
    public function browse()
    {
      $user = User::where('email','<>','dev@support.com')
             ->where('is_admin_verified','=','1')
             ->where('is_blocked','=','0')
             ->orderBy('name','asc')
             ->paginate(500);
      return view('admin.users.browse')->withUsers($user);
    }
    public function browseBlock()
    {
      $user = User::where('email','<>','dev@support.com')->where('is_blocked','=','1')->orderBy('id','desc')->paginate(500);
      return view('admin.users.blocked')->withUsers($user);
    }
    public function browseUnverified()
    {
      $user = User::where('email','<>','dev@support.com')->where('is_blocked','=','0')->where('is_admin_verified','=','0')->orderBy('id','desc')->paginate(500);
      return view('admin.users.unverified')->withUsers($user);
    }


    public function resendmail($id)
    {
      try{

        $user = User::find($id);

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randstring = '';
        for ($i = 0; $i < 40; $i++) {
          $randstring .= $characters[rand(0, strlen($characters) - 2)];
        }
        $user->verification_token = $randstring;
        $user->save();
        event(new SendVerficationMail($user));
        Toastr::success('Verification mail has been resent');
        return redirect()->back();
  
      }
      catch(\Exception $e)
      {
        Toastr::error('Mail sending failed, contact info@premiummeat.co.nz');       
        return redirect()->back();

      }
    }

    public function block($id)
    {
      $user = User::findOrFail($id);
      $user->is_blocked = 1;
      $user->save();
      Toastr::success($user->email.' Has Been Blocked');
      return redirect()->back();
    }

    public function verify($id)
    {
      $user = User::findOrFail($id);
      $user->is_admin_verified = 1;
      $user->save();
      Toastr::success($user->email.' Has Been Verified');
      return redirect()->back();
    }

    public function unblock($id)
    {
      $user = User::findOrFail($id);
      $user->is_blocked = 0;
      $user->save();
      Toastr::success($user->email.' Has Been Unblocked');
      return redirect()->back();
    }

    public function unverify($id)
    {
      $user = User::findOrFail($id);
      $user->is_admin_verified = 1;
      $user->save();
      Toastr::success($user->email.' Has Been Unverified');
      return redirect()->back();
    }

    public function getUserManager()
    {
      $order = \App\Order::where('customer_id','=',Auth::user()->id)->orderBy('order_status','asc')->paginate(3);
      return view('userManager/v2/index')->with('orders',$order);
    }

    public function changePassword(Request $request)
    {
      try {
        $user  = User::findOrFail(Auth::user()->id);
        $user->password = Hash::make($request->password);
        Toastr::success('Password Changed Succesffully');
        $user->save();
        Toastr::success('Password Changed Succesffully');
        return redirect()->back();
      } catch (\Exception $e) {
        Toastr::error('An Occured While Changing UserName');
        return redirect()->back();
      }


    }

    public function emailVerification($verification_token)
    {
      if($verification_token)
      {

      $user = User::where('verification_token','=',$verification_token)->first();
      if($user)
      {
          try {
              $user->isVerified = 1;
              $user->verification_token = NULL;
              $user->save();
              session()->flash('success','Email Verified Succesffully!');
              return redirect()->route('login');
              
          } 
          catch (\Exception $e) 
          {
            session()->flash('error','Email Verified Succesffully!');
            return redirect()->route('login');
          }
      }
     else
      {
        session()->flash('error','Verification Token Expired!, Contact Administrator to regenerate token');
        return redirect()->route('login');
      }
    }
    else {
      echo 'Bad Request';
    }

    }
    public function updateProfile(Request $request)
    {
      if(!$request->has('password'))
      {
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:50',
            'address' => 'required|string|max:100',
            'landline' => 'required|max:15',
            'ethnic' => 'required',
            'suburb_id' => 'required',
        ],[
          'suburb_id.required' => 'Choose City & Suburb',
        ]);

        if($validator->fails()){
          session()->forget('failed');
          session()->flash('failed',$validator->messages()->first());
          return redirect()->back()->withInput();
        }
        User::unguard();
        $user = User::findOrFail(Auth::user()->id)->update($request->except('_token'));
        User::reguard();

        if($user){
          Toastr::success('Profile Updated Successfully');
          session()->flash('success','Profile Updated Successfully');
          return redirect()->back();
        }else
        {
          Toastr::success('Profile Updated Successfully');

          session()->flash('failed','Unable to updated profile');
          return redirect()->back();

        }
      }
      else
      {

        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:50',
            'address' => 'required|string|max:100',
            'landline' => 'required|max:15',
            'ethnic' => 'required',
            'password' => 'required|confirmed|min:6',
            'suburb_id' => 'required',
        ],[
          'suburb_id.required' => 'Choose City & Suburb',
        ]);

        if($validator->fails()){

          Toastr::error($validator->messages()->first());
          session()->forget('failed');
          session()->flash('failed',$validator->messages()->first());
          return redirect()->back()->withInput();
        }
        $data = $request->except(['_token','password_confirmation']);
        $data['password'] = Hash::make($data['password']);
        User::unguard();
        $user = User::findOrFail(Auth::user()->id)->update($data);
        User::reguard();

        if($user){
          Toastr::success('Profile Updated Successfully');
          // session()->flash('success','Profile Updated Successfully');
          return redirect()->back();
        }else
        {
          Toastr::error('Unable to updated profile');
          session()->forget('failed');
          session()->flash('failed','Unable to updated profile');
          return redirect()->back();

        }
      }


    }
    public function getEditForm(){
      return view('userManager.v2.edit');
    }

    public function edit($id)
    {
      $suburb = Suburb::orderBy('suburbName','asc')->get();
      $user = User::findOrFail($id);

      return view('admin.users.edit')
             ->with('user',$user)
             ->with('suburbs',$suburb);
    }

    public function update(Request $request,$id)
    {
      if($request->has('password')  )
      {
        if(strlen($request->password) > 8)
        {
          $request->merge(['password' => \Hash::make($request->password)]);
        }
        else 
        {
            Toastr::error('Password Must Be greater than 8 and less than 16 characters ');
            return redirect()->back();
        }
      }
      User::findOrFail($id)->update($request->except('_token'));
      Toastr::success('User Updated Successfully');
      return redirect()->back();

    }

    public function search(Request $request)
    {
      $q = $request->q;
      $user = User::where(function($query) use ($q){        
        $query->where('email','<>','dev@support.com')
              ->where('is_admin_verified','=','1')
              ->where('is_blocked','=','0');           
      })->where(function($query) use ($q){
            $query->orWhere('email','LIKE','%'.$q.'%')
            ->orWhere('name','LIKE','%'.$q.'%')
            ->orWhere('landline','LIKE','%'.$q.'%');
            
      })->orderBy('name','asc')->paginate(500);
      return view('admin.users.browse')->withUsers($user);

    }



    // public function resetPassword(Request $request){
    //   $errors = $request->validate(
    //     [
    //       'token' => 'required',
    //       'password' => 'required|confirmed|min:6',
    //     ]
    //   );

    // $user =  User::where('email','=',$request->email)->first();
    // $user->password = Hash::make($request->password);
    // $user->save();
    // session()->flash('success','Password Updated');
    // return redirect('/login');
    // }



















}
