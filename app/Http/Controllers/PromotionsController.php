<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Auth;
use App\User;
use Toastr;
class PromotionsController extends Controller
{
    public function getUsersPromotions()
    {
      $users = User::orderBy('id','desc')->paginate(20);
      return view('admin.promotions.index')->with('users',$users);
    }
    public function getUserReferalInformation($id)
    {
      $user = User::findOrFail($id);
      return view('admin.promotions.info')->with('user',$user);
    }
    public function postPromotion(Request $request)
    {
      try {
        $user = User::findOrFail($request->user_id);
        $user->promotion = $request->promote;
        if($user->save())
        {
          Toastr::success("Promotion Provided To User");
          return redirect()->back();
        }
        else
        {
          Toastr::error("Promotions Was Not Provided Due To Unknown Error!");
          return redirect()->back();
        }


      } catch (\Exception $e) {
        Toastr::error("Promotions Was Not Provided Due To Unknown Error!");
        return redirect()->back();

      }

    }
}
