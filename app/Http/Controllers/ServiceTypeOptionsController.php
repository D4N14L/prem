<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceTypeOptions;
use Toastr;
use Validator;
class ServiceTypeOptionsController extends Controller
{
    public function browse()
    {
      $ServiceTypeOptions = ServiceTypeOptions::orderBy('id','desc')->paginate(10);
      return view('admin.serviceTypeOptions.browse')->with('serviceOptionsTypes',$ServiceTypeOptions);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
          'name' => 'required | min:4',
          'cat_id' => 'required'
        ],[
          'name.required' => 'Please Enter Service Option Name',
          'name.min' => 'Name Must Be Of Minimum 4 Characters',
          'cat_id.required' =>'Choose Category'
        ]);
      if($validator->fails()){
        Toastr::error($validator->messages()->first());
        return redirect()->back();
      }





      if($request->is_edit == 0)
      {

        ServiceTypeOptions::create([
          'name' => $request->name,
          'service_type_id' => $request->service_type_id,
        ]);
      }
      else
      {
        $serviceTypesOptions = ServiceTypeOptions::find($request->service_id);

        $serviceTypesOptions->name = $request->name;
        $serviceTypesOptions->service_type_id = $request->service_type_id;
        $serviceTypesOptions->timestamps = false;
        $serviceTypesOptions->save();

      }

      return redirect()->route('serviceTypeOptions.browse');

    }

    public function destroy($id)
    {
    $serviceTypesOptions = ServiceTypeOptions::findOrFail($id);
    $serviceTypesOptions->delete();
    return redirect()->back();
    }

    public function getServiceTypes(Request $request)
    {
      if($request->id != 0)
      {
        $serviceTypesOptions = ServiceTypeOptions::where('service_type_id','=',$request->id)->get();
        return response()->json($serviceTypesOptions);
      }



    }

}
