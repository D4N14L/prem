<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceGroup;
use App\ServiceGroupOptions;
use App\ServiceType;
use Toastr;
class GroupServiceController extends Controller
{
    public function index()
    {
        $service_groups = ServiceGroup::orderBy('name','asc')->get();
        $service_options = ServiceType::orderBy('name','asc')->get();
        return view('admin.ServiceGroups.index')
                    ->with('service_groups',$service_groups)
                    ->with('service_options',$service_options);
    }

    public function linkGroups(Request $request)
    {
        try {

            $group_exists = ServiceGroupOptions::where('group_id',$request->group_id)->get();

            if($group_exists && count($group_exists) != 0)
            {
                foreach($group_exists as $g)
                {
                    $g->delete();
                }
            }
            foreach($request->service_options as $so)
                {
                    ServiceGroupOptions::create([
                        'group_id' => $request->group_id,
                        'service_id' => $so
                    ]);
                }    
            Toastr::success('Services Added to Group Successfully');
            return redirect()->back();
        } catch (\Throwable $th) {
            Toastr::error('An error occured');
            return redirect()->back();                
        }
    }

    public function view($id)
    {

    }

    public function store(Request $request)
    {   
        try {
            $service_group = new ServiceGroup();
            $service_group->create($request->except('_token'));
            Toastr::success('Service group created succesfully');
            return redirect()->back();    
        } catch (\Throwable $th) {
            Toastr::error('An error occured');
            return redirect()->back();                
        }
    }


    public function delete($id)
    {
        try {
            $group_exists = ServiceGroupOptions::where('group_id',$id)->get();

            if($group_exists && count($group_exists) != 0)
            {
                foreach($group_exists as $g)
                {
                    $g->delete();
                }
            }
    
            $service_group = ServiceGroup::find($id);
            $service_group->delete();
            Toastr::success('Service group created succesfully');
            return redirect()->back();        

        } catch (\Throwable $th) {
    
            Toastr::error('An error occured');
            return redirect()->back();                

        }

    }

}
