<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;
use App\Portions;
use Toastr;
use App\User;
use App\Events\CustomMail;
use Validator;
class CMSController extends Controller
{
    public function index()
    {
      return view('admin.cms.index');
    }


    public function addSlider(Request $request)
    {
      if($request->hasFile('slide'))
      {

        $slide =  $request->file('slide');
        $slideName = time().'.'.$slide->getClientOriginalExtension();
        $slide= \Image::make($request->file('slide'));
        $slide->resize(1366,850);
        if($slide->save(public_path('sliders/').$slideName,10))
        {
            $slider = new Slider();
            $slider->image = $slideName;
            $slider->timestamps = false;
            if($slider->save())
            {
              Toastr::success('Slide Added Successfully!');
            }
            return redirect()->back();
        }
        Toastr::error('Failed To Add Slider!');
        return redirect()->back();
      }
    }


    public function updatePortion(Request $request)
    {
      $portion = Portions::findOrFail($request->portion_id);
      if($request->has('title'))
      {
        $portion->title = $request->title;
      }
      $portion->body = $request->body;
      $portion->timestamps = false;
      if($request->hasFile('image'))
      {
        $image = $request->file('image');
        $imageName = time().'.'.$image->getClientOriginalExtension();
        $newImage = \Image::make($request->file('image'));
        $newImage->resize(1366,850);
        $newImage->save(public_path('extra/'.$imageName),10);
        $portion->image = $imageName;
      }
      if($portion->save())
      {
        if($portion->id == 4)
        {
          Toastr::success('Eula Updated Succesfully!');

        }
        else
        {
          Toastr::success('Portion'.$portion->id.' Updated Succesfully!');
        }
        return redirect()->back();

      }

    }

    public function manageSlides()
    {
        $slides = Slider::orderBy('id','desc')->paginate(15);
        return view('admin.cms.slides.manage')->with('slides',$slides);
    }

    public function postManageSlides(Request $request)
    {
      try {

        if($request->slides != "")
        {
          $slides = explode(',',$request->slides);
          foreach ($slides as $slide)
          {
            Slider::findOrFail($slide)->delete();
          }

          Toastr::success(count($slides).'Images Deleted Succesfully');
          return redirect()->back();
        }

      } catch (\Exception $e) {
        Toastr::error('Failed To Delete Succesfully!');
        return redirect()->back();

      }

    }

    public function updateFooter(Request $request)
    {
       foreach ($request->except(['_token']) as &$data) {
         $data = trim($data);
      }
      try {
        \App\FooterCMS::where('id','=','1')->update($request->except(['_token']));
        Toastr::success('Footer Data Updated Successfully!');
         return redirect()->back();
      } catch (\Exception $e) {
        Toastr::error('Footer Data Failed To Update!');
        return redirect()->back();
      }
    }


    public function updateWebsiteColor(Request $request)
    {
      try {
        $cms = \App\FrontendCMS::findOrFail(1);
        $cms->backgroundColor = $request->backgroundColor;
        $cms->save();
        Toastr::success('Frontend Color Updated Successfully!');
        return redirect()->back();

      } catch (\Exception $e) {
        Toastr::error('Unexpected Error Occured While Updating Website Color');
        return redirect()->back();

      }

    }


   public function updatePosition(Request $request)
   {
      try {
        foreach ($request->positions as $value) {
          \App\Slider::where('id',$value[0])->update(['arrange' => $value[1]]);
        }
        return response()->json(['status' => '200']);
      } catch (\Exception $e) {
        return response()->json(['status' => '500']);
      }

   }

   public function updateMiscellaneous(Request $request)
   {

     try {
       \App\Miscellaneous::first()->update($request->only(['deliveryCharges','minimumAmount']));
       Toastr::success('Delivery & Minumum Amounts Saved Sucessfully');
       return redirect()->back();
     } catch (\Exception $e) {
       Toastr::error('Unable to store Delivery & Minumum Amounts at the moment');
       return redirect()->back();

     }



   }

   public function emailmanager()
   {
      $email = User::all('email');
      return view('admin.cms.email.email')->with('emails',$email); 
   }

   public function postMail(Request $request)
   {
    try {

      $validator = Validator::make($request->all(),[
        'body'   => 'required',
        'subject' => 'required',
         'to' => 'required_without:is_all',
         'is_all' => 'required_without:to'
      ],[
        'to' => 'atleast one email should be given',
        'is_all' => 'atleast one email should be given',
        'subject' => 'subject of mail is required',
        'body' => 'body of the mail is required'
      ]);
     if($validator->fails())
     {
       Toastr::error($validator->messages()->first());
       return redirect()->back();
     }

      if($request->has('is_all') && $request->is_all)
      {
        $users = User::all();
        foreach($users as $user )
        {
          event(new CustomMail($user,$request->body,$request->subject));
        }
        Toastr::success('All the mails are added to queue. Will be sent eventually');        
        return redirect()->back();
      }else 
      {
        $users = User::whereIn('email',$request->to)->get();
        if(!$users)
        {
          Toastr::error('Can\'t Find Recipient(s) in our database. Please try again!');
          return redirect()->back();
        }
        else 
        {
          foreach($users as $user )
          {
            event(new CustomMail($user,$request->body,$request->subject));
          }
          Toastr::success('All the mails are added to queue. Will be sent eventually');
          return redirect()->back();

        }
      }
  
    } catch (\Throwable $th) {
      Toastr::error($th->getMessage());
      return redirect()->back();
    }
   }

}
