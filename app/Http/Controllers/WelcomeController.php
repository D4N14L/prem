<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Suburb;
use Identify;
class WelcomeController extends Controller
{
    public function welcome()
    {
        return view('welcome');
    }

    public function getSuburbByAreaId(Request $request)
    {
      $suburb = Suburb::where('areas_id','=',$request->area_id)->orderBy('suburbName','asc')->get();
      return response()->json($suburb);
    }

}
