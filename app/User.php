<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','ethnic','referral_link','address','landline','verification_token','suburb_id','stripeCustomerID'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    public function getNumberOfReferals()
    {
      $referal = \App\Referal::where('referer_id',$this->id)->get();
      if(!empty($referal))
      {
        return $referal->count();
      }
      else
      {
        return 0;
      }
    }

    public function getRefarals()
    {
      return \App\Referal::where('referer_id',$this->id)->get();
    }

      public function getLastOrderDate()
    {
      $order= \App\Order::where('customer_id','=',$this->id)->orderBy('created_at','desc')->first()->created_at;
      if(!empty($order))
      {
        return $order;
      }
      else
      {
        return null;
      }
    }
    public function getSuburbName()
    {   
      try {

        $suburb_id = \App\Suburb::where('id',$this->suburb_id)->get();
        if($suburb_id->count() > 0)
            return \App\Suburb::where('id',$suburb_id->first()->id)->first()->suburbName;
        else 
            return '';
      }catch(\Exception $e)
      {
        return '';
      }
    }
    public function getSuburbDeliveryCharges()
    {   
      try {

        $suburb_id = \App\Suburb::where('id',$this->suburb_id)->get();
        if($suburb_id->count() > 0)
            return \App\Suburb::where('id',$suburb_id->first()->id)->first()->delivery_charges;
        else 
            return 0;
      }catch(\Exception $e)
      {
        return 0;
      }
    }

    public function getAreaName()
    { 

        $suburb = \App\Suburb::where('id',$this->suburb_id)->get()->first();
        if($suburb)
        {
          return \App\Area::where('id','=',$suburb->areas_id)->get()->first()->cityName;
        }
        else {
          return '';
        }
    }
    public function getNumberOfOrders()
    {
      return \App\Order::where('customer_id','=',$this->id)->get()->count();
    }
    public function getOrderCompleted()
    {
      return \App\Order::where('customer_id','=',$this->id)->where('order_status','=',1)->get()->count();
    }
    public function getOrderPending()
    {
      return \App\Order::where('customer_id','=',$this->id)->where('order_status','=',0)->get()->count();
    }
    public function amountPaid()
    {
      return is_null(DB::select("SELECT sum(total_actual_amount) as 'amount_paid' from orders WHERE customer_id = ?",[$this->id])[0]->amount_paid) ? '0' : DB::select("SELECT sum(total_actual_amount) as 'amount_paid' from orders WHERE customer_id = ?",[$this->id])[0]->amount_paid;
    }

    public function broadcastAs()
    {
      return '.App.User'.$this->id;
    }

    public function recievesBroadcastNotificationsOn() {
      return 'App.User.'.$this->id;
    }

}
