<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderInfo extends Model
{
  protected $table = "orders_info";
  public $timestamps = false;

  public function Product()
  {
      return \App\Product::where('id','=',$this->product_id)->first();
  }
  public static function getOrder($id)
  {
    return \App\Order::where('id','=',$id)->first();
  }
  public static function  getProduct($id)
  {
    return \App\Product::where('id','=',$id)->first();
  }
  public function getCustomerAddress()
  {
    return \App\Order::where('id','=',$this->order_id)->first()->User()->address;
  }
  // public function getTimeSlot()
  // {
  //   return \App\TimeSlot::where('id','=',$this->timeslotsSelection)->first();
  // }
}
