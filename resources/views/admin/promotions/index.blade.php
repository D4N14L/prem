@extends('voyager::master')


@section('page_header')
  <h1 class="page-title">
      <i class="voyager-bomb"></i>
      <p> {{ 'Promotions' }}</p>
  </h1>
  <span class="page-description">{{ 'Browse Users Promotions' }}</span>
  @include('toaster')

@endsection



@section('content')
  <style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }

  </style>
  <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}"></script>
  <div class="container">
    <div class="page-content">
      <div class="pull-right">
        <center> {!! $users->render() !!} </center>
      </div>

      <table class="table table-responsive table-bordered table-striped">
        <thead>
          <th>User Name</th>
          <th>User Email</th>
          <th>Total User Referals</th>
          <th>View User Referals Information</th>
          <th>Assign Promotions</th>
        </thead>
        <tbody>
          @foreach ($users as $user )
          <tr>
              <td>{{ $user->name }}</td>
              <td>{{ $user->email }}</td>
              <td>{{ $user->getNumberOfReferals() }}</td>
              <td> <a href="{{ route('promotions.info.users',['id' => $user->id]) }}" role="button " class="btn btn-info" style="text-decoration:none"> <i class="voyager-info-circled"></i> View Info </a> </td>
              <td>
                <form action="{{ route('promotions.promote') }}" method="post">
                  {{ csrf_field() }}
                  <div class="form-group" style="display:flex">
                    $ &nbsp;
                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                    <input type="number" class="form-control" name="promote" value="{{ $user->promotion }}" min="0"/>
                  </div>
                    <button type="submit" class="btn btn-success pull-right">Give Promotion</button>

                </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>

  </div>

@endsection
