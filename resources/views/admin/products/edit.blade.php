@extends('voyager::master')
@section('page_header')

  <h1 class="page-title">
      <i class="voyager-bag"></i>
      <p> {{ 'Products' }}</p>
  </h1>
  <span class="page-description">{{ 'Edit Products' }}</span>

@endsection
{{-- products.update --}}

@section('content')
  <style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }
    .checkbox-serviceoptions{
      margin:5px;
      padding:12px;
      color:black;
    }
  .service_type_grid{
     display: grid;
     grid-template-columns: repeat(7,2fr);
     justify-content: space-between;
     background-color:white;
     padding:12px 24px;
  }
  @media screen and (max-width:768px){
    .service_type_grid{
           grid-template-columns: repeat(4,2fr) !important;
    }
  }

  </style>

<div class="container">
  <div class="page-content">
    <div class="row">

      <form  action="{{ route('products.update') }}" enctype="multipart/form-data" method="post">

        {{ csrf_field() }}
        {{ method_field('post') }}

        <input type="hidden" name="product_id" value="{{$product->id}}">
        <div class="form-group col-sm-12 col-md-12 col-lg-12">
          <label for="">Product Name</label>
          <input type="text" name="name" value="{{ $product->name }}" class="form-control" />
        </div>


        <div class="form-group col-sm-12 col-md-12 col-lg-12">
          <label for="">Product Description</label>
          <textarea name="description" class="form-control" rows="8" cols="80">{{$product->description}}</textarea>
        </div>



        <div class="form-group col-sm-12 col-md-12 col-lg-12">
          <label for="cat_id">Select Category</label>
          <select  id="cat_id" class="form-control" name="cat_id">
            <option value="">Select Category</option>
            @foreach (\App\ProductCategory::all() as $category)
              @if($product->cat_id == $category->id)
              <option value="{{ $category->id }}" selected>  {{ $category->name }}</option>
              @else
                <option value="{{ $category->id }}">{{ $category->name }}</option>
              @endif
            @endforeach
          </select>
        </div>

        <div class="form-group col-sm-12 col-md-12 col-lg-12">
          <label for="cat_id">Select Service Options ( Check The One's Which You Want To Show To The User )</label>
          <div class="service_type_grid">
            @php $exploded = explode(",",$product->service_groups);@endphp
            @foreach (\App\ServiceGroup::where('cat_id','=',$product->cat_id)->get() as $service)
              @if(!in_array($service->id,$exploded))
                <div class="checkbox-serviceoptions"><input type="checkbox"  name="service_groups[]" value="{{$service->id}}"> {{$service->name}}</div>
              @else
                <div class="checkbox-serviceoptions"><input type="checkbox" checked  name="service_groups[]" value="{{$service->id}}"> {{$service->name}}</div>
              @endif
            @endforeach
          </div>
        </div>




        <div class="form-group col-sm-12 col-md-4 col-lg-4">
          <label for="timeslots">Select Delievery Availability</label>
          <select id="timeslots" class="form-control" name="timeslot">
            <option value="">Select Delievery Availability Slots</option>
            <optgroup label="General">
              @if($product->timeslot == "0")
                <option selected value="0">General</option>
              @else
                <option  value="0">General</option>
              @endif
            </optgroup>
              <optgroup label="Morning">
                @foreach (\App\TimeSlot::where('shift','=','morning')->get() as $timeslot)
                  @if($product->timeslot == '7')
                  <option selected value="{{ $timeslot->id }}">{{ $timeslot->startTime }} - {{ $timeslot->endTime }}</option>
                  @else
                    <option value="{{ $timeslot->id }}">{{ $timeslot->startTime }} - {{ $timeslot->endTime }}</option>
                  @endif
                @endforeach
              </optgroup>
              <optgroup label="Evening">
                @foreach (\App\TimeSlot::where('shift','=','evening')->get() as $timeslot)
                  @if($product->timeslot == '8')
                  <option selected value="{{ $timeslot->id }}">{{ $timeslot->startTime }} - {{ $timeslot->endTime }}</option>
                  @else
                    <option value="{{ $timeslot->id }}">{{ $timeslot->startTime }} - {{ $timeslot->endTime }}</option>
                  @endif
                @endforeach
              </optgroup>
          </select>
        </div>

        <div class="form-group col-sm-4 col-md-4 col-lg-4">
          <label for="sellingUnit">Select Selling Unit</label>
          <select id="sellingUnit" onchange="togglePieceAvgWeight(this)" class="form-control" name="sellingUnit_id">
            <option value="">Select Selling Unit</option>
            @foreach (\App\SellingUnit::all() as $su)
              @if($su->id == $product->sellingUnit_id )
                <option selected value="{{ $su->id }}">{{ $su->name }}</option>
              @else
                <option value="{{ $su->id }}">{{ $su->name }}</option>
              @endif
            @endforeach
          </select>
        </div>


        <?php $isPiecesCategory = (strtolower($product->sellingUnit->name) === "piece" || strtolower($product->sellingUnit->name) === "pieces");  ?>
        <div class="form-group col-sm-12 col-md-4 col-lg-4" id="piece-average-weight" style="display:{{$isPiecesCategory ? 'block':'none'}}">
          <div class="form-group">
            <label for="avgPerPieceInput">Average Weight Of Piece</label>
            <input id="avgPerPieceInput" {{!$isPiecesCategory ? 'disabled':''}} type="text" required placeholder="Average Weight Of Per Piece e.g 2.5 kg"  name="avg_weight_piece" class="form-control" required value="{{$isPiecesCategory ? $product->avg_weight:""}}">
          </div>
        </div>


        <div class="form-group col-sm-12 col-md-4 col-lg-4">
          <div class="form-group">
            <label for="price">Price</label>
            <input id="price" type="text" placeholder="Total Price" name="price" class="form-control" value="{{ $product->price }}" required />
          </div>
        </div>


        <div class="form-group col-sm-12 col-md-6 col-lg-6">
          <label for="product_image">Product Image</label>
          <input id="product_image" onchange="getImage(this)" accept="image/*" type="file" name="image" class="form-control" />
        </div>

        <div class="form-group col-sm-12 col-md-6 col-lg-6">
          <img id="thumbnail" src="{{ URL::to('/assets/products/'.$product->image) }}" width="100px" height="100px" style="object-fit:cover" alt="" />
        </div>

          <button type="submit" class="btn btn-success btn-lg pull-right"> <i class="voyager-plus"></i> Update Product </button>


      </form>
















    </div>
  </div>
</div>
<script type="text/javascript">
function getImage($el)
{
  var fileReader = new FileReader();
  fileReader.readAsDataURL($el.files[0])
  fileReader.onload = function()
  {
    document.getElementById('thumbnail').src = fileReader.result;
  }
}
function togglePieceAvgWeight($el){
    let avgWeight = document.querySelector('#piece-average-weight');
    let avgWeightInput = document.querySelector('#avgPerPieceInput');
    let selectedIndex = $el.options.selectedIndex;
    let selectedIndexValue = $el.options[selectedIndex].innerHTML;
    if(selectedIndexValue.toLowerCase() == "pieces" || selectedIndexValue.toLowerCase() == "piece"){
        avgWeight.style.display = "block";
        avgWeightInput.disabled = false;
    }else {
      avgWeight.style.display = "none";
      avgWeightInput.disabled = true;

    }

}

function fetchTimeslots($el){
  let cat_id = $el.options[$el.selectedIndex].value;
  let service_type_grid = document.querySelector('.service_type_grid');
  axios.get('{{route("product.fetch.services")}}',{
    params:{
      cat_id
    }
  }).then( ({data}) => {
        if(data.length <= 0)
        {
          service_type_grid.innerHTML = "<b><i>No Service Options Are Mentioned For This Category</i></b>";
          service_type_grid.style.gridTemplateColumns = "repeat(1,1fr)";
        }else {
          service_type_grid.style.gridTemplateColumns = "repeat(7,1fr)";
          let services = "";
          data.forEach( service => {
            services += `<div class="checkbox-serviceoptions"><input type="checkbox"  name="service_groups[]" value="${service.id}"> ${service.name}</div>`;
          });
          service_type_grid.innerHTML = services;
        }
  }).catch(error => console.log( error ))
}
</script>

@endsection
