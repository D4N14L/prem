@extends('voyager::master')

@section('page_header')
    <link rel="stylesheet" href="{{ URL::to('/')}}/css/multi-select.css">
    <h1 class="page-title">
        <i class="voyager-wallet"></i>
        <p> {{ 'Service Group' }}</p>
    </h1>
    <span class="page-description">{{ 'Service Groups' }}</span>

    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@endsection

@section('content')
@php $backgroundColor = \App\FrontendCMS::first()->backgroundColor @endphp 
<style>
.multi-wrapper .item
{
    text-decoration:none;
    color:{{$backgroundColor}};
}
.multi-wrapper .item:hover{
    color:white;
    background:{{$backgroundColor}};
}


.multi-wrapper .non-selected-wrapper .item.selected{
    background:{{$backgroundColor}};
    color:white;

}
.multi-wrapper .non-selected-wrapper .item.selected:hover{
    background:{{$backgroundColor}};
    color:white;

}
</style>
<div class="container">
   
        <form action="{{ route('groupservice.store') }}" method="post">
                <div class="row">
                {{ csrf_field() }}
                    <div class="col-sm-4">
                            <label for="">Name of Group</label>
                        <input type="text" name="name" class="form-control" required placeholder="New Service Group">
                    </div>
                    <div class="col-sm-4">
                        <label for="">Select Category</label>
                        <select class="form-control" required name="cat_id">
                            @foreach(\App\ProductCategory::orderBy('name','asc')->get() as $cat)
                            <option value="{{$cat->id}}">{{ $cat->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-4" >
                    <br/>
                        <button type="submit"  class="btn btn-success"> Create  </button>
                    </div>
                </div>
                </form>
            <hr>
        
    <table class="table table-bordered table-striped">
        <thead>
                <th>Group Name</th>
                <th>Group Category</th>
                <th>Service Option(s)</th>        
                <th>Add Service Options to group</th>
                <th>Delete</th>
        </thead>

        <tbody>
                
                @foreach($service_groups as $service_group)
                <tr>
                @php 

                    $groupServices = \App\ServiceGroupOptions::where('group_id',$service_group->id)->get()->pluck('service_id')->toArray();
                    if(!$groupServices || count($groupServices) == 0)
                    {
                        $groupServices = [0];
                    }
                @endphp
                <td>{{ $service_group->name }}</td>
                <td>{{ \App\ProductCategory::find($service_group->cat_id)->name }}</td>
                @php    $services = \App\ServiceGroupOptions::where('group_id',$service_group->id)->get()->pluck('service_id')->toArray();
                         $services = \App\ServiceType::whereIn('id',$services)->get()->pluck('name')->toArray();
                         $services = implode('</li><li>',$services);
                @endphp
                <td>
                    <ul>
                    <li>
                        {!! $services !!}
                    </li>
                    </ul>
                </td>
                <td><a class="btn btn-primary" data-toggle="modal" data-target="#link_service_options-{{$service_group->id}}"  style="text-decoration:none">Link Service Option</a>
                    
                <div id="link_service_options-{{$service_group->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                          
                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                    <h4>Add Service Options To Group {{ $service_group->name }}</h4>
                                    <hr>
                                    <p>Choose from left box which service types you want to add</p>
                                </div>
                                <div class="modal-body">

                                    <form action="{{route('groupservice.linkgroup')}}" method="post">
                                    {{ csrf_field() }}
                                        <div class="form-group">
                                            <input type="hidden" name="group_id" value="{{ $service_group->id }}">
                                            
                                            <select class="form-control" name="service_options[]" multiple>
                                                @foreach($service_options as $service_option)
                                                        @if(in_array($service_option->id,$groupServices))
                                                        <option selected value="{{ $service_option->id }}">{{ $service_option->name }}  </option>
                                                        @else 
                                                        <option value="{{ $service_option->id }}">{{ $service_option->name }} </option>
                                                        @endif
                                                 @endforeach
                                            </select>

                                            <button class="btn btn-success" type="submit">Save</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                          
                            </div>
                          </div>
                
                
                
                
                </td>
                <td>
                    <a href="{{ route('admin.service.groups.delete',['id' => $service_group->id]) }}" class="btn btn-danger" style="text-decoration:none"> Delete </a>
                </td>
                
        </tr>
                @endforeach
        </tbody>



    </table>
</div>





<script src="{{ URL::to('/')}}/js/multi-select.js" defer="true"></script>
<script>
$(document).ready(function(){
    $('select').multi()
})
</script>


@endsection

