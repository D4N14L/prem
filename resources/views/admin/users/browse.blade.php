@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-person"></i>
        <p> {{ 'Users' }}</p>
    </h1>
    <span class="page-description">{{ 'Showing Verified & Unblocked Users ' }}</span>
    <h1 class="page-title pull-right">
      Total Active Users: {{ $users->total() }}</p> 
    </h1>


@endsection

@section('content')

    @php 

    function expected($var)
    {
      return '<i style="font-size:9px;font-weight:bold">'.$var.'</i>';

    }



    @endphp
<style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }

  </style>
<div class="container" >
  <div class="page-content">

      <form action="{{route('admin.users.index')}}" method="post" style="width:60%">
          {{csrf_field()}}
          <div style="display:flex;justify-content:space-between;align-items:center">
            <input type="text" class="form-control pull-right" name="q" value=""  placeholder="Search User via email,username or number">
            &nbsp;&nbsp;
            &nbsp;
            <button  class="btn btn-info" type="submit">
                Search Users
              </button>    
              &nbsp;
              &nbsp;
              &nbsp;
            <a href="{{route('voyager.users.index')}}"  class="btn btn-info">Display All Active Users</a>
          </div>
        </form>
        <br><br>
      {{-- <a href="" class="btn btn-info">All User</a> --}}
    <table style="margin-top:-40px" class="table table-responsive table-striped table-bordered">
      <thead>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        {{-- <th>Ethnic</th> --}}
      
        <th>Placed Ago	</th>
        <th>Address</th>
        {{-- <th>Total Orders</th> --}}
        <th>Completed Orders</th>
        {{-- <th>Pending Orders</th> --}}
        <th>Total Payment So Far</th>
        {{-- <td>Send Verif. Mail</td> --}}
        {{-- <th>Email Verified</th> --}}
        <th style="text-align:center">Action</th>
        {{-- <th>Action</th> --}}
      </thead>
        <tbody>
          @foreach($users as $user)
          <?php $iSVerified = $user->isVerified ?>
            <tr>
              <td> {{ $user->name }} </td>
              <td> {{ $user->email }} </td>
              <td> {{ $user->landline }} </td>
              {{-- <td> {{ $user->ethnic }} </td> --}}
             <td style="white-space: nowrap">
                {{ \Carbon\Carbon::parse($user->created_at)->format('Y-m-d H:i A') }}
             </td>
              <td> {{ $user->getAreaName().','.$user->getSuburbName().','.$user->address }} </td>
              {{-- <td> {{ $user->getNumberOfOrders() }} </td> --}}
              <td> {{ $user->getOrderCompleted() }} </td>
              {{-- <td> {{ $user->getOrderPending() }} </td> --}}
              <td>${{ round($user->amountPaid(),2) }} / NZ </td>
              <td style="white-space: nowrap">
                <div style="display:flex;justify-content:space-between">
                  @if(!$iSVerified)

                  <center>
                      <a style="text-decoration:none" href="{{route('mail.resend',['id' => $user->id])}}"  class="btn btn-success">
                    <i style="font-size:18px" class="voyager-mail"></i>
                  </a>
                  &nbsp;
                </center>

                  @else 
                  <center>
                      <a style="text-decoration: none;background:transparent;border:none;background-color:transparent;margin:0;border:0" href="#" class="btn btn-success">
                        <span class="voyager-check" style="color:#2ecc71;font-size:24px;font-weight:bold" ></span>                
                      </a>
                  </center>
                  @endif
                  {{-- <td> <a href="{{route('admin.users.verify',['id' => $user->id])}}" type="button" class="btn btn-primary"> Verify </a> </td> --}}
                {{-- <td>
                  @if(!$iSVerified)
                  <span > <i style="color:orange">Not Verified</i> </span>
                @else
                  @php $shouldDeliver = true @endphp
                  <span> <i style="color:green">Verified</i> </span>
                @endif --}}
                  <a style="text-decoration:none" href="{{route('admin.users.block',['id' => $user->id])}}" type="button" class="btn btn-danger"> Block </a> 
                  &nbsp;
                <a style="text-decoration:none" href="{{route('admin.users.edit',['id' => $user->id])}}" type="button" class="btn btn-info"> Edit </a> 

                </div>

              </td>
            </tr>
          @endforeach
        </tbody>












    </table>
    <div class="pull-right">
      {!! $users->render() !!}
    </div>
  </div>

</div>
@endsection
