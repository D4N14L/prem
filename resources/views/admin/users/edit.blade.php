@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-person"></i>
        <p> {{ 'Edit Users' }}</p>
    </h1>
    <span class="page-description">{{ "Editing $user->name" }}</span>


@endsection

@section('content')

    @php 

    function expected($var)
    {
      return '<i style="font-size:9px;font-weight:bold">'.$var.'</i>';

    }



    @endphp
<style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }

  </style>

<div class="container" style="background-color:white;padding:42px 42px">
<a href="{{route('voyager.users.index')}}" class="btn btn-info"> Back </a>
<br>
<br>
<div class="page-content" >
    <form action="{{route('admin.users.update',['id' => $user->id])}}" method="post">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-sm-6">
                <label for="">Name</label>
                <input type="text" name="name" class="form-control" value="{{ $user->name }}" required />
            </div>
            <div class="col-sm-6">
                    <label for="">Email</label>
                <input type="email" name="email" class="form-control" value="{{ $user->email }}" required />
            </div>
            <div class="col-sm-6">
                <label for="">Suburb</label>
                <select name="suburb_id" class="form-control" id="" required>
                    @foreach($suburbs as $suburb)
                        @if($suburb->id == $user->suburb_id)
                            <option selected value="{{ $suburb->id }}">{{ $suburb->suburbName }}</option>
                        @else 
                            <option  value="{{ $suburb->id }}">{{ $suburb->suburbName }}</option>
                        @endif
                    @endforeach
                </select>    
            </div>
            <div class="col-sm-6">
                <label for="">Address</label>
                <input type="text" class="form-control" name="address" value="{{ $user->address }}" required/>
            </div>
            <div class="col-sm-6">
                <label for="">Landline</label>
                <input type="text" class="form-control" name="landline" value="{{ $user->landline }}" required/>
            </div>
            <div class="col-sm-6">
                <button onclick="togglePassword(this,event)" id="showPassword" class="btn btn-danger">Enable Password Change</button>
                <input type="password" id="password_change" min="8" max="16" disabled name="password" placeholder="Type New Password" required class="form-control" />        
            </div>            
            <div class="col-sm-12">
                <button class="btn btn-success pull-right"  type="submit">Update</button>
            </div>
        </div>
    </form>

  </div>
</div>
@endsection
@section('javascript')
<script>
        function togglePassword($el,event)
        {
            event.preventDefault()
            let password_field = document.querySelector('#password_change');
            if(password_field.disabled)
            {
                password_field.disabled = false
                $el.innerHTML = "Disabled Password Change"
            } else 
            {
                password_field.disabled = true
                $el.innerHTML = "Enable Password Change"
            }
        }   
    </script>
    


@endsection