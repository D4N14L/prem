@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-person"></i>
        <p> {{ 'Users' }}</p>
    </h1>
    <span class="page-description">{{ 'Showing Blocked Users ' }}</span>
    <h1 class="page-title pull-right">
      Total Blocked Users: {{ $users->total() }}</p> 
    </h1>


@endsection

@section('content')

    @php 

    function expected($var)
    {
      return '<i style="font-size:9px;font-weight:bold">'.$var.'</i>';

    }



    @endphp
<style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }

  </style>
<div class="container">
  <div class="page-content">

    {{-- <div class="form-group" >
        <input type="text" class="form-control pull-right" name="" value="" style="width:40%; padding:24px 8px" placeholder="Search User">

        <br>
        <br>

    </div> --}}

    <table class="table table-responsive table-striped table-bordered">
      <thead>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        {{-- <th>Ethnic</th> --}}
      
        <th>Placed Ago	</th>
        <th>Address</th>
        {{-- <th>Total Orders</th> --}}
        <th>Completed Orders</th>
        {{-- <th>Pending Orders</th> --}}
        <th>Total Payment So Far</th>
        {{-- <td>Send Verif. Mail</td> --}}
        {{-- <th>Email Verified</th> --}}
        <th style="text-align:center">Action</th>
        {{-- <th>Action</th> --}}
      </thead>
        <tbody>
          @foreach($users as $user)
          <?php $iSVerified = $user->isVerified ?>
            <tr>
              <td> {{ $user->name }} </td>
              <td> {{ $user->email }} </td>
              <td> {{ $user->landline }} </td>
              {{-- <td> {{ $user->ethnic }} </td> --}}
             <td style="white-space: nowrap">
                {{ \Carbon\Carbon::parse($user->created_at)->format('Y-m-d H:i A') }}
             </td>
              <td> {{ $user->getAreaName().','.$user->getSuburbName().','.$user->address }} </td>
              {{-- <td> {{ $user->getNumberOfOrders() }} </td> --}}
              <td> {{ $user->getOrderCompleted() }} </td>
              {{-- <td> {{ $user->getOrderPending() }} </td> --}}
              <td>${{ round($user->amountPaid(),2) }} / NZ </td>
              <td> <a href="{{route('admin.users.unblock',['id' => $user->id])}}" type="button" class="btn btn-success"> Unblock </a> </td>
            </tr>
          @endforeach
        </tbody>












    </table>
    <div class="pull-right">
      {!! $users->render() !!}
    </div>
  </div>

</div>
@endsection
