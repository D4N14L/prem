@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-category"></i>
        <p> {{ 'Category' }}</p>
    </h1>
    <span class="page-description">{{ 'Edit Product Categories' }}</span>
@endsection

@section('content')
  <style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }

  </style>

  <div class="container">
    <div class="page-content">

      <div class="row">
        <form class="" action="{{ route('category.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('post') }}
            <div class="col-sm-12 col-md-6 col-lg-6 form-group">
                <label for="name">Category Name</label>
                <input type="text" class="form-control" style="padding:24px" name="name" value="{{$category->name}}" placeholder="e.g Chicken" required/><br>
                <button type="submit" class="btn btn-success pull-left"> <i class="voyager-edit"></i> Edit Category </button>
            </div>

            <div class="col-sm-12 col-md-3 col-lg-3 form-group" >
              <label for="image">Image</label>
                <input  type="file" accept="image/*" onchange="getImage(this)" class="form-control"  name="image" value="" required />
              </div>

              <div class="col-sm-12 col-md-3 col-lg-3 form-group">
                <label>Thumbnail</label>
                  <img id="thumbnail" src="{{ URL::to('/assets/category/'.$category->image) }}" style="width:100px;height:100px;object-fit:cover;display:block" alt="">

                </div>


        </form>

      </div>

@endsection
