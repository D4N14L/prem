@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-world"></i>
        <p> {{ 'Areas' }}</p>
    </h1>
    <span class="page-description">{{ 'Service Areas' }}</span>
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

@endsection

@section('content')
  <style media="screen">
    a{
      text-decoration: none !important;
    }
  </style>
  <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}"></script>

<div class="container">
  <div class="page-content">
    <div class="row">
      <div class="col-sm-12 col-lg-5 col-md-5">
          <h4>Add City</h4>
          <hr style="display:block" />
          <div style="display:flex;justify-content:space-between;width:100%;align-items:center">
            <input class="form-control" id="getCityInput" style="width:100%;" type="text" name="cityName" placeholder="Enter City Name To Add" value="">
          &nbsp;  <button type="button" onclick="cityInputHandler()" class="btn btn-primary " > <i class="voyager-plus"></i> Add City </button>
          </div>
          <div >
            <br>

              <table class="table table-responsive table-bordered table-striped">
                <thead>
                  <th>CityName</th>
                  <th>Created</th>
                  <th>Last Updated</th>
                  <th>Action</th>
                </thead>
                <tbody>
                  @foreach(\App\Area::orderBy('cityName','asc')->get() as $city)
                    <tr>
                      <td>{{$city->cityName}}</td>
                      <td>{{$city->created_at->diffForHumans()}}</td>
                      <td>{{$city->updated_at->diffForHumans()}}</td>
                      <td>
                        <button onclick="toggleEditAreaModal('{{$city->cityName}}','{{$city->id}}')" type="button" class="btn btn-info" > <i class="voyager-pencil"></i> Edit </button>
                        <a type="button" class="btn btn-danger" href="{{route('area.delete',['id' => $city->id])}}" > <i class="voyager-trash"></i> Delete </a>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
          </div>

      </div>
      <div  class="col-sm-12 col-lg-6 col-md-6 col-lg-offset-1 col-md-offset-1" style="border-left:2px solid #ddd;height:100%;margin-left:20px; padding-left:25px">



        <h4>Add Suburb</h4>
        <hr style="display:block" />
        <div style="display:flex;justify-content:space-between;width:100%;align-items:center">
          <input class="form-control" id="suburbNameInput"  type="text" name="cityName" placeholder="Enter Suburb" />&nbsp;
          <input class="form-control" id="suburbDeliveryCharges"  type="text" name="suburb_delivery_charges" placeholder="Delivery Charges" />&nbsp;
          {{-- <button type="button" onclick="cityInputHandler()" class="btn btn-primary " > <i class="voyager-plus"></i> Add City </button> --}}
          <select class="form-control" id="citiesList"  required name="cityNames">
              <option disabled selected >Select City</option>
                @foreach(\App\Area::orderBy('cityName','asc')->get() as $city)
                  <option value="{{$city->id}}"> {{$city->cityName}} </option>
                @endforeach
          </select> &nbsp;
          <button class="btn btn-primary" onclick="manageSuburbs()"  name="button"> <i class="voyager-plus"></i> Add Suburb </button>

        </div>
        <div class="">
          <br>

          <table class="table table-responsive table-bordered table-striped">
            <thead>
              <th>CityName</th>
              <th>Delivery Charges</th>
              <th>Created</th>
              <th>Last Updated</th>
              <th>Area</th>
              <th>Action</th>
            </thead>
            <tbody>
              @foreach(\App\Suburb::orderBy('suburbName','asc')->get() as $suburb)
                <tr>
                  <td>{{ $suburb->suburbName }}</td>
                  <td>{{ $suburb->delivery_charges }}</td>
                  <td>{{ $suburb->created_at->diffForHumans() }}</td>
                  <td>{{ $suburb->updated_at->diffForHumans() }}</td>

                  <td>{{$suburb->area->cityName}}</td>
                  <td>
                    <button type="button" onclick="toggleEditSuburbModal('{{$suburb->id}}','{{$suburb->suburbName}}','{{$suburb->area->id}}','{{$suburb->delivery_charges}}')" class="btn btn-info" > <i class="voyager-pencil"></i> Edit </button>
                    <a type="button" href="{{route('suburb.delete',['id' => $suburb->id])}}" class="btn btn-danger" > <i class="voyager-trash"></i> Delete </a>
                  </td>

                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>

    </div>
  </div>
</div>

{{--


  - Modal
  -- AREA EDIT
  --- #editArea



 --}}

<div class="modal fade" id="editArea" tabindex="-1" role="dialog" aria-labelledby="editArea" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="{{route('area.update')}}" method="post">
      {{csrf_field()}}
    <div class="modal-content">
      <div class="modal-body">
          <h5 class="modal-title" id="exampleModalLabel" style="font-weight:600;font-size:18px">Edit / Update City Name</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> <br>

            <label style="font-weight:600" for="modal_area_name">City Name</label>
            <input class="form-control" type="text" id="modal_area_name" name="areaName" placeholder="Enter City Name" />
            <input type="hidden"  name="area_id" id="modal_area_id">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>

    </div>
  </form>

  </div>
</div>











{{--


  - Modal
  -- Suburb EDIT
  --- #editSuburb



 --}}

<div class="modal fade" id="editSuburb" tabindex="-1" role="dialog" aria-labelledby="editArea" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <form action="{{route('suburb.update')}}" method="post">
      {{csrf_field()}}
    <div class="modal-content">
      <div class="modal-body">
          <h5 class="modal-title" id="exampleModalLabel" style="font-weight:600;font-size:18px">Edit / Update Suburb Name</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button> <br>
            <div style="display:flex;width:100%">
              <div class="">
                <label for="" style="font-weight:600">Suburb Name</label>
                <input class="form-control" type="text" id="modal_suburb_name" name="suburbName" placeholder="Enter Suburb Name" />
                <input class="form-control" type="text" id="modal_suburb_delivery_charges" name="delivery_charges" placeholder="Enter Delivery Charges" />
              </div>&nbsp;&nbsp;
              <div class="">
                <label for="" style="font-weight:600">Select City</label>
                <select class="form-control" id="modal_cities_list" required name="suburbCityNames">
                    <option disabled selected >Select City</option>
                      @foreach(\App\Area::orderBy('id','asc')->get() as $city)
                        <option value="{{$city->id}}"> {{$city->cityName}} </option>
                      @endforeach
                </select>
              </div>
            </div>

            <input type="hidden"  name="suburub_id" id="modal_suburb_id">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>

    </div>
  </form>

  </div>
</div>


















  <script type="text/javascript">


    function cityInputHandler(){
      let cityName =  document.querySelector('#getCityInput').value.trim();
      if(cityName.length >= 0){
          axios.post(`{{URL::to("/")}}/admin/areas/add/city`,{
            cityName,
            _token:"{{csrf_token()}}",
            _method:"post"
          }).then( ({data}) => {
            if(data.status == 200){
              toastr.success('City Name Added Successfully!');
              setTimeout(function(){
                window.location = window.location.href
              },150)

            }else {
              toastr.error(data.errMsg);
            }
          })
      }else {
        toastr.error('City Name Cannot Be Empty');
      }
    }
  </script>
  {{-- Manage Suburbs --}}
  <script type="text/javascript">
        function manageSuburbs(){
            let suburbName = document.querySelector('#suburbNameInput').value.trim();
            let delivery_charges = parseInt(document.querySelector('#suburbDeliveryCharges').value.trim());

            let cityList = document.querySelector('#citiesList');
            let selectedIndex = cityList.selectedIndex;
            let selectedCityID = cityList.options[selectedIndex].value;


            axios.post(`{{URL::to("/")}}/admin/areas/add/suburb`,{
              suburbName,
              areas_id:selectedCityID,
              delivery_charges:delivery_charges,
              _token:"{{csrf_token()}}",
              _method:"post",
            }).then(({data}) => {
              if(data.status == 200){
                toastr.success('Suburb Added Successfully')
                let {suburb} = data;
                setTimeout(function(){
                    window.location = window.location.href;
                },150)
              }else {
                toastr.error(data.errMsg);
              }
            })













        }
  </script>

    {{-- // EDIT AREA MODAL --}}
  <script type="text/javascript">
  function toggleEditAreaModal($area_name,$area_id){
    document.querySelector('#modal_area_id').value = $area_id;
    document.querySelector('#modal_area_name').value = $area_name
    document.querySelector('#modal_area_name').focus()

    $('#editArea').modal('toggle');

  }
  </script>




  <script type="text/javascript">
  function toggleEditSuburbModal($suburb_id,$suburb_name,$suburb_city_list_id,delivery_charges){
    document.querySelector('#modal_suburb_id').value=$suburb_id;
    document.querySelector('#modal_suburb_name').value=$suburb_name;
    document.querySelector('#modal_suburb_delivery_charges').value = delivery_charges;
    for (var i = 0; i < document.querySelector('#modal_cities_list').options.length; i++) {
          if(parseInt(document.querySelector('#modal_cities_list').options[i].value) === parseInt($suburb_city_list_id)){
            document.querySelector('#modal_cities_list').selectedIndex = i;
          }
    }
    $('#editSuburb').modal('toggle');

  }
  </script>












@endsection
