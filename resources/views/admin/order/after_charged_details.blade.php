@extends('voyager::master')


@section('page_header')
  <h1 class="page-title">
      <i class="voyager-barbeque"></i>
      <p> {{ 'Order Details' }}</p>
  </h1>
  <span class="page-description">{{ 'Viewing Order Details' }}</span>
  <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

@endsection


@section('content')
  <style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }
    th{
      font-weight: 600 !important;
    }
    b {
      font-weight:600;
    }
  </style>

  <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}"></script>
  <div class="container">
    <div class="page-content">
      <table class="table table-responsive table-striped table-bordered">
        <thead>
          <th>Product Name</th>
          <th>Product Price</th>
          <th>Service Options</th>
          <th>Quantity</th>
          <th>Average Weight</th>
          <th>Estd. Amount</th>
          <th>Extra Quantity</th>
          <th>Extra Pay ($ NZ)</th>
            <th>Act. Amount</th>
        </thead>
        <tbody>
          <?php $i=0; $product_quantities = [];$ei=0;$totalActualAmount = 0 ?>
          @foreach ($OrderInfo as $oi)
            <tr>
              <td>{{ $oi->Product()->name }}</td>
              <td>$ {{ $oi->Product()->price }} / <b>{{ $oi->Product()->sellingUnit->name }}</b></td>
                <td>
                @if(!is_array(json_decode($oi->service_type_option)))
                    <i style="font-size:12px">{{ 'No Service Requested' }}</i>
                  @else
                    <ol>
                      @foreach (json_decode($oi->service_type_option) as  $serviceOptionItem)
                        <li>
                          {{ \App\ServiceType::where('id','=',$serviceOptionItem)->first()->name  }}</b> </sup>
                        </li>
                      @endforeach
                    </ol>
                @endif
              </td>


              <td id="table-quantity-{{$oi->id}}">
                  {{ $oi->quantity }}  <b>{{ $oi->Product()->sellingUnit->name }}</b>
              </td>

              <td>
                {!! is_null($oi->Product()->avg_weight) ? "<i>N/A</i>" : $oi->Product()->avg_weight.'<sup>Kg</sup>' !!}
              </td>

              {{-- ESTIMATED AMOUNT --}}

              <td class="product-subtotal" id="table-subtotal-{{$oi->id}}">
                {{$oi->amount}}
              </td>

              <td>
                {{ (float)$oi->extra_quantity}}
              </td>

              {{-- EXTRA AMOUNT --}}

              <td class="extra-payments-column" id="extra_payment_{{$oi->id}}">
                 {{(float)$oi->extra_payment}}
              </td>

              {{-- ACTUAL AMOUNT --}}

            <td class="actual-payments-column"  id="actual_amuount_column_{{$oi->id}}">{{ (float) $oi->amount + (float) $oi->extra_payment }}</td>
            </tr>


            {{-- Previouse Details --}}

            {{-- Original Quantity Of Product --}}
            <input id="prev-quantity-{{$oi->id}}" type="hidden" value="{{$oi->quantity}}">











            <?php $i += round(((float) $oi->Product()->price * ((float) $oi->quantity + (float) $oi->extra_quantity)),2);?>
            <?php $ei += round(((float) $oi->extra_payment ),2);?>
            <?php $totalActualAmount += (float) $oi->amount + (float) $oi->extra_payment;  ?>

          @endforeach
          <tr >
            <th colspan="7"></th>
            <td  style="text-align:left" > <b>NZ $ <span id="total-extra-price-holder"><?php echo $ei; ?></span>  </b> </td>
            <td style="text-align:left" > <b>NZ $ <span id="total-actual-price-holder"><?php echo round($totalActualAmount,2); ?></span>  </b> </td>
          </tr>

        </tbody>
      </table>
      <table class="table table-responsive table-striped table-bordered">
        <thead>
          <th style="text-align:center" >Delivery Charges</th >
          <th style="text-align:center" >Amount to be paid by client</th >
        </thead>
        <tbody>
          <tr >
            <?php
              $isDelivery = 0;
              $isDelivery = \App\Order::where('id','=',$OrderInfo->first()->order_id)->first()->isDelivery;
              $deliveryCharges = 0;
              if($isDelivery == 1){
                $deliveryCharges= (float) \App\Miscellaneous::first()->deliveryCharges;
              }
             ?>
            <td  style="text-align:center" > <b> NZ $ {{ $deliveryCharges }} </b> </td>
            <td   style="text-align:center" > <b>NZ $ <span id="amount_to_be_paid"> {{ (float)($deliveryCharges + $totalActualAmount)}}</span> </b> </td>

          </tr>
        </tbody>
      </table>

    </div>
















  </div>
@endsection
