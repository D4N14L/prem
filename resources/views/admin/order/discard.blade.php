@extends('voyager::master')
@section('page_header')
  <h1 class="page-title">
      <i class="voyager-trash"></i>
      <p> {{ 'Orders' }}</p>
  </h1>
  <h1 class="page-title pull-right">
    Total Discarded Orders: {{ $order->total() }}</p> 
  </h1>
  <span class="page-description">{{ 'Discarded Order' }}</span>
  <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@endsection


@section('content')
  <style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }

  </style>

  <style media="screen">
    a{
      text-decoration: none !important;
      outline:none !important;
    }
  </style>
  <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}"></script>
  <div class="container">
    <div class="page-content">
      <div class="row">
        <table class="table table-responsive table-bordered table-striped">
          <thead>
            <th>Cust. Name</th>
            <th>Address</th>
            <th>Landline</th>
            <th>Date</th>
            <th>Shipping</th>
            <th>Estd. Amount</th>
            <th>Extra Amount</th>
            <th>Act. Amount</th>
            <th>Act. Amount + Delivery </th>
            <th>Order Details</th>
            {{-- <th>Order Report</th> --}}
            <th>Print Report</th>
          </thead>
          <tbody>
            @foreach ($order as $o)

              <?php $i=0; ?>
                <tr>
                  <td>{{ $o->User()->name }}</td>
                  <td style="font-size:12px">{{ $o->city.' , '. $o->suburb.' , '.$o->address }} </td>
                  <td>{{ $o->User()->landline }}</td>
                  <td>
                    {{$o->order_date}}
                  </td>
                  <td >
                    {{$o->getTimeSlot()->startTime}} - {{$o->getTimeSlot()->endTime}}&nbsp;<sup style="font-weight:bold">{{$o->getTimeSlot()->shift == "morning" ? $o->getTimeSlot()->endTime == "12:00" ? "PM":"AM" : "AM" }}
                      @php $shouldDeliver = false @endphp
                    @if($o->isDelivery == 0)
                      <span > <i style="color:orange">Self</i> </span>
                    @else
                      @php $shouldDeliver = true @endphp
                      <span> <i style="color:green">Delivery</i> </span>
                    @endif
                  </td>
                  <td>
                    {{$o->total_amount}}
                  </td>
                  <td>
                    <?php $totalExtraAmount = 0;
                        foreach ($o->OrderInfo() as $orderInfo) {
                          $totalExtraAmount += $orderInfo->extra_payment;
                        }
                    ?>
                    {{ $totalExtraAmount }}
                  </td>
                  <td>
                    <?php
                    $ActualAmount = 0;
                    if($o->total_actual_amount <= 0){
                      $ActualAmount = $totalExtraAmount + $o->total_amount;
                    }else {
                      $ActualAmount = $o->total_actual_amount;
                    }
                    ?>
                    {{ $ActualAmount }}

                  </td>
                  <td>
                    {{ (float) $ActualAmount + $o->delivery_charges }}
                    <input type="hidden" id="order_amount_{{$o->id}}" value="{{ (float) $ActualAmount + $o->delivery_charges }}"/>
                  </td>
                  <td>
                    <a  class="btn btn-info"  href="{{ route('admin.order.details.charged',['id' => $o->id]) }}" > <i class="voyager-eye"></i> View </a>
                    {{-- <a role="button" href="{{ route('admin.order.mark',['id' => $o->id]) }}"  class="btn btn-success"> <i class="voyager-check"></i> Mark as Delivered  </a> --}}
                   </td>
                   {{-- <td>
                     <a target="_blank" href="{{ route('admin.order.report.generate',['id' => $o->id]) }}" class="btn btn-danger">
                       <i class="voyager-book"> </i>
                     </a>
                   </td> --}}
                   <td>  <a class="btn btn-success" target="_blank" style="color:white" href="{{route('admin.order.print.report',['id' => $o->id])}}"> <i class="voyager-receipt"></i> </a> </td>
                </tr>
            @endforeach
          </tbody>
        </table>
        <div class="pull-right">
          {!! $order->render() !!}
        </div> 
      </div>
    </div>
  </div>
  @if(session()->has('failed'))
    <script type="text/javascript">
    toastr.error('{!! session()->get('failed') !!}');
    </script>
  @endif
  @if(session()->has('success'))
    <script type="text/javascript">
    toastr.success('{!! session()->get('success') !!}');
    </script>
  @endif



@endsection
