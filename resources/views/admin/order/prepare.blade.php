<?php $admin_logo_img = Voyager::setting('site.logo');
			$admin_logo_img = '/storage/'.$admin_logo_img;
			$backgroundColor = \App\FrontendCMS::first()->backgroundColor;
?>

<!DOCTYPE html>
<html>
<head>
	<title> Order Preparation - {{ \Carbon\Carbon::now()->format('Y-m-d') }}  </title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
@php 

$admin_logo_img = Voyager::setting('site.logo');
      $extension = pathinfo($admin_logo_img,PATHINFO_EXTENSION);
      $data = file_get_contents(public_path().'/storage/'.$admin_logo_img);
      $logo = 'data:image/'.$extension.';base64,'.base64_encode($data);



@endphp
<style media="screen">
    body,html{
			font-family: 'Raleway', sans-serif !important;
      font-size:12px !important;
    }
		tr,th,td{
			font-family: 'Raleway', sans-serif !important;
            text-align:center;
		}
		.reach-us{
			display: flex;
			justify-content: space-between;;

		}
		.reach-us > p {
			font-size:12px;
		}
  </style>
</head>
<body>
    <div class="container">
        <br>
        
	<div>
        <img src="{{$logo}}" style="width:auto;height:80px;float:left" alt="" />
		<p style="float:right">
                <b>IRD <span style="font-family:Helvetica">109-047-813</span> </b>
            </p>
        </div>
        <?php $i=0; $product_quantities = [];$ei=0;$totalActualAmount = 0 ?>
        <br><br><br><br><br>
        <center>
            <h4>Order Preparation Report All Products <strong>{{\Carbon\Carbon::now()->format('Y-m-d')}}</strong></h4>
            <hr>
        </center>
        
        <div class="reach-us" >
            <p>
                <i class="fa fa-map-marker" style="color:red"></i> 	<b>{{	\App\FooterCMS::first()->address }}</b>
		</p>
		<p>
            <i class="fa fa-phone" style="color:green"></i> 		<b>{{	\App\FooterCMS::first()->phone }}</b>
		</p>
		<p>
            <i class="fa fa-fax" style="color:dark-grey"></i> 	<b>{{	\App\FooterCMS::first()->landline }}</b>
		</p>
		<p>
            <i class="fa fa-envelope" style="color:black"></i> 	<b>{{	\App\FooterCMS::first()->email }}</b>
		</p>
		<p>
            <i class="fa fa-globe" style="color:black"></i> 	<b> www.premiummeat.co.nz	 </b>			
		</p>
        
	</div>
    
    
    <hr>
    <div class="row" id="options" style="margin-bottom:10px">
            <div class="col-sm-4">
                <button class="btn btn-success" id="print" > <i class="fa fa-print"></i> Print</button>
            </div>
            <div class="col-sm-8">
                <form action="{{route('admin.order.prepare.index')}}" method="GET">
                    <div class="row">
                        <div class="col-sm-6">
                            <select name="prepartion_by" class="form-control" onchange="document.querySelector('form').submit()" >
                                
                                <option selected="{{ $prepartion_by == 'all' ? true:false }}" value="all">All</option>
                                @foreach($unique_products as $product)
                                    @if((int) $prepartion_by == (int) $product['id'])                                    
                                    <option  selected="selected" value="{{ $product['id'] }}">{{$product['name']}}</option>
                                    @else 
                                    <option  value="{{ $product['id'] }}">{{$product['name']}}</option>
                                    @endif
    
                                @endforeach
                        </select>                
                        </div>
                        <div class="col-sm-6">
                            <select name="order_date" class="form-control" onchange="document.querySelector('form').submit()" >
                    
                                <option   value="{{ "null" }}">Select By Order Date </option>                            
                                @foreach($unique_orders as $order)
                                @if($selected_order_date == $order)                                    
                                    <option  selected="selected" value="{{ $order }}">{{ $order }}</option>
                                @else 
                                    <option  value="{{ $order }}">{{  $order }}</option>
                                @endif
                                @endforeach
                        </select>                
    
                        </div>
                    </div>
                    </form>
        </div>
    </div>
  <table class="table table-responsive table-striped table-bordered">
    <thead>
      <th>Product Name</th>
      <th>Order Information</th>
    </thead>
    <tbody>
        @foreach($new_orders as $key => $new_order)
        <tr>
            <td>{{ $key }}</td>

           <td> 
            <table class="table table-striped table-bordered">
                <thead>
                    <th>Customer Name</th>
                    <th>Customer Email</th>
                    <th>Customer Landline</th>
                    <th>Order Date</th>
                    <th>Product Price</th>
                    <th>Quantity</th>
                    <th>Average Weight</th>
                    <th>Service Options</th>
                    <th>Additional Information</th>
                </thead>
                @foreach($new_order as $order)

                   <td> {{ $order['customer_name'] }}</td>
                   <td>{{ $order['customer_email'] }}</td>
                   <td>{{ $order['customer_landline'] }}</td>
                   <td>{{ $order['order_date'] }} </td>
                   <td>{{ $order['product_price']}} </td>
                   <td>{{ $order['quantity'] }} <sup> <b>{{$order['selling_unit'] }}</b> </sup></td>
                <td> <span style="font-family:Verdana">{{ $order['product_average_weight'] }}</span> <sup>Kg</sup>  </td>
                    <td> 
                        @if(json_decode($order['service_type_options'],true))
                        <ol style="padding:3px;padding-left:10px;padding-right:0">
                         @foreach(json_decode($order['service_type_options']) as $serviceOptionItem)
                            <li>{{ \App\ServiceType::where('id','=',$serviceOptionItem)->first()->name }}</li>
                         @endforeach
                        </ol>
                         @else 
                        N/a
                        @endif
                 </td>
                <td style="text-align:left"> {{ !($order['additional_information'] && trim($order['additional_information'])) ? 'N/a' : $order['additional_information'] }} </td>
            </tr>
                @endforeach            
            </table>
           </td>
     
        </tr>
        @endforeach
    </tbody>
  </table>
</div>

<script>
    document.querySelector('#print').addEventListener('click',function(){
        otions = document.querySelector('#options')
        options.style.display = 'none';
        window.print()
        options.style.display = 'block'
    })
</script>
</body>
</html>
