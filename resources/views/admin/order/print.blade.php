<?php $admin_logo_img = Voyager::setting('site.logo');
			$admin_logo_img = '/storage/'.$admin_logo_img;
			$backgroundColor = \App\FrontendCMS::first()->backgroundColor;
?>

<!DOCTYPE html>
<html>
<head>
  @php 
    $orders = \App\Order::where('id','=',$order_id)->get();
    $order = $orders->first();
@endphp
	<title> {{ ucfirst($customer->name) }}, {{$orders->pluck('created_at')->first()}}   </title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <?php $admin_logo_img = Voyager::setting('site.logo'); ?>
  <style media="screen">
    body,html{
			font-family: 'Raleway', sans-serif !important;
      font-size:12px !important;
    }
		tr,th,td{
			font-family: 'Raleway', sans-serif !important;
		}
		.reach-us{
			display: flex;
			justify-content: space-between;;

		}
		.reach-us > p {
			font-size:12px;
		}
  </style>
</head>
<body>
<div class="container">
	<br>

	<div>
		<img src="{{$logo}}" style="width:auto;height:80px;float:left" alt="" />
		<p style="float:right">
			<b>IRD <span style="font-family:Helvetica">109-047-813</span> </b>
		</p>
	</div>
	<?php $i=0; $product_quantities = [];$ei=0;$totalActualAmount = 0 ?>
	<br><br><br><br><br>
	<div class="reach-us" >
		<p>
		<i class="fa fa-map-marker" style="color:red"></i> 	<b>{{	\App\FooterCMS::first()->address }}</b>
		</p>
		<p>
		<i class="fa fa-phone" style="color:green"></i> 		<b>{{	\App\FooterCMS::first()->phone }}</b>
		</p>
		<p>
			<i class="fa fa-fax" style="color:dark-grey"></i> 	<b>{{	\App\FooterCMS::first()->landline }}</b>
		</p>
		<p>
		<i class="fa fa-envelope" style="color:black"></i> 	<b>{{	\App\FooterCMS::first()->email }}</b>
		</p>
		<p>
			<i class="fa fa-globe" style="color:black"></i> 	<b> www.premiummeat.co.nz	 </b>			
		</p>

	</div>
	<br>
  <h4>Customer Information</h4>
  <hr>
  <table class="table table-bordered table-striped">
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Landline</th>
        <th>Address</th>
        <th>Delivery / Self-Pickup</th>
      </tr>
      <tr>
        <th>{{$customer->name}}</th>
        <th>{{$customer->email}}</th>
        <th ><span style="font-family:Helvetica" >{{$customer->landline}}</span></th>
        <th style="font-weight:bold">{{ $order->city.','.$order->suburb.','.$order->address }} </th>
        <th>
          {{ $order->order_date }}
          <br>
           @if($order->isDelivery)
            <i style="color:green">Delivery</i>

          @else 
          <i style="color:orange">Self</i>
          @endif
          </center>

        </th>  
        </tr>

	</table>
  <h4>Order Information</h4>
	<hr>
  <table class="table table-responsive table-striped table-bordered">
    <tr>
      <th>Product Name</th>
      <th>Product Price</th>
      <th>Service Options</th>
      <th>Additional Info</th>
      <th>Quantity</th>
      <th>Average Weight</th>
      <th>Estd. Amount</th>
      <th>Extra Quantity</th>
      <th>Extra Pay ($ NZ)</th>
      <th>Act. Amount</th>
    </tr>
      <?php $OrderInfo = \App\OrderInfo::where('order_id','=',$order_id)->get(); ?>
      @foreach ($OrderInfo as $oi)
        <tr>
          <th>{{ $oi->Product()->name }}</th>
          <th>$ <span style="font-family:Helvetica">{{ $oi->Product()->price }}</span> / <b>{{ $oi->Product()->sellingUnit->name }}</b></th>
            <th>
            @if(!is_array(json_decode($oi->service_type_option )))
            <ul>
              <i>
                  {{ 'Not Requested' }}
              </i>
            </ul>
              @else
                <ol>
                  @foreach (json_decode($oi->service_type_option) as  $serviceOptionItem)
                    <li>
                      {{ \App\ServiceType::where('id','=',$serviceOptionItem)->first()->name  }}</b> </sup>
                    </li>
                  @endforeach
                    
                </ol>
            @endif
              
          </th>
          <th>
            <b>{!! is_null($oi->additional_information) ? '<i>N/a</i>':$oi->additional_information !!}</b>

          </th>
          <th id="table-quantity-{{$oi->id}}">
              {{ $oi->quantity }}  <b>{{ $oi->Product()->sellingUnit->name }}</b>
          </th>

          <th>
            {!! is_null($oi->Product()->avg_weight) ? "<i>N/a</i>" : '<span style="font-family:Helvetica">'.$oi->Product()->avg_weight.'</span><sup>Kg</sup>' !!}
          </th>

          {{-- ESTIMATED AMOUNT --}}

          <th class="product-subtotal" id="table-subtotal-{{$oi->id}}">
						<span style="font-family:Helvetica">
							{{$oi->amount}}
						</span>
          </th>


          <th >
              <span style="font-family:Helvetica">{{ (float)$oi->extra_quantity }}</span>
          </th>

          {{-- EXTRA AMOUNT --}}

          <th class="extra-payments-column" id="extra_payment_{{$oi->id}}">
						<span style="font-family:helvetica">{{(float)$oi->extra_payment}}</span>
          </th>

          {{-- ACTUAL AMOUNT --}}

        <th class="actual-payments-column"  id="actual_amuount_column_{{$oi->id}}"> <span style="font-family:helvetica" >{{ (float) $oi->amount + (float) $oi->extra_payment }}</span> </th>
        </tr>

        <?php $i += round(((float) $oi->Product()->price * ((float) $oi->quantity + (float) $oi->extra_quantity)),2);?>
        <?php $ei += round(((float) $oi->extra_payment ),2);?>
        <?php $totalActualAmount += (float) $oi->amount + (float) $oi->extra_payment;  ?>

      @endforeach
      <tr >
        <th colspan="8"></th>
        <td  style="text-align:left" > <b>NZ $ <span id="total-extra-price-holder"style="font-family:helvetica"><?php echo $ei; ?></span>  </b> </td>
        <td style="text-align:left" > <b>NZ $ <span id="total-actual-price-holder" style="font-family:helvetica"><?php echo round($totalActualAmount,2); ?></span>  </b> </td>
      </tr>
  </table>
  <table class="table table-responsive table-striped table-bordered">
      <tr>
          <th style="text-align:center">Service Description</th>
      </tr>
      <tr>
        <td>
          {{ (!$order->service_description )  ? 'N/a' : $order->service_description }}
        </td>
      </tr>
    </table>

  <table class="table table-responsive table-striped table-bordered">
    <tr>
      <th style="text-align:center">Service Charges</th>
      <th style="text-align:center" >Delivery Charges</th >
      <th style="text-align:center" >Total Amount Charged</th >
    </tr>
      <tr >
        <td  style="text-align:center" > <b> NZ $  <span style="font-family:helvetica">{{ $order->service_charges  }}</span>  </b> </td>
        <td  style="text-align:center" > <b> NZ $  <span style="font-family:helvetica">{{ $order->delivery_charges }}</span>  </b> </td>
        <td   style="text-align:center" > <b>NZ $ <span  style="font-family:helvetica"  id="amount_to_be_paid"> {{ (float)($order->delivery_charges + $totalActualAmount + $order->service_charges)}}</span> </b> </td>
      </tr>
    </table>

</div>
	<script type="text/javascript">
	window.print()

	</script>
</body>
</html>
