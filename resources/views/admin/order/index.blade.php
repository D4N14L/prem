@extends('voyager::master')
@section('page_header')
  <h1 class="page-title">
      <i class="voyager-bulb"></i>
      <p> {{ 'Orders' }}</p>
  </h1>

  <h1 class="page-title pull-right">
    Total New Orders: {{ $order->total() }}</p> 
  </h1>

  <span class="page-description">{{ 'New Orders' }}</span>

  <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
@endsection


@section('content')
  <style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }

  </style>

  <style media="screen">
    a{
      text-decoration: none !important;
      outline:none !important;
    }
  </style>
  <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}"></script>
  <div class="container">
    <div class="page-content">
      <div class="row" >
        <table class="table table-responsive table-bordered" style="margin-left:-3 %">
          <thead>
            <th>Cust. Name</th>
            <th>Address</th>
            <th>Landline</th>
            <th>Shipment</th>
            <th>Placed Ago</th>
            <th>Operating System</th>
            <th>Estd. Amount</th>
            <th>Extra Amount</th>
            <th>Act. Amount</th>
            <th>Service Charges</th>
            <th>Act. Amount + Delivery </th>
            <th><i class="voyager-credit-cards"></i> Click To Charge</th>
            <th>Order Details</th>
            {{-- <th>Order Report</th> --}}
            <th>Discard</th>
          </thead>
          <tbody>
            @foreach ($order as $o)

              <?php $i=0; ?>
                <tr
                @if($o->isRead == 0)
                  style="background-color:#EEE8AA;border:1px solid #EEE8AA"
                @endif

                >
                  <td>{{ $o->User()->name }}</td>
                  <td style="font-size:12px">{{ $o->city.' , '. $o->suburb.' , '.$o->address }} </td>
                  <td>{{ $o->User()->landline }}</td>
                  <td style="white-space: nowrap" >
                    {{$o->order_date}} <br>
                    @php $shouldDeliver = false @endphp
                    @if($o->isDelivery == 0)
                      <span > <i style="color:orange">Self</i> </span>
                    @else
                      @php $shouldDeliver = true @endphp
                      <span> <i style="color:green">Delivery</i> </span>
                    @endif

                  </td>
                  <td  style="white-space: nowrap">
                    {{ \Carbon\Carbon::parse($o->created_at)->format('Y-m-d H:i A') }}
                  </td>
                  <td>
                    {{ $o->operating_system }}
                  </td>
                  <td>
                    {{$o->total_amount}}
                  </td>
                  <td>
                    <?php $totalExtraAmount = 0;
                        foreach ($o->OrderInfo() as $orderInfo) {
                          $totalExtraAmount += $orderInfo->extra_payment;
                        }
                    ?>
                    {{ $totalExtraAmount }}
                  </td>
                  <td>
                    <?php
                    $ActualAmount = 0;
                    if($o->total_actual_amount <= 0){
                      $ActualAmount = $totalExtraAmount + $o->total_amount;
                    }else {
                      $ActualAmount = $o->total_actual_amount;
                    }
                    ?>
                    {{ $ActualAmount }}

                  </td>
                  <td>
                      {{ $o->service_charges }}
                    </td>

                  <td>
                    {{ (float) $ActualAmount + (float) $o->delivery_charges + $o->service_charges }}
                    <input type="hidden" id="order_amount_{{$o->id}}" value="{{ (float) $ActualAmount + $o->delivery_charges + $o->service_charges }}"/>
                  </td>
                  <td>
                    <a target="_blank" onclick="chargeCustomer(event,this)" data-order-id="{{$o->id}}"  data-customer-email="{{$o->User()->email}}" data-customer-id="{{$o->User()->stripeCustomerID}}" class="btn btn-success">Charge</a>
                  </td>
                  <td >
                    <a  class="btn btn-info btn-sm" href="{{ route('admin.order.details',['id' => $o->id]) }}" > <i class="voyager-eye"></i> View </a>
                    <a class="btn btn-success" target="_blank" style="color:white" href="{{route('admin.order.print.report',['id' => $o->id])}}"> <i class="voyager-receipt"></i> Print</a>
                    {{-- <a role="button" href="{{ route('admin.order.mark',['id' => $o->id]) }}"  class="btn btn-success"> <i class="voyager-check"></i> Mark as Delivered  </a> --}}
                  </td>
                   {{-- <td>
                     <a target="_blank" href="{{ route('admin.order.report.generate',['id' => $o->id]) }}" class="btn btn-danger">
                       <i class="voyager-book"> </i>
                      </a>
                    </td> --}}
                    <td>
                      <a onclick="grantPermission(this,event)"  href="{{route('admin.order.discard.now',['id' => $o->id])}}" class="btn btn-danger">Discard </a>
                    </td>
                </tr>
            @endforeach
          </tbody>
        </table>
        <div class="pull-right">
          {!! $order->render() !!}
        </div>
      </div>
    </div>
  </div>
  @if(session()->has('failed'))
    <script type="text/javascript">
    toastr.error('{!! session()->get('failed') !!}');
    </script>
  @endif
  @if(session()->has('success'))
    <script type="text/javascript">
    toastr.success('{!! session()->get('success') !!}');
    </script>
  @endif


<script type="text/javascript">
  function chargeCustomer(event,$el){
      event.preventDefault()
      let confirmation = confirm('This action is not revertable. Are you sure to make a charge?')
      $el.disabled = true;

      if(confirmation)
      {
        let order_id = $el.getAttribute('data-order-id');
        let customer_stripe_token = $el.getAttribute('data-customer-id').trim();
        let total_amount =  document.querySelector(`#order_amount_${order_id}`).value;
        let customer_email = $el.getAttribute('data-customer-email');
        // redirect to charge page
        if(customer_stripe_token)
        {
        let toRedirect = '{{ URL::to('/admin/orders/charge/') }}'+`/${customer_stripe_token}/${total_amount}/${order_id}/${customer_email}`
        window.location.href = toRedirect;
        }
        else 
        {
          alert('This customer doesn\'t stripe token for payment. Please contact customer')
        }
        $el.disabled = false;
        
      }else 
      {
        $el.disabled = false;
      }


  }
  function grantPermission($el,event)
  {
    event.preventDefault();
    let confirmation = confirm('This action is not revertable. Are you sure to discard this order?')
    if(confirmation)
    {
      window.location = $el.href;

    }
  }

</script>

@endsection
