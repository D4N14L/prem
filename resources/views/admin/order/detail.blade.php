<?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>

@extends('voyager::master')

@section('page_header')

  <h1 class="page-title">
      <i class="voyager-barbeque"></i>
      <p> {{  'Order Details'  }}</p>
  </h1>
  <span class="page-description">{{  'Viewing Order Details'  }}</span>
  <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
  <script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>

@endsection


@section('content')
  <style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }
    th{
      font-weight: 600 !important;
    }
    b {
      font-weight:600;
    }

  </style>

  <script type="text/javascript" src="{{  URL::to('/js/axios.js')  }}"></script>
  <div class="container">
    <div class="page-content">
    
      <div style="display:flex;justify-content:flex-start;align-items:flex-start">
          <button onclick="disableMenus(event)" style="display:none"  id="disableMenu" class="btn btn-danger">Disable</button>
          <button onclick="enableMenus(event)" id="enableMenu" class="btn btn-success">Enable</button>      
      </div>
    <a data-order-id="{{$OrderInfo->first()->order_id}}" href="{{ route('admin.order.index') }}" onclick="saveChanges(this,event)" type="button" style="background-color:{{$backgroundColor}};color:white" class="btn pull-right">Complete Order</a>

      <table class="table table-responsive table-striped table-bordered">
        <thead>
          <th>Product Name</th>
          <th>Product Price</th>
          <th>Service Options</th>
          <th>Additional Description</th>
          <th>Quantity</th>
          <th>Average Weight</th>
          <th>Estd. Amount</th>
          <th>Actual Quantity</th>
          <th>Extra Quantity</th>
          <th>Extra Pay ($ NZ)</th>
          <th>Act. Amount</th>
        </thead>
        <tbody>
          <?php $i=0; $product_quantities = [];$ei=0;$totalActualAmount = 0 ?>
          @foreach ($OrderInfo as $oi)
            <tr>

              <td>{{  $oi->Product()->name  }}</td>
              <?php $sellingUnit = (strtolower($oi->Product()->sellingUnit->name) == "piece" || strtolower($oi->Product()->sellingUnit->name) == "pieces") ? true : false;   ?>
              <td>$ {{  $oi->Product()->price  }} / <b>{{   $sellingUnit ? 'Kg' : $oi->Product()->sellingUnit->name  }}</b></td>
                <td>
                @if(!is_array(json_decode($oi->service_type_option)) )
                    <i style="font-size:12px">{{  'No Service Requested'  }}</i>
                @else
                    <ol>
                      @php $exploded = json_decode($oi->service_type_option); @endphp
                      @foreach ( $exploded as  $serviceOptionItem)
                        <li>
                          {{  \App\ServiceType::where('id','=',$serviceOptionItem)->first()->name   }}</b> </sup>
                        </li>
                      @endforeach
                    </ol>
                @endif
              </td>
              <td>
                <b>{{ is_null($oi->additional_information) ? 'N/L':$oi->additional_information }}</b>

              </td>

              <td data-ordered-quantity="{{  $oi->quantity  }}" id="table-quantity-{{ $oi->id }}">
                  {{  $oi->quantity  }}  <b>{{  $oi->Product()->sellingUnit->name  }}</b>
              </td>

              <td>
                {!! is_null($oi->Product()->avg_weight) ? "<i>N/A</i>" : $oi->Product()->avg_weight.'<sup>Kg</sup>' !!}
              </td>

              {{-- ESTIMATED AMOUNT --}}

              <td class="product-subtotal" id="table-subtotal-{{ $oi->id }}">
                {{ $oi->amount }}
              </td>

              {{-- Actual Amount --}}


              <td>
                {{-- @if(strtolower($oi->product()->sellingUnit->name) == "piece"  ||
                    strtolower($oi->product()->sellingUnit->name) == "pieces" ||
                    strtolower($oi->product()->sellingUnit->name) == "kg"     
                 ) --}}
                <input type="number" class="form-control toggler" disabled="true" oninput="extraQuantityChangeHandler(document.querySelector('#extraQuantiyMinusAvgWeight-{{$oi->id}}'))"   data-order-detail-id="{{ $oi->id }}" data-product-price="{{ $oi->product()->price }}" id="ActualQuantity-{{$oi->id}}"  min="0.0" step="0.01"  value="{{(float) $oi->actual_quantity}}">
              {{-- @else
                <input disabled type="number" class="form-control" oninput="extraQuantityChangeHandler(document.querySelector('#extraQuantiyMinusAvgWeight-{{$oi->id}}'))" data-order-detail-id="{{ $oi->id }}" data-product-price="{{ $oi->product()->price }}" id="ActualQuantity-{{$oi->id}}"  min="0.0" step="0.01"  value="{{(float) $oi->actual_quantity}}">
              @endif --}}
              </td>

              <td clas id="extraQuantiyMinusAvgWeight-{{$oi->id}}"  data-product-ordered-quantity="{{ (float) $oi->quantity }}"  data-product-sellingUnit="{{ strtolower($oi->Product()->sellingunit->name) }}" data-average-weight="{{(float) $oi->Product()->avg_weight}}" data-extra-quantity="{{(float)$oi->extra_quantity}}" data-order-detail-id="{{ $oi->id }}" data-product-price="{{ $oi->product()->price }}">
                {{abs(((float) $oi->extra_quantity))}}
              </td>
              <td class="extra-payments-column" id="extra_payment_{{ $oi->id }}">
                {{ (float)$oi->extra_payment }}
              </td>
              <td class="actual-payments-column"  id="actual_amuount_column_{{ $oi->id }}">{{  (float) $oi->amount + (float) $oi->extra_payment  }}</td>
              {{-- EXTRA AMOUNT --}}

              {{-- ACTUAL AMOUNT --}}

          </tr>


            {{-- Previouse Details --}}

            {{-- Original Quantity Of Product --}}
            <input id="prev-quantity-{{ $oi->id }}" type="hidden" value="{{ $oi->quantity }}">










            <?php $i += round(((float) $oi->Product()->price * ((float) $oi->quantity + (float) $oi->extra_quantity)),2);?>
            <?php $ei += round(((float) $oi->extra_payment ),2);?>
            <?php $totalActualAmount += (float) $oi->amount + (float) $oi->extra_payment;  ?>

          @endforeach
          <tr >
            <th colspan="8">
                <label for="Admin_Description"> <b>Service Description</b> </label>
            <textarea id="Admin_Description" rows="4"  placeholder="Add Extra Description From Admin" class="form-control">{{ \App\Order::find($OrderInfo->first()->order_id)->service_description }}</textarea>
            </th>
            <th colspan="1">
              <br><br>
                <label for="Service_Description"> <b>Service Charges</b> </label>
            <input data-order-id="{{$OrderInfo->first()->order_id}}" value="{{ \App\Order::find($OrderInfo->first()->order_id)->service_charges }}" oninput="addToActualAmount(this)" id="Service_Description" disabled="true" min="0" step="1" type="number" name="extra_charges" placeholder="0" class="form-control" />        
            </th>
            <td  style="text-align:left" > 
                <br><br>
              <b>
              NZ $ <span id="total-extra-price-holder"><?php echo $ei; ?></span>  </b> </td>
            <td style="text-align:left" ><br><br> <b>NZ $ <span id="total-actual-price-holder"><?php echo round($totalActualAmount,2); ?></span>  </b> </td>
          </tr>

        </tbody>
      </table>



      <table class="table table-responsive table-striped table-bordered">
        <thead>
            {{-- <th style="text-align:center" >Service Charges</th >
              <th style="text-align:center" >Delivery</th > --}}
          <th style="text-align:center" >Amount to be paid by client ( Actual Amount )</th >
        </thead>
        <tbody>
          <tr >
            <td   style="text-align:center" > <b>NZ $ <span id="amount_to_be_paid"> {{   $totalActualAmount  }}</span> </b> </td>
          </tr>
        </tbody>

      </table>

    </div>
















  </div>
@endsection


<script type="text/javascript">
   document.querySelector('#disableMenu').display = "none"
    
    function extraQuantityChangeHandler($el){
      if($el.value == ""){
        $el.value = 0;
      }
      let orderDetailID = $el.getAttribute('data-order-detail-id');
      let productPirce = $el.getAttribute('data-product-price')
      let ActualQuantity = document.querySelector(`#ActualQuantity-${orderDetailID}`).value;
      let ProductAverageWeight = $el.getAttribute('data-average-weight');
      let extraQuantity = $el.innerHTML.trim();



      let sellingUnitType = $el.getAttribute('data-product-sellingUnit');
      let productOrderedQuanitity = $el.getAttribute('data-product-ordered-quantity')


      let ExtraQuantityCalculated = 0;
      if(sellingUnitType == "pieces" || sellingUnitType == "piece"){
        let userOrderedQuantity = document.querySelector(`#table-quantity-${orderDetailID}`).getAttribute('data-ordered-quantity');
         ExtraQuantityCalculated = parseFloat(parseFloat(ActualQuantity).toFixed(2) - (parseFloat(userOrderedQuantity) * parseFloat(ProductAverageWeight).toFixed(2))).toFixed(2)
      }else {
        ExtraQuantityCalculated = parseFloat(parseFloat(ActualQuantity).toFixed(2) - parseFloat(productOrderedQuanitity).toFixed(2)).toFixed(2)
      }













      $el.innerHTML = ExtraQuantityCalculated
      let calculateQuantity = parseFloat(productPirce) * parseFloat(ExtraQuantityCalculated);

      let extraPaymentColumn = document.querySelector(`#extra_payment_${orderDetailID}`);
      extraPaymentColumn.innerHTML = calculateQuantity.toFixed(2);

      let estimatedAmountColumn = document.querySelector(`#table-subtotal-${orderDetailID}`).innerHTML;
      let actualAmountColumn = document.querySelector(`#actual_amuount_column_${orderDetailID}`);
      actualAmountColumn.innerHTML = "";
      actualAmountColumn.innerHTML = (parseFloat(calculateQuantity.toFixed(2)) + parseFloat(estimatedAmountColumn)).toFixed(2)


      recalculateExtraPaymentAll();
      recalculateActualAmount();
      recalculateAmountToBePaid();
      storeExtraAmount(calculateQuantity,ExtraQuantityCalculated,orderDetailID,ActualQuantity)
  }





  function storeExtraAmount(totalExtraAmount,extraQuantity,order_detail_id,actualQuantity){
    let currentOrderID = "{{ $OrderInfo->first()->order_id }}";
    axios.post('{{ URL::to('/admin/orders/details/') }}/'+currentOrderID,{
      extraQuantity,
      actualQuantity,
      order_detail_id,
      totalExtraAmount,
      currentOrderID,
      _token:"{{ csrf_token() }}",
      _method:"post"
    }).then(({data}) => {
      if(data.status == 200){
        toastr.options.timeOut = 100;
        toastr.success('Total Extra Amount Updated');
      }else {
        toastr.options.timeOut = 100;

        toastr.error('Failed to update the Extra amount ')
      }
    }).catch(err => {
      toastr.options.timeOut = 100;

      toastr.error('Failed to update the new amount ')
    });
  }

  // Extra Payment Re Calcualte

  function recalculateExtraPaymentAll(){
    let extraPaymentPriceHolder = document.querySelector('#total-extra-price-holder');
    let allExtraPaymentsColumns = document.querySelectorAll('.extra-payments-column');
    let total = 0;
    for(let i=0;i<allExtraPaymentsColumns.length;i++) {
      total +=  parseFloat(allExtraPaymentsColumns[i].innerHTML)
    }
    total = total.toFixed(2)
    extraPaymentPriceHolder.innerHTML = total;
    return total;
  }

  // Actual Amount  Re Calcualte


  function recalculateActualAmount(){
    let actualAmountPriceHolder = document.querySelector('#total-actual-price-holder');
    let allActualPaymentsColumns = document.querySelectorAll('.actual-payments-column');

    let total = 0;
    for(let i=0;i<allActualPaymentsColumns.length;i++) {
      total +=  parseFloat(allActualPaymentsColumns[i].innerHTML)
    }
    total = total.toFixed(2)
    actualAmountPriceHolder.innerHTML = total;
    return total;
  }


  function recalculateAmountToBePaid(){
    let actualTotalAmount = document.querySelector('#total-actual-price-holder').innerHTML;
    let amountToBePaid = parseFloat(actualTotalAmount).toFixed(2)
    document.querySelector('#amount_to_be_paid').innerHTML = amountToBePaid;
    amountToBeStored = parseFloat(amountToBePaid).toFixed(2);
    storeAmountToBePaid(amountToBeStored)
  }

  function storeAmountToBePaid(amount_to_be_paid){
    axios.post(`{{ URL::to('/admin/orders/actualAmount') }}`,{
      totalActualAmount:amount_to_be_paid,
      _token:"{{ csrf_token() }}",
      _method:"post",
      order_id:"{{ $OrderInfo->first()->order_id }}"
    }).then( ( { data } ) =>{
      if(data.status !== 200){
        toastr.error('Actual Amount Update Failed!');

      }
    })
  }


// CKEDITOR.replace('textarea')
function addToActualAmount($el)
{
  order_id = $el.getAttribute('data-order-id')
  let value = parseFloat($el.value)
  if (!value)
  {
    value = 0
  }
  
  axios.post(`{{ URL::to('/admin/orders/service_charges') }}`,{
      service_charges:value,
      _token:"{{ csrf_token() }}",
      _method:"post",
      order_id:order_id
    }).then(({ data }) =>{
      if(data.status !== 200){
        toastr.error('Actual Amount Update Failed!');
      }
    })
}
function saveChanges($el,$event)
{
  $event.preventDefault()
  order_id = $el.getAttribute('data-order-id')
  description = document.querySelector('#Admin_Description').value
  axios.post(`{{ URL::to('/admin/orders/admin_description') }}`,{
      description:description,
      _token:"{{ csrf_token() }}",
      _method:"post",
      order_id:order_id
    }).then(({ data }) =>{
      if(data.status !== 200){
        toastr.error('Extra Description Update Failed!');
      }
      else {
        window.location = $el.href

      }
    })
}


function enableMenus(event)
{
  event.preventDefault()
 var list =  document.getElementsByClassName('toggler')
 Array.prototype.forEach.call(list,function(el){
  el.disabled = false
})
document.querySelector('#Service_Description').disabled = false
document.querySelector('#disableMenu').style.display = "block"
document.querySelector('#enableMenu').style.display = "none"
}
function disableMenus(event)
{
  event.preventDefault()
 
 var list =  document.getElementsByClassName('toggler')
 Array.prototype.forEach.call(list,function(el){
  el.disabled = true
})
document.querySelector('#Service_Description').disabled = true
document.querySelector('#disableMenu').style.display = "none"
document.querySelector('#enableMenu').style.display = "block"

}

</script>
