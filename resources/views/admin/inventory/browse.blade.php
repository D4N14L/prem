@extends('voyager::master')


@section('page_header')
  <h1 class="page-title">
      <i class="voyager-bag"></i>
      <p> {{ 'Inventory' }}</p>
  </h1>
  <span class="page-description">{{ 'Browse Inventory' }}</span>
@endsection



@section('content')
  <style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }

  </style>

  <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}"></script>
  <div class="container">
    <div class="page-content">
      <div class="row">
        <div class="col-sm-12 col-lg-12 col-md-12">
          <div class="form-group pull-right">
            <br>

            <select class="form-control" name="cat_id" onchange="redirectToCategory(this)">
              <option value="" disabled selected>Show Product By Category</option>
              @foreach (App\ProductCategory::all() as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
              @endforeach
            </select>
          </div>

        </div>
      </div>
      <table class="table table-responsive table-bordered table-striped">
        <thead>
          <th>Name</th>
          <th>Selling Unit</th>
          <th>Price</th>
          <th>Image</th>
          <th>Category</th>
          <th>Quantity</th>
          <th>Sold</th>
          <th>Remaining Quanitity</th>
          <th>Add Quantity</th>

        </thead>
        <tbody>
          @foreach($products as $product)
            <tr>
                <td>{{ ucfirst($product->name) }}</td>
                <td>{{ $product->selling_unit_amount }} &nbsp; {{ $product->sellingUnit->name }}</td>
                <td> <b>$ &nbsp;{{ $product->price }}</b> </td>
                <td> <img src="{{ URL::to('/assets/products/'.$product->image) }}" width="80px" height="80px" style="object-fit:cover" alt=""> </td>
                <td>{{ $product->category->name }}</td>

                <td id="quantity-{{ $product->id }}">{{ $product->inventory->quantity }}</td>
                <td>{{ $product->inventory->sold }} </td>
                <td id="quanity-remaining-{{$product->id}}">{{ $product->inventory->quantity - $product->inventory->sold }}</td>
                <td>
                    <button type="button" class="btn btn-info" data-product-id="{{ $product->id }}" onclick="addQuantity(this)" > <i class="voyager-plus"></i> Add Quantity</button>
                    <button type="button" class="btn btn-danger" data-product-id="{{ $product->id }}" onclick="removeQuantity(this)"  > <i class="voyager-belt"></i> Reduce Quantity</button>
                    <hr>
                    <div class="form-group">
                      <input type="text" id="quanity-input-{{$product->id}}" class="form-control" id="quanity-input" placeholder="Enter Quantity You Want To Add Or Subtract" value="">
                    </div>
                 </td>
            </tr>


          @endforeach

        </tbody>


      </table>

      <div class="pull-right">
          {!! $products->render() !!}
      </div>
    </div>

    <script type="text/javascript">
    function redirectToCategory($el)
    {
      let element = $el.value;
      let route = `{{ URL::to('/') }}/admin/products/${element}`
      window.location = route;
    }
    function addQuantity($el)
    {
      let product_id = $el.getAttribute('data-product-id');
      let value = document.getElementById(`quanity-input-${product_id}`).value;
      if(value.length > 0 && value != "")
      {
        axios.post('{{ URL::to('admin/inventory/add/quantity') }}',{
            product_id:product_id,
            _token:"{{ csrf_token() }}",
            value:value
        }).then(response => {
          document.getElementById(`quantity-${product_id}`).innerHTML = response.data.quantity;
          document.getElementById(`quanity-input-${product_id}`).value = "";
          document.getElementById(`quanity-remaining-${product_id}`).innerHTML = response.data.remaining;

        });
      }

    }


    function removeQuantity($el)
    {
      let product_id = $el.getAttribute('data-product-id');
      let value = document.getElementById(`quanity-input-${product_id}`).value;
      if(value.length > 0 && value != "")
      {
        axios.post('{{ URL::to('admin/inventory/remove/quantity') }}',{
            product_id:product_id,
            _token:"{{ csrf_token() }}",
            value:value
        }).then(response => {
          document.getElementById(`quantity-${product_id}`).innerHTML = response.data.quantity;
          document.getElementById(`quanity-input-${product_id}`).value = "";
          document.getElementById(`quanity-remaining-${product_id}`).innerHTML = response.data.remaining;
        })
      }
    }

    </script>


  </div>
@endsection
