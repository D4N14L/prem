@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-alarm-clock"></i>
        <p> {{ 'Time Slots' }}</p>
    </h1>
    <span class="page-description">{{ 'Product Delivery Time Slot' }}</span>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.css">
    <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}">

    </script>
    @include('toaster')

@endsection

@section('content')
  <style media="screen">
    thead,th,tr,td
    {
      text-align: center;
      font-weight:400 !important;

    }
    <style media="screen">
    table{
        font-size:12px !important;
        font-weight:400 !important;

      }

    </style>
  </style>

<div class="container">

  <div class="page-content">

    <div class="row">
    <form class="" action="{{ route('timeslots.store') }}" method="post">
      {{ method_field('post') }}
      {{ csrf_field() }}
        <div class="col-sm-12 col-lg-3 col-md-3">
          <label for="times">Morning or Evening</label>
          <select id="times" class="form-control" name="type" required>
            <option value="morning" selected>Morning</option>
            <option value="evening">Evening</option>
          </select>
        </div>
        <div class="col-sm-12 col-lg-3 col-md-3">
          <div class="form-group">
            <label for="startTime">Start Time</label>
            <input data-default="00:00" required id="startTime" class="form-control" placeholder="Select Time, Click Here!" name="startTime"  />
          </div>
        </div>
        <div class="col-sm-12 col-lg-3 col-md-3">
          <div class="form-group">
            <label for="endTime">End Time</label>
            <input data-default="00:00" required id="endTime" class="form-control" placeholder="Select Time, Click Here!" name="endTime"  />
          </div>
        </div>

        <div class="col-sm-12 col-lg-3 col-md-3" style="margin-bottom:-12px">
          <div class="form-group">
            <label for="submission">Add TimeSlot</label><br>
            <button id="submission" type="submit" class="btn btn-success btn-lg"> <i class="voyager-plus"></i>Add TimeSlot </button>
            <input type="hidden" name="isEdit" id="isEdit" value="0">
            <button type="button" onclick="exitEditMode()" id="exitEditModes" style="display:none;" class="btn btn-danger pull-right"  > <i class="voyager-pirate-swords"></i> Cancel </button>
            <input type="hidden" id="editTimeSlot" name="editTimeSlot" value="">
          </div>

        </div>
    </form>
  </div>














    <div class="row">
      <div class="col-sm-12 col-md-6 col-lg-6">
          <center>
            <h4>Morning</h4>
          <table class="table table-responsive table-striped table-bordered">
            <thead>
              <th>Time</th>
            </thead>
            <tbody>
              @foreach ($morningTimeSlot as $morning )
                <tr>
                  <td>
                    {{$morning->startTime}} <sup>AM</sup> - {{ $morning->endTime }} <sup>AM</sup> &nbsp;&nbsp;&nbsp;

                    <button data-id="{{ $morning->id }}" data-startTime="{{ $morning->startTime }}" data-endTime="{{ $morning->endTime }}" data-type="morning" type="button" onclick="editMode(this)"  class="btn btn-info btn-lg">Edit</button>

                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </center>

      </div>
      <div class="col-sm-12 col-md-6 col-lg-6">
        <center>
          <h4>Evening</h4>
      <table class="table table-responsive table-striped table-bordered">
        <thead>
          <th>Time</th>
        </thead>
        <tbody>
          @foreach ($eveningTimeSlot as $evening )
            <tr>
              <td>{{$evening->startTime}} <sup>PM</sup> - {{ $evening->endTime }} <sup>PM</sup>
                &nbsp;&nbsp;&nbsp;                     <button data-id="{{ $evening->id }}" data-startTime="{{ $evening->startTime }}" data-endTime="{{ $evening->endTime }}" data-type="evening" type="button" onclick="editMode(this)"  class="btn btn-info btn-lg">Edit</button>

               </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </center>

    </div>
  </div>
  <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12">
        <h4>General</h4>
          <table class="table table-responsive table-bordered table-striped">
            <thead>
              <th>Shift</th>
              <th>Start Time</th>
              <th>End Time</th>
              <th>Remove</th>
            </thead>
            <tbody>
              @if($generalTimeslot)
                @if(count($generalTimeslot) > 0)
                  @foreach ($generalTimeslot as $general)
                      <tr>
                        <td>{{ ucfirst($general->shift) }}</td>
                        <td>{{ $general->startTime }}</td>
                        <td >{{ $general->endTime }}</td>
                        <td >
                            @if($general->isActive)
                              <button  data-id="{{ $general->id }}" data-status="disable" onclick="manageGeneral(this)" class="btn btn-danger" style="text-decoration:none"><i class="voyager-pirate-swords"></i> </button>
                            @else
                              <button  data-id="{{ $general->id }}" data-status="enable" onclick="manageGeneral(this)" class="btn btn-success" style="text-decoration:none"><i class="voyager-check"></i> </button>
                            @endif

                        </td>
                      </tr>
                  @endforeach
                @else
                  <td colspan="4" style="text-align:center">No General Timeslot Found!</td>
                @endif

              @endif
            </tbody>
          </table>
      </div>
  </div>





</div>
</div>

<script
  src="https://code.jquery.com/jquery-1.11.0.min.js"
  integrity="sha256-spTpc4lvj4dOkKjrGokIrHkJgNA0xMS98Pw9N7ir9oI="
  crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.js">

</script>

    <script type="text/javascript">
        function editCategory($el)
        {
          var category = document.getElementById($el.getAttribute('data-id'));
          var catName = category.childNodes[1].innerHTML;
          var editObj = {
            catName,
            productID:category.getAttribute('data-id')
          }
          switchToEditMode(editObj);
        }

        function switchToEditMode(editObj)
        {
          document.getElementById('name').value = editObj.catName;
          document.getElementById('add-submit-btn').innerHTML = '<i class="voyager-edit"></i> Update Record'
          document.getElementById('add-submit-btn').className = "btn btn-info"
          document.getElementById('checker').value = 1
          document.getElementById('edit-close-btn').style.display = "block"
          document.getElementById('edit-close-btn').style.marginRight = "5px"
          document.getElementById('productID').value = editObj.productID
        }
        function switchToAddMode($el)
        {
           $el.style.display = "none"
           document.getElementById('add-submit-btn').className = "btn btn-success"
           document.getElementById('add-submit-btn').innerHTML = '<i class="voyager-plus"></i> Add Record'
           document.getElementById('checker').value = 0

        }

    </script>
    <script type="text/javascript">
      // ERROR
        function redirectAndWarn($el)
        {
          let url = `{{URL::to('admin/sellingUnits/delete/')}}/${$el.getAttribute('data-id')}`;

              if(window.confirm('Caution! This will delete all the related products'))
              {
                window.location.href =  url
              }

        }
    </script>
    <script type="text/javascript">
      (function(){
          $('#startTime').clockpicker({
              autoclose: true
           });
           $('#endTime').clockpicker({
               autoclose: true
            });
      }())
    </script>
    <script type="text/javascript">
        function manageGeneral($el)
        {
             let general_id = $el.getAttribute('data-id');
             let status = $el.getAttribute('data-status');
             if(status === 'disable')
             {
                let confirmation = confirm('Are you sure want to disable general timslot item ?')
                  if(confirmation)
                  {
                    postGeneral(general_id,status,$el);
                  }
             }
             else
             {
               postGeneral(general_id,status,$el);

             }

        }
        @if($generalTimeslot)
          @if(count($generalTimeslot) > 0)
          function postGeneral(general_id,status,$el)
          {
            axios.post('{{URL::to('/admin/manageGeneralState')}}',{
                general_id,
                _token:"{{ csrf_token() }}",
                status
            }).then(response => {
                    if(response.data.message === "200")
                    {
                       let statusHolder = $el.parentNode;
                      if(status === "disable")
                      {
                          statusHolder.innerHTML = `      <button  data-id="{{ $general->id }}" data-status="enable" onclick="manageGeneral(this)" class="btn btn-success" style="text-decoration:none"><i class="voyager-check"></i> </button>
                          `
                      }
                      else
                      {
                          statusHolder.innerHTML = `    <button  data-id="{{ $general->id }}" data-status="disable" onclick="manageGeneral(this)" class="btn btn-danger" style="text-decoration:none"><i class="voyager-pirate-swords"></i> </button>
                          `
                      }
                    }
             })
          }

          @endif
        @endif
    </script>
    <script type="text/javascript">
      function editMode($el)
      {
        var type = $el.getAttribute('data-type');
        var startTime = $el.getAttribute('data-startTime');
        var endTime = $el.getAttribute('data-endTime');
        var timeslot_id = $el.getAttribute('data-id');
        document.getElementById('startTime').value = startTime;
        document.getElementById('endTime').value = endTime;
        document.getElementById('isEdit').value = 1;

        document.getElementById('startTime').style.border = "1px solid red";
        document.getElementById('startTime').style.backgroundColor = 'red';
        document.getElementById('startTime').style.color = "white";
        document.getElementById('startTime').style.fontWeight = "bold";


        document.getElementById('endTime').style.border = "1px solid red";
        document.getElementById('endTime').style.backgroundColor = 'red';
        document.getElementById('endTime').style.color = "white";
        document.getElementById('endTime').style.fontWeight = "bold";

        var optHandler = '';
        if(type == 'morning')
        {
          optHandler = `<option value="morning" selected>Morning</option>
            <option value="evening">Evening</option>`;
        }
        else
        {
          optHandler = `<option value="morning">Morning</option>
            <option value="evening" selected>Evening</option>`;

        }

          document.getElementById('times').innerHTML = optHandler;

        document.getElementById('submission').className = "btn btn-info"
        document.getElementById('submission').innerHTML = `<i class="voyager-edit"></i> Update TimeSlot`;
        document.getElementById('exitEditModes').style.display = "block";
        document.getElementById('editTimeSlot').value = timeslot_id;
      }

      function exitEditMode()
      {
        document.getElementById('startTime').value = '';
        document.getElementById('endTime').value = '';
        document.getElementById('isEdit').value = 0;

        document.getElementById('startTime').style.border = "1px solid #e4eaec";
        document.getElementById('startTime').style.backgroundColor = 'white';
        document.getElementById('startTime').style.color = "black";
        document.getElementById('startTime').style.fontWeight = "100";



        document.getElementById('endTime').style.border = "1px solid #e4eaec";
        document.getElementById('endTime').style.backgroundColor = 'white';
        document.getElementById('endTime').style.color = "black";
        document.getElementById('endTime').style.fontWeight = "100";

        document.getElementById('submission').className = "btn btn-success"
        document.getElementById('exitEditModes').style.display = "none";
        document.getElementById('submission').innerHTML = `<i class="voyager-plus"></i>  Add TimeSlot`;
      }

    </script>

@endsection
