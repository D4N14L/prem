@extends('voyager::master')


@section('page_header')
  <h1 class="page-title">
      <i class="voyager-campfire"></i>
      <p> {{ 'Manage' }}</p>
  </h1>
  <span class="page-description">{{ 'Manage Your Slides' }}</span>
  @include('toaster')

@endsection


@section('content')
  <div class="container">
    <div class="row">
      <form id="form-slide" action="{{ route('cms.manage.post.slides') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="slides" id="slides-array"/>
      </form>

      <div class="container">
        <div class="col-sm-6 col-lg-11 col-md-11">
          <div id="selected-images" class="row">

          </div>
          <hr>
          <div class="" style="display:flex;justify-content:space-between">
            <h4>Select Images To Delete</h4>
            <button type="button" style="display:none;" onclick="submission()" id="deleteBTN" class="btn btn-danger pull-right"> <i class="voyager-trash"></i> Delete </button>
          </div>
          <hr>
        </div>

      </div>
        <div class="row">

          @foreach ($slides as $slide)
            <div class="col-sm-12 col-lg-3 col-md-3">
              <img id="slide-{{$slide->id}}" onclick="hightLight(this)" data-slide-id="{{ $slide->id }}" data-toggle="0" src="{{ URL::to('/sliders') }}/{{ $slide->image }}" style="object-fit:cover" width="150" height="150" alt="">
            </div>
          @endforeach
        </div>

      </div>

  </div>
  <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12">
        <center>
          {!! $slides->render() !!}
        </center>
      </div>
  </div>




  <script type="text/javascript">
      var images = [];
      var selectedElement = []
      function hightLight($el)
      {
        var status = $el.getAttribute('data-toggle');
        if(status == "0")
        {
          $el.style.border = "3px solid #19B5FE";
          images.push($el.getAttribute('data-slide-id'))
          selectedElement.push($el);
          $el.setAttribute('data-toggle','1')
        }
        else
        {
          $el.style.border = "";
          images = images.filter(function(x){
                return x != $el.getAttribute('data-slide-id')
          });
          selectedElement = selectedElement.filter(function(x1){
              return x1.getAttribute('data-slide-id') != $el.getAttribute('data-slide-id')
          });

          $el.setAttribute('data-toggle','0')
        }
        display(selectedElement);
        if(images.length == 0)
        {
          document.getElementById('deleteBTN').style.display = "none"
        }
        else
        {
          document.getElementById('deleteBTN').style.display = "block"
        }
      }

      function submission()
      {
          var form = document.getElementById('form-slide')
          var slideArray = document.getElementById('slides-array');
          slideArray.value = images
          form.submit();
      }
      function display(selectedElement)
      {
          var selectedImages = document.getElementById('selected-images');
          selectedImages.innerHTML = ""
          selectedElement.forEach(function(x){
            selectedImages.innerHTML += `<div class='col-sm-4 col-lg-1 col-md-1'><img  data-slide-id="${x.getAttribute('data-slide-id')}" onclick="cancelThis(this)" src="${x.src}" style="object-fit:cover;border-radius:100%" width="50" height="50" /></div>`
          });
      }
      function cancelThis($el)
      {
        images = images.filter(function(x){
              return x != $el.getAttribute('data-slide-id')
        });
        selectedElement = selectedElement.filter(function(x1){
            return x1.getAttribute('data-slide-id') != $el.getAttribute('data-slide-id')
        });

        document.getElementById(`slide-${$el.getAttribute('data-slide-id')}`).style.border = "";

        display(selectedElement);
      }

  </script>







@endsection
