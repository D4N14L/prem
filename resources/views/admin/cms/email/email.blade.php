@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-mail"></i>
        <p> {{ 'Email Manager' }}</p>
    </h1>
    <span class="page-description">{{ 'Send Mail to customers of your choice' }}</span>
@endsection

@section('content')
<script src="{{ URL::to('/') }}/js/ckeditor/ckeditor.js"></script>

<div class="container" style="background-color:white;box-shadow:1px 1px 0.2px 0.4px #eee;padding:24px">
    <div class="page-content">
        
    <form action="{{ route('mail-list-post') }}" method="POST" >
        @csrf
        <div class="row">

                <div class="col-sm-12 col-md-8 col-lg-8">
                    <p>
                        <b>
                            To (Recipient)
                        </b>
                    </p>
                    <select id="mail-list" required name="to[]"multiple class="form-control">
                        @foreach($emails as $email)
                            <option value="{{ $email->email }}"> {{ $email->email }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-4" style="padding:28px">
                   <span style="font-size:18px;font-weight:bold">All</span> &nbsp; <input  name="is_all" type="checkbox" onchange="toggleMailList(this)" />
                </div>

                <div class="col-sm-12 col-md-12 col-lg-12">
                    @if ($errors->has('subject'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{  $errors->first('subject')  }}</strong>
                        </span>
                    @endif
                    <br />
                    <label for="subject"> <b>Subject</b>  </label>
                    <input id="subject" type="text" class="form-control {{  $errors->has('subject') ? ' is-invalid' : ''  }}" name="subject" value="{{  old('subject')  }}"  autofocus>
                </div>

                
                <div class="col-sm-12 col-md-12 col-lg-12">
                        @if ($errors->has('body'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{  $errors->first('body')  }}</strong>
                            </span>
                        @endif
                        <p> <b>Body</b> </p>
                        <textarea required id="body" type="text" rows="10" class="form-control {{  $errors->has('subject') ? ' is-invalid' : ''  }}" name="body"  autofocus>{{  old('body')  }}</textarea>
                </div>
    



            </div>
            <button class="pull-right btn btn-success" type="submit"> <i class="voyager-mail"></i>&nbsp;Send</button>            

            <br />
                
        </form>
    </div>
</div>  

<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script>
$(document).ready(function() { 
 $("#mail-list").select2({
    allowClear: true,
    tags: true,
	tokenSeparators: ['/',',',';'," "]
 });
 CKEDITOR.replace('body')
});
function toggleMailList($el)
{
    if($el.checked)
    {
        $('#mail-list').prop('disabled', true);
        $('#mail-list').empty()
        $('#mail-list').disable()
        
    }
    else 
    {
        $('select').prop('disabled', false);
    }
}

</script>
@endsection