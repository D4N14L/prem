<?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>
  <?php $admin_logo_img = Voyager::setting('site.logo'); ?>
  @extends('layouts.app2')


  @section('content')

   @include('app-badges')









    {{-- <link rel="stylesheet" href="{{ URL::to('/css/selecty.css') }}"> --}}
    <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.js"></script>
    {{-- <script type="text/javascript" src="{{ URL::to('/js/selecty.js') }}"></script> --}}
    @include('styles')

    <style media="screen">

    /* .dropdown-content>li>span>label::before {
      border-right:2px solid {{$backgroundColor}} !important;
      border-bottom:2px solid {{$backgroundColor}} !important;

    } */

    /* large screens handler for estimated price */
    [type="checkbox"]:checked+label::before{
      border-right:2px solid {{$backgroundColor}} !important;
      border-bottom:2px solid {{$backgroundColor}} !important;
    }
.dropdown-content li>span>label {
  top:-10px !important;
}
    #historian{
      position: fixed;
      left:5%;
      z-index:1000;
      top:34%;
    }


    .additional_add_to_cart_information_flex-box{
      display:flex;justify-content:space-between;
    }
    .additionalDescriptionClass{
      width:300px;
      height: 150px;
    }
    #product-unit {
      color:{{$backgroundColor}};
      font-weight:bold;
      border:1px solid #ccc;
      height:66.5%;
      margin-bottom:-5px;
      line-height: 38px;
      font-size:14px ;
      padding:2px 5px;
    }
    .product-description {
      margin-top:-80px !important;
      margin-left:50px !important;
    }



      /* card header grid */

      .card-top-grid{
        display: grid;
        grid-template-areas: "logo quantity btn-modal"
      }
      .card-top-grid .logo {
        grid-area: logo;
      }
      .card-top-grid .add-quantity-title{
        grid-area: quantity
      }
      .card-top-grid .closed-btn-modal{
        grid-area: btn-modal
      }



      /* counter grid */

      .counter-grid{
        display: grid;
        grid-template-areas: "decrementer quantity-display product-unit incrementer";
        align-items:center;
        justify-content: center;
      }
      .counter-grid .decrementer{
        grid-area: decrementer;
        margin-right:15px;
        margin-top:-25px;
      }
      .counter-grid .quantity-display{
        grid-area:quantity-display;
        margin-left:10px;
      }
      .counter-grid #product-unit{
        grid-area:product-unit;
        margin-top:-25px;
        margin-left:0px;
        border-left:0px;
      }
      .counter-grid .incrementer{
        grid-area:incrementer;
        margin-left:15px;
        margin-top:-25px;
      }


      /* inner  grid container */

      .innerCardGrid{
        display: grid;
        grid-template-areas: "counter-grid calculator serviceType"
      }
      .innerCardGrid .counter-grid{
        grid-area: counter-grid;
      }
      .innerCardGrid .calculator{
        grid-area: calculator;
        margin-top:-15px;
        align-self: center !important ;
        justify-content: flex-end !important;
      }
      .innerCardGrid .serviceType{
        grid-area: serviceType;
        margin-top:-15px;
      }
      .materialize-textarea
      {
          padding:12px 24px !important;
          margin-left:20px !important;
          color:{{$backgroundColor}} !important;
      }
      .materialize-textarea::placeholder{
        color:{{$backgroundColor}} !important;
      }
      .materialize-textarea:focus {
        border:1px solid {{$backgroundColor}} !important;
        border-bottom-color: none !important;
        outline: none !important;
      }
      .add-to-btn-spacer
      {
        display: block;
      }

      .marginer{
        display: none
      }

      @media screen and (max-width:768px){
        #product-unit {
          color:{{$backgroundColor}};
          font-weight:bold;
          border:1px solid #ccc;
          height:68.5%;
          margin-bottom:-5px;
          line-height: 38px;
          font-size:14px ;
          padding:2px 5px;
        }
        .product-description {
          margin-top:30px !important;
          margin-left:0px !important;
          margin-bottom: 20px !important;
        }

          .card-top-grid{
            grid-template-areas: ". quantity btn-modal";
          }
          .add-quantity-title{
            margin-right:-30px;
          }
          .innerCardGrid{
              grid-template-areas:
              "counter-grid"
              "calculator"
              "serviceType";
          }
          #historian{
            left:0%;
          }
          .innerCardGrid .counter-grid{
            align-self: center;
            width:100%;
          }
          .innerCardGrid .calculator{
            width:100%;
          }
          .innerCardGrid .serviceType{
            align-self: center;
            text-align:center;
            align-items: center;
            margin-top:0px;
            width:100%;
          }
          .card-top-grid .logo {
            /* width: 85px !important;
            height: auto !important; */
            display: none;
          }
          .decrementer{
            width: 80%;
            margin-right: 25px;
          }
          .counter-grid .incrementer{
            width: 110%;
            margin-left:9.5px;
          }
          .notify{
            word-wrap: break-word !important;

            width:100% !important;
            display: block !important;
          }
          .marginer{
            display: block;
          }
          .additional_add_to_cart_information_flex-box
          {
            display:block;
          }
          .additionalDescriptionClass{
            width:250% !important;
          }
          .add-to-btn-spacer{
            display: none;
          }
          .dynamic-btn-on-select{
            background-color:#A6152C;z-index:100000000 !important;
          }
      }
      .dropdown-content{
        max-height:1000px !important;
      }
      .dropdown-content{
        top:90% !important;
      }
    </style>

















  <div class="container">

    <div class="row">

      <center>
        <div class="">
          <br><br><br><br>
          <p class="flow-text pink-text" style="color:{{$backgroundColor}} !important">
            <strong style="font-weight:800">{{ ucfirst($cat->name) }}</strong> Products
            <div class="divider pink" style="background-color:{{$backgroundColor}} !important;width:15%;height:1px;border-radius:15px"></div>
          </p>
          {{-- <img src="{{URL::to('/')}}/assets/halal-label.jpg" style="width:50px;height:50px;border-radius:100%" alt="" /> --}}
        </div>
      </center>
    </div>
    <a id="historian" style="background-color:{{$backgroundColor}} !important" onclick="history.back()" class="btn btn-floating pink left"> <i class="fa fa-arrow-circle-left white-text"></i> </a>
    <div class="marginer" style="margin:50px;">

    </div>
    <div class="row">

      @if( $products->count() > 0 )
        {{-- <input type="hidden" id="main-cat-name" name="" value="{{ strtolower($products->first()->category->name) }}"> --}}
        @foreach ( $products as $product )


            {{--
              * Detecting sellingUnit of currently iterating product
              *
              *
            --}}


            <input type="hidden" id="sellingUnit_of_product-{{$product->id}}" value="{{strtolower($product->sellingUnit->name)}}">



            {{--
              * Average of currently iterating product
              * having sellingunit type of piece
              *
            --}}

            <input type="hidden" id="avg_weight_of_product-{{$product->id}}" value="{{(float)$product->avg_weight}}">






          <?php static $incrementerMaring = 1; ?>

            <div class="col s12 m4 l4"  >
              <div class="card" style="width:auto;background:transparent;box-shadow:none;" >
                <b style="font-size:12px;position:absolute;top:10px;left:15px;z-index: 1000;background-color: white;color:{{$backgroundColor}} !important;width:auto;border-radius:25px;padding:12px;text-align:left;z-index:2">NZ $ {{ $product->price }} / {{ strtolower($product->sellingUnit->name) == "piece" ? "Kg": $product->sellingUnit->name }}</b>

                @if($incrementerMaring <= 3)
                  <div class="card-image top3 waves-effect waves-block waves-light" style="height:300px">
                @else
                  <div class="card-image waves-effect waves-block waves-light" style="margin-top:-50px !important;height:300px">
                @endif
                <?php $incrementerMaring++; ?>
                          <img class="modal-trigger" data-target="modal-{{$product->id}}"  style="object-fit:cover;margin:0px;max-height:85%;min-width:100%;height:85%"  src="{{ URL::to('/assets/products/'.$product->image) }}" alt="">
                        @if($product->is_available == 0)
                          @php $out_of_stock = '<br /><span style="font-size:12px;font-weight:bold;margin-top:-5px;display:block">( Out of Stock )</span>'; @endphp
                          @php $name = $product->name.' '.$out_of_stock @endphp
                        @else 
                          @php $name = $product->name @endphp
                        @endif
        
                          <span class="card-title modal-trigger" data-target="modal-{{$product->id}}" style="text-align:left;margin-left:-2px;display:block;float:left;background-color:rgba(0,0,0,0.4);height:78px;position:relative;bottom:26%;width:100%;left:0.5%;margin-right:-20px!;font-size:18px;">{!! $name !!} <br>&nbsp;</span>

                            </div>
                      <div class="card-content" style="padding-bottom:0px;padding-top:0px">
                        <button  class="btn z-depth-3 btn-floating halfway-fab waves-effect pink waves-light  modal-trigger" data-target="modal-{{$product->id}}"  style="line-height:42px;right:-2%;top:64%;right:5%;background-color:white !important"> <i style="font-size:16px;color:{{$backgroundColor}}" class=" fa fa-shopping-cart"></i> </button>
                          <div class="row">

                          </div>
                      </div>



                  </div>

                  <div id="modal-{{$product->id}}" style="padding:auto 400px" data-product-id="{{$product->id}}"  class="modal bottom-sheet">
                    <div class="modal-content" style="margin-bottom:10px" >
                      <div class="card-top-grid" >



                          {{-- card logo --}}

                        <img class="logo" src="{{ Voyager::image($admin_logo_img) }}"  style="align-self:center;height:45px;width:auto;" alt="">

                        {{-- card quantity title --}}


                          <center class="add-quantity-title" style="margin-top:-10px;align-self:baseline">
                            <p >
                            <span style="text-align: center;font-size:18px;font-weight:800;display:block;">Add Quantity</span>
                                <div class="divider " style="background-color: {{$backgroundColor}};width:20%;height:2px;border-radius:15px"></div>
                            </p>
                          </center>

                          {{-- card modal close btn --}}

                          <span class="card-title grey-text text-darken-4" ><i style="color:{{$backgroundColor}} !important" class="fa fa-times-circle right modal-close closed-btn-modal"></i></span>



                      </div>

                        <div style="margin-top:10px;" >
                          <div class="innerCardGrid" >
                            <div class="counter-grid">
                              {{-- DECREMENTER --}}






                              <button style="background-color:{{ $backgroundColor }} !important;" type="button"  onmousedown="decrement(this)" data-product-price="{{ $product->price }}"  data-product-id="{{ $product->id }}"  class="btn btn-success btn-flat decrementer white-text btn-small" name="button">
                                <i class="fa fa-minus"></i>
                              </button>






                              {{-- DECREMENTER END --}}







                              {{-- quantity-display --}}

                              <?php $initialValue = 1; ?>

                              @if(ucfirst($cat->name) == 'Whole Bird' || ucfirst($cat->name) == 'Free Range Chicken Whole Bird')
                                <?php $initialValue = 1 ?>
                                  @if(strtolower($product->sellingUnit->name) == "kg")

                                    <input class="quantity-display" oninput="assignQuantity(this)" disabled id="assignQuantity-{{$product->id}}"   data-product-price="{{ $product->price }}" data-product-id="{{ $product->id }}"  type="number" step="0.5" min="0.5" value="1"  style="width:50px;border:1px solid #ccc;border-right:0;text-align:center"    /> <span id="product-unit"> {{ $product->sellingUnit->name }} </span> &nbsp;&nbsp;
                                  @else
                                    <input class="quantity-display" oninput="assignQuantity(this)" disabled id="assignQuantity-{{$product->id}}"   data-product-price="{{ $product->price }}" data-product-id="{{ $product->id }}"  type="number" step="1" min="1" value="1"  style="width:50px;border:1px solid #ccc;border-right:0;text-align:center"  /> <span id="product-unit"> {{ucfirst("piece(s)") }} </span> &nbsp;&nbsp;

                                  @endif


                              @else
                                <?php $initialValue = (float)1; ?>
                                @if(strtolower($product->sellingUnit->name) == "kg")
                                  <input class="quantity-display" oninput="assignQuantity(this)"   disabled id="assignQuantity-{{$product->id}}" data-product-price="{{ $product->price }}" data-product-id="{{ $product->id }}"  type="number" step="0.5" min="0.5" value="1"  style="width:50px;border:1px solid #ccc;border-right:0;text-align:center"   /> <span id="product-unit">{{ ucfirst($product->sellingUnit->name) }}</span> &nbsp;&nbsp;
                                @else
                                  <?php $initialValue = 1; ?>
                                  <input class="quantity-display" oninput="assignQuantity(this)"   disabled id="assignQuantity-{{$product->id}}" data-product-price="{{ $product->price }}" data-product-id="{{ $product->id }}"  type="number" step="1" min="1" value="1"  style="width:50px;border:1px solid #ccc;border-right:0;text-align:center"     /> <span id="product-unit">{{ ucfirst("piece(s)") }}</span> &nbsp;&nbsp;
                                @endif
                              @endif



                              {{-- quantity-display End --}}




                              {{-- INCREMENTER --}}

                              <button style="background-color:{{ $backgroundColor }} !important;" onmousedown="increment(this)"  type="button" data-product-price="{{ $product->price }}"  data-product-id="{{ $product->id }}" class="btn btn-success btn-flat incrementer  white-text btn-small" name="button">
                                <i class="fa fa-plus"></i>
                              </button>

                              {{-- INCREMENTER END --}}

                            </div>

                            <input type="hidden" id="quantity-{{ $product->id }}" name="" value="{{$initialValue}}"  min="{{$initialValue}}"  />
                            @if(strtolower($product->sellingUnit->name) == "pieces" || strtolower($product->sellingUnit->name) == "piece")
                              <input type="hidden" id="priceMade-{{$product->id}}" value="{{ (float) ($product->price * $initialValue) * (float) $product->avg_weight }}">
                            @else
                              <input type="hidden" id="priceMade-{{$product->id}}" value="{{ (float) $product->price * $initialValue }}">
                            @endif
                            <div class="calculator" id="for-large-screens">
                              <center>

                                <p  style="color:{{ $backgroundColor }} !important;font-weight:1000;padding:0px;margin:0 auto ">
                                  <div>
                                    <label  style="color:{{$backgroundColor}};font-family:'Raleway';font-size:14px;font-weight:600" >Estimated Price</label> <br><br>

                                    @if(strtolower($product->sellingUnit->name) == "pieces" || strtolower($product->sellingUnit->name) == "piece")
                                      <span style="color:{{ $backgroundColor }} " id="count-total-{{$product->id}}"> {{ number_format((float) $product->price,2,'.','') }} X  {{$initialValue}} X {{$product->avg_weight}} Kg = $ {{ (float) ((float) number_format((float) $product->price,2,'.','') * (float) $initialValue) * (float) $product->avg_weight }}</span>

                                    @else
                                      <span style="color:{{ $backgroundColor }} " id="count-total-{{$product->id}}"> {{ number_format((float) $product->price,2,'.','') }} X  {{$initialValue}} = $ {{ (float) number_format((float) $product->price,2,'.','') * (float) $initialValue }}</span>

                                    @endif


                                   </div>
                                </p>
                              </center>

                            </div>

                            <div class="input-field serviceType" id="service-type-{{ $product->id }}">
                            <label id="service-label-{{$product->id}}"  style="color:{{$backgroundColor}};font-size:14px;font-weight:500" >Choose Service Type</label> <br><br>
                            @php $exploded = explode(",",$product->service_groups) @endphp
                            @if(count($exploded) > 0 && $exploded[0] != '0')
                            <select  style="border:2px solid rgba(233, 30, 99,0.7);color:white;height:1000px !important" id="service-type-sub-{{$product->id}}" multiple   data-product-id="{{$product->id}}" onchange="handleOkButton(this)"  class="service_type" name="service_type">
                            <option value="0" style="display:none" selected disabled> <span>Service Type</span> </option>
                            @foreach (\App\ServiceGroup::whereIn('id',$exploded)->orderBy('name','asc')->get() as $group)
                              <optgroup label="<b>{{ $group->name}}</b>" style="background-color:{{$backgroundColor}}">                                
                                @foreach(\App\ServiceGroupOptions::where('group_id',$group->id)->get() as $service_group_option)
                                  @php $sgo = \App\ServiceType::find($service_group_option->service_id); @endphp    
                                <option value="{{ $sgo->id }}">{{ $sgo->name }}</option>
                                @endforeach
                              </optgroup>     
                              @endforeach

                            </select>
                            @else 
                          <p style="color:{{$backgroundColor}}"><b>No service option available for <sup>*</sup>{{ $product->name }}</b></p>
                            @endif
                            </div>
                          </div>
                          <div >
                          </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="margin-top:-40px" >
                      <br>
                      <div class="additional_add_to_cart_information_flex-box">

                        <div class="input-field" style="width:30%;">
                          <textarea placeholder="Additional Information About Your Order (Optional)" style="border:1px solid {{$backgroundColor}};height:120px" name="additionalDescription" class="materialize-textarea additionalDescriptionClass"   id="additionalDescription-{{$product->id}}"></textarea>
                        </div>
                        <div class="">
                          <span class="add-to-btn-spacer">
                            <br><br>
                          </span>
                            <center>
                              <button  onclick="addToCart(this)"   data-price="{{ $product->price }}" data-product-id="{{ $product->id }}" class="btn add-to-card-btn btn-flat waves-effect waves-light white-text modal-close" style="background-color:{{$backgroundColor}} !important">Add To Cart <i class="white-text fa fa-shopping-cart"></i> </button>
                            </center>
                        </div>
                        <div class="">

                        </div>
                        <div class="">

                        </div>
                          <div></div>
                      </div>

                      <div style="text-align:center;color:{{$backgroundColor}};" class="product-description">
                        <h6 style="color:#000;font-weight:600">Product Description</h6>
                        <center>
                          <div class="divider " style="background-color: {{$backgroundColor}};width:5%;height:2px;border-radius:15px"></div>
                        </center>
  <br>
                          {{$product->description}}
                      </div>

                      {{-- <br>
                      <div>
                        <b style="color:{{$backgroundColor}}">Product Description</b>
                          <div style="color:{{$backgroundColor}} !important;float:left;margin-left:25px">
                            <p style="float:left;margin-left:25px">
                            </p>
                          </div>

                      </div> --}}
                    </div>

                  </div>
            </div>

        @endforeach


      @else


        <div class="col s12 m12 l12 card">
          <div class="card-content">
            <p class="pink-text flow-text" style="color:{{$backgroundColor}} !important">
              <i> <i  class="fa fa-warning red-text" style="font-size:32px;color:red"></i> &nbsp;&nbsp;&nbsp;<span style="font-size:18px">No, Products Found For <b>{{ $cat->name }}</b> <sup>Category</sup> </span> </i>
            </p>
          </div>
        </div>


      @endif
    </div>











  </div>
<style media="screen">
  .select-dropdown li.disabled, .select-dropdown li.disabled>span, .select-dropdown li.optgroup{
    color:white !important;
  }
</style>
  <script>

  function handleOkButton($el){

    var product_id = $el.getAttribute('data-product-id')
    document.querySelector(`#service-type-${product_id} > div > ul > li > span`).innerText = `Ok`
    document.querySelector(`#service-type-${product_id} > div > ul > li`).style.backgroundColor = `{{$backgroundColor}}`
    document.querySelector(`#service-type-${product_id} > div > ul > li > span`).style.color = "{{$backgroundColor}}"
    document.querySelector(`#service-type-${product_id} > div > ul > li > span`).addEventListener('click',function(){
    document.querySelector(`#modal-${product_id}`).click()
    document.querySelector(`#service-type-${product_id} > div > ul > li > span`).innerText = `Service Type`
});
}
  function closeOkBtn($el,product_id)
  {
      $(`service-label-${product_id}`).innerHTML = "Choose Service Option";
  }

  $(document).ready(function(){



//     $('li[id^="select-options"]').on('touchend', function (e) {
//    e.stopPropagation();
// });
    // $('select').prop('selectedIndex',0)
    $('input,textarea').focus(function(){
       $(this).data('placeholder',$(this).attr('placeholder'))
              .attr('placeholder','');
    }).blur(function(){
       $(this).attr('placeholder',$(this).data('placeholder'));
    });

    $('.modal').modal({
      dismissible: true,
      ready:function(currentModal,trigger){
      let modal = currentModal[0]
      let productID = modal.getAttribute('data-product-id');
      document.getElementById('quantity-'+productID).value = "";
      document.getElementById('priceMade-'+productID).value = "";
      document.getElementById('additionalDescription-'+productID).value = ""
      document.getElementById('assignQuantity-'+productID).value = document.getElementById('assignQuantity-'+productID).getAttribute('value')
      calculate(document.getElementById('assignQuantity-'+productID))
        let select = $('select');
          select.prop('selectedIndex', 0); //Sets the first option as selected
          $('select').material_select()
          $(".dropdown-content>li>span").css("color", "{{$backgroundColor}}");
          $('.optgroup > span').css({'background-color':'{{$backgroundColor}} !important'})
          $('.select-dropdown').on('close', function(event) {
            event.target.parentNode.parentNode.getElementsByTagName('label')[0].innerHTML = "Choose Service Option";
          });
      }
    });
    // $('.dropdown-trigger').dropdown({ alignment:'top' })
});
</script>
  <script type="text/javascript">

  function calculate($el) {
    var productID = $el.getAttribute('data-product-id');
    var productPrice = $el.getAttribute('data-product-price');
    counterPrice(productPrice, productID, $el.value);
  }

  function getServiceOptions($el) {
    



    var serviceType = $el.options[$el.selectedIndex].value;
    var pID = $el.getAttribute('data-product-id');
    var serviceOptionsContainer = document.getElementById("service-type-options-" + pID);
    serviceOptionsContainer.style.display = "block";
    var serviceOptions = document.getElementById("service-type-sub-" + pID);

    while (serviceOptions.firstChild) {
      serviceOptions.removeChild(serviceOptions.firstChild);
    }

    var noneSelection = document.createElement('option');
    noneSelection.value = '0';
    noneSelection.text = "Service Option";
    noneSelection.selected = true;
    serviceOptions.appendChild(noneSelection);
    $('select').trigger('contentChanged');
    var newOption = "";
  }

  function increment($el) {
    var productID = $el.getAttribute('data-product-id');
    var price = parseFloat($el.getAttribute('data-product-price')).toFixed(2);
    var quantityCalculator = document.getElementById("assignQuantity-" + productID);
    quantityCalculator.stepUp(1);
    counterPrice(price, productID, quantityCalculator.value);
  }

  function counterPrice(price, product_id, quantity) {
    var counter = document.getElementById("count-total-" + product_id);
    var priceMade = document.getElementById("priceMade-" + product_id);
    var SellingUnitName = document.querySelector("#sellingUnit_of_product-" + product_id).value.toLowerCase();

    if (SellingUnitName === "pieces" || SellingUnitName === "piece") {
      var sellingUnitAvgWeight = document.querySelector("#avg_weight_of_product-" + product_id).value;
      var calc = parseFloat(parseFloat(price).toFixed(2) * parseFloat(quantity).toFixed(2) * parseFloat(sellingUnitAvgWeight).toFixed(2)).toFixed(2);
      var calculatedPirce = "$ " + price + " X " + quantity + " X " + sellingUnitAvgWeight + " Kg = $ " + calc;
      priceMade.value = calc;
      counter.innerHTML = calculatedPirce;
      document.getElementById("quantity-" + product_id).value = quantity;
    } else {
      var _calc = parseFloat(parseFloat(price).toFixed(2) * parseFloat(quantity).toFixed(2)).toFixed(2);

      var _calculatedPirce = '$ ' + price + ' X ' + quantity + ' = $ ' + _calc;

      priceMade.value = _calc;
      counter.innerHTML = _calculatedPirce;
      document.getElementById(`quantity-${product_id}`).value = quantity;
    }

  }

  function decrement($el) {
    let productID = $el.getAttribute('data-product-id');
    let quantityCalculator = document.getElementById(`assignQuantity-${productID}`);
    let price = parseFloat($el.getAttribute('data-product-price')).toFixed(2);
    quantityCalculator.stepDown(1);
    counterPrice(price, productID, quantityCalculator.value);
  }

  function assignQuantity($el) {// calculate($el)
  }

  function addToCart($el) {
    let product_id = $el.getAttribute('data-product-id');
    let quantity = document.getElementById(`quantity-${product_id}`).value;
    let serviceOption = document.getElementById(`service-type-sub-${product_id}`);

    let serviceOptionsSelection = '0';

    if(serviceOption)
    {

      if (serviceOption.options.length > 0) {
        serviceOptionsSelection = $(`#service-type-sub-${product_id}`).val();
      } else {
        serviceOptionsSelection = '0';
      }

    }

    let priceMade = document.getElementById(`priceMade-${product_id}`).value;
    let additionalDescription = document.querySelector(`#additionalDescription-${product_id}`).value;








    axios.post(`{{URL::to('/')}}/addToCart`, {
      product_id,
      quantity,
      priceMade,
      serviceOptionsSelection,
      additionalDescription,
      _token: "{{ csrf_token() }}"
    }).then(response => {
      if (response.data.status == "200") {
        $('.cart-counter').text(response.data.cartSize);
        $('.cart-counter').addClass('animated heartBeat').one('animationend webkitAnimationEnd oAnimationEnd', function () {
          $(this).removeClass('animated heartBeat');
        });
        toastr.success('Item added to cart','Success');
      }  
      else 
      {
        toastr.error(response.data.message,'Try Again').css("width","500px")
      }
    });
  }

  function moveToCard($el) {
    let p_id = $el.getAttribute('data-product-id');
    let elementSelect = $(`#activator-${p_id}`);
    let offset = elementSelect.offset();
    $('html').scrollTop(offset.top);
    elementSelect.trigger('click');
  }




  </script>

  @endsection
