<?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>

@extends('layouts.app')

@section('content')
<style media="screen">
label{
  color:{{$backgroundColor}} !important;
}
.invalid-feedback{
  color:{{$backgroundColor}} !important;
}
</style>
<div class="row">
    <div class="col s12 m10 l8 offset-m1 offset-l2" >
    <br><br><br><br><br>

    <div class="card row" style="padding:24px 12px 24px 12px">
         <div class="card-content">
         <div class="flow-text" style=>Reset Password</div>
            <br>
        <form class="form-horizontal" role="form" method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">
            <div class="input-field col s12 {{ $errors->has('email') ? ' has-error' : '' }}">
                <input type="email" name="email" id="email" value="{{ $email or old('email') }}" class="validate">
                <label for="email">E-Mail Address</label>
                @if ($errors->has('email'))
                    <span class="red-text">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="input-field col s12 {{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" class="validate" name="password">
                <label for="password">Password</label>
                @if ($errors->has('password'))
                    <span class="red-text">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="input-field col s12 {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input id="password-confirm" type="password" class="validate" name="password_confirmation">
                <label for="password-confirm">Confirm Password</label>
                @if ($errors->has('password_confirmation'))
                    <span class="red-text">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                @endif
            </div>
            <div class="input-field col s12">
                <button style="background-color:{{$backgroundColor}};color:white" type="submit" class="btn waves-effect waves-light">Reset Password</button>
            </div>
        </form>

            </div>
        </div>
    </div>
</div>
@endsection
