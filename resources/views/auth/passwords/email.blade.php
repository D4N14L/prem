<?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>
@extends('layouts.app')

@section('content')
  <style media="screen">
    label{
      color:{{$backgroundColor}} !important;
    }
    .invalid-feedback{
      color:{{$backgroundColor}} !important;
    }
  </style>

<div class="container">
    <div class="row justify-content-center" style="padding-top:15%">
        <div class="col-md-8"><br>
            <div class="card">

                <div class="card-content">
                  <div class="card-title">{{ __('Reset Password') }}</div>

                    @if (session('status'))
                        <div class="alert alert-success" style="color:{{$backgroundColor}}" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="row">

                            <div class="col m12 l12 s12 input-field">
                              <label for="email" >{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback red-text" role="alert">
                                        <strong style="color:{{$backgroundColor}}">{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" style="background-color:{{$backgroundColor}}" class="btn white-text left" >
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
