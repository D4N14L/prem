    @extends('layouts.app2')

  @section('content')
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
      {!! Toastr::render() !!}
      @if($errors->count() > 0)
        <script>toastr.error('{{ $errors->first() }}')</script>
      @endif
    <?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>
  <style media="screen">
    body{
      overflow-y: scroll;;
    }
    a:hover
    {
      cursor: pointer;
    }
    label
    {
      color:{{ $backgroundColor }} !important;

    }
    @if(url()->current() == URL::to('/').'/register')
    #logo-main
    {
      display: none;
    }
    @endif
    .invalid-feedback{
      color:red;
    }
    .invalid-feedback::before{
      content:'*';
    }
  </style>
  <style>
  .select-dropdown{
    color:{{ $backgroundColor }} !important;
  }
  </style>

  <div class="container">
    <script type="text/javascript" src="{{  URL::to('/js/axios.js')  }}"></script>
    <div id="eula" class="modal">
        <div class="modal-content"  style="word-wrap:break-word;text-align:justify !important;;">
          <p >
             {!! \App\Portions::where('portion_1','=',5)->first()->body !!}

          </p>
        </div>
        <div class="modal-footer" >
          <a style="position:absolute;top:0%;right:0%" href="#!" class="modal-close waves-effect  pink btn-floating"> <i class="fa fa-times"></i> </a>
        </div>
      </div>

      <div class="row justify-content-center" style="padding-top:10%">

          <div class="col-md-8">
              <div class="card">
                  <div class="card-content">
                    <a style="background-color:{{ $backgroundColor }} !important" onclick="history.back()"  class="btn btn-floating pink"> <i class="fa fa-arrow-circle-left white-text"></i> </a>
                    <center>
                      <div class="navbar-logo  z-depth-2 hide-on-med-and-down">
                      </div>
                    </center><br>

                      <center>
                        <br>
                        <div class="card-title pink-text"> <h5 style="font-size:14px;color:{{ $backgroundColor }}">{{  __('Join the best meat seller')  }} <br>                      </h5>
                          <div class="divider pink"  style="width:10%;background-color:{{ $backgroundColor }} !important">

                          </div>
                       </div>
                     </center>


                      <form autocomplete="off" onsubmit="checkForEULA(this,event)" method="POST" action="{{  route('register')  }}">
                          @csrf

                          <div class=" row">

                              <div class="input-field col s12 m4 l4">
                                <label for="name">Full Name</label>
                                  <input autocomplete="off"  id="name" type="text" class="{{  $errors->has('name') ? ' is-invalid' : ''  }}" name="name" value="{{  old('name')  }}"  autofocus>

                                  @if ($errors->has('name'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{  $errors->first('name')  }}</strong>
                                      </span>
                                  @endif
                              </div>

                              <div class="col m4 l4 s12 input-field">
                                <label for="email">{{  __('E-Mail Address')  }}</label>
                                  <input autocomplete="off"  id="email" type="email" class="{{  $errors->has('email') ? ' is-invalid' : ''  }}" name="email" value="{{  old('email')  }}" >

                                  @if ($errors->has('email'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{  $errors->first('email')  }}</strong>
                                      </span>
                                  @endif
                              </div>
                              <div class="col l4 m4 s12 input-field">
                                <label for="email">Contact Number</label>
                                  <input autocomplete="off"  id="landline" type="text" style="font-family:Verdana" class="{{  $errors->has('landline') ? ' is-invalid' : ''  }}" name="landline" value="{{  old('landline')  }}" />

                                  @if ($errors->has('landline'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{  $errors->first('landline')  }}</strong>
                                      </span>
                                  @endif
                              </div>

                          </div>

                          <div class=" row">

                            <div class="input-field col s12 m4 l4">
                                <select id="cityList" onchange="updateSuburbList(this)" >
                                  <option value="0" style="color:{{ $backgroundColor }}" disabled  selected>Choose City</option>
                                  @foreach (\App\Area::orderBy('cityName','asc')->get() as $city)
                                    <option value="{{ $city->id }}">{{ $city->cityName }}</option>
                                  @endforeach
                                </select>


                                @if ($errors->has('suburb_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Choose Your City</strong>
                                    </span>
                                @endif
                            </div>




                            <div class="input-field col s12 m4 l4">

                                <select id="suburbList" name="suburb_id" >
                                    <option value="0" selected disabled >Choose Suburbs</option>
                                </select>
                                @if ($errors->has('suburb_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{  $errors->first('suburb_id')  }}</strong>
                                    </span>
                                @endif
                            </div>

                              <div class="input-field col s12 m4 l4">
                                <label for="name">{{  __('Street Address')  }}</label>
                                  <input autocomplete="off"  style="font-family:Verdana" id="address" type="text" class="{{  $errors->has('address') ? ' is-invalid' : ''  }}" name="address" value="{{  old('address')  }}" >

                                  @if ($errors->has('address'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{  $errors->first('address')  }}</strong>
                                      </span>
                                  @endif
                              </div>


                              <div class="col l4 m4 s12 input-field" style="display:none">
                                    <select name="ethnic"  >
                                      <option value="null">Select Ethnic</option>
                                      <option value="NZ European">NZ European</option>
                                      <option value="Chinese">Chinese</option>
                                      <option value="Arabs ">Arabs</option>
                                      <option value="Pakistanies ">Pakistanies</option>
                                      <option value="Indians ">Indians</option>
                                      <option value="Fiji Indians ">Fiji Indians</option>
                                      <option value="Afghanies ">Afghanies</option>
                                      <option value="Bangalies ">Bangalies</option>
                                      <option value="Pacific Islands">Pacific Islands</option>
                                    </select>
                                    <label>{{  __('Ethnic')  }}</label>

                                  @if ($errors->has('ethnic'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{  $errors->first('ethnic')  }}</strong>
                                      </span>
                                  @endif
                              </div>


                          </div>

                          <div class=" row">

                              <div class="col l6 m6 s12 input-field">
                                <label for="password">{{  __('Password')  }}</label>
                                  <input autocomplete="off"  id="password" type="password" class="{{  $errors->has('password') ? ' is-invalid' : ''  }}" name="password" >

                                  @if ($errors->has('password'))
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{  $errors->first('password')  }}</strong>
                                      </span>
                                  @endif
                              </div>

                              <div class="col m6 l6 s12 input-field">
                                <label for="password-confirm" >{{  __('Confirm Password')  }}</label>
                                  <input autocomplete="off"  id="password-confirm" type="password" class="" name="password_confirmation" >
                              </div>
                          </div>
                          <div>
                            <input autocomplete="off" type="checkbox" name="isAgreementSigned" id="check">
                            <label for="check" >Click here to indicate that you have read and agreed to the terms of the &nbsp;  </label><a   data-target="eula"class="modal-trigger" >End User License Agreement ( EULA )</a>
                          </div>
                          <br>
                          <div class=" row ">
                              <div class="col-md-6 offset-md-4">
                                  <button style="margin-bottom:12px;width:210.64px;background-color:{{ $backgroundColor }} !important" type="submit" class="btn pink white-text left" >
                                      {{  __('Register')  }}
                                  </button>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>


  <style media="screen">

    ul.dropdown-content.select-dropdown li span {
        color: #e91e63; /* no need for !important :) */
    }
  </style>


  <script type="text/javascript">

    function checkForEULA($el,event)
    {
      event.preventDefault()
      if(!(document.getElementById('check').checked))
      {
        alert('You Have To Agree To Our Terms And License');
      }
      else
      {
        console.log('submitting')
        $el.submit();
      }

    }

  </script>
  <script type="text/javascript">
    function updateSuburbList($el){
      let area_id = $el.options[$el.selectedIndex].value;
      let suburbList =   document.querySelector('#suburbList');
          suburbList.innerHTML = "<option disabled selected>Loading Suburbs...</option>";
          $('select').material_select();
            axios.get(`{{ route('get.suburb.by.area') }}`,{
              params:{
                area_id
              }
            }).then( ({data}) => {
                suburbList.innerHTML = "";
                data.forEach( suburb => {
                    let option = document.createElement('option');
                    option.value = suburb.id;
                    option.innerHTML = suburb.suburbName;
                    document.querySelector('#suburbList').appendChild(option);
                });
                $('select').material_select();
            });
    }
  </script>









  @endsection
