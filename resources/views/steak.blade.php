<?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>

<?php
$steak = \App\Portions::where('id','=',2)->first()
?>

<div class="section" style="background-image:url('{{URL::to("/extra/")}}/{{$steak->image}}');
background-size:cover;
background-repeat: no-repeat;
height: 50%;
/* background-attachment: fixed; */
background-position: center right right ;
width: 100%;" data-anchor="steak"  style="margin-bottom:25px" >

        <div  class="row">
            <div  ></div>        <br>
            <div class="col s12 m6 l6" style="background-color:{{$backgroundColor}};opacity:0.8;height:100%;width:750px;padding:42px">
                <div>
                        <h5 style="color:white;font-weight:100;font-size:42px;font-family:Great Vibes">{{ $steak->title }}</h5>
                        <hr style="background-color:white;height:1px;border:none;width:9%;float:left">
                    </div>

                <br>
                <p id="steak-defination" style="text-align: justify;color:white;font-size:18px;font-weight:50;font-family:Raleway">{{ $steak->body }}</p>
            </div>
            <br>
        </div>
        {{-- <center>
          <a href="{{URL::to('/#footer')}}">
            <div  class="scroll-down" style="position: absolute;left:45%;top:73%;z-index:100;color:#fff;background-color:rgb(194, 30, 86,0.35);width:120px;height:120px;border-radius:100%;">
                            <svg width="40px" style="margin:0 auto" height="100%"  viewBox="0 0 247 390" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:1.5;">
                                    <path id="wheel" d="M123.359,79.775l0,72.843" style="fill:none;stroke:#fff;stroke-width:20px;"/>
                                    <path id="mouse" d="M236.717,123.359c0,-62.565 -50.794,-113.359 -113.358,-113.359c-62.565,0 -113.359,50.794 -113.359,113.359l0,143.237c0,62.565 50.794,113.359 113.359,113.359c62.564,0 113.358,-50.794 113.358,-113.359l0,-143.237Z" style="fill:none;stroke:#fff;stroke-width:20px;"/>
                            </svg><br>
                            <span style="color:rgb(194, 30, 86);font-size:18px;color:white;text-shadow:0px 0px 1px white" class="white-text">Scroll down</span>

            </div>
          </a>
        </center> --}}

    </div>
