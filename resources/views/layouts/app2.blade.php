<?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>
<?php $admin_logo_img = Voyager::setting('site.logo'); ?>

  <!doctype html>
  <html lang="{{ app()->getLocale() }}">
    <head>



            <noscript><meta http-equiv="refresh"content="0; url=/enable-javascript"></noscript>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
	       <meta name="google-site-verification" content="L1-CIHVtDLBsreI9YiimEGBwAVWziuQ5MY5bBzC1TLM" />
        <title>Premium Meat</title>
        <meta name="csrf-token" content="{{ csrf_token() }}"/>

        <link rel="shortcut icon" href="{{URL::to("/extra/logo.png")}}" />
        <!-- Fonts -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        {{-- <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"> --}}
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

        <script>

                $(document).keydown(function(e){
                    if(e.which === 123){
                       return false;
                    }
                });
                $(document).bind("contextmenu",function(e) {
                      e.preventDefault();
                });
                
                
                document.onkeydown = function(e) {
                if(event.keyCode == 123) {
                return false;
                }
                if(e.ctrlKey && e.keyCode == 'E'.charCodeAt(0)){
                return false;
                }
                if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
                return false;
                }
                if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
                return false;
                }
                if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
                return false;
                }
                if(e.ctrlKey && e.keyCode == 'S'.charCodeAt(0)){
                return false;
                }
                if(e.ctrlKey && e.keyCode == 'H'.charCodeAt(0)){
                return false;
                }
                if(e.ctrlKey && e.keyCode == 'A'.charCodeAt(0)){
                return false;
                }
                if(e.ctrlKey && e.keyCode == 'F'.charCodeAt(0)){
                return false;
                }
                if(e.ctrlKey && e.keyCode == 'E'.charCodeAt(0)){
                return false;
                }
                }
                </script>
                
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.green.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.7/jquery.fullpage.css">
        <link href="https://fonts.googleapis.com/css?family=Great+Vibes|Raleway|Lobster" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://yarnpkg.com/en/package/normalize.css">

        <!-- Styles -->
        <style>
            .no_transition {
    transition: none !important;
}
            .product-listings
            {
                height: 205px;
                width: 100%;
                padding:0% 10% 0% 10%;
                display:flex;
                flex-flow:column;
                flex-direction: column;
            }
            .product-listings > *
            {
                flex:1 1 80px;
            }
            .title-category
            {
                font-size:18px;
                font-weight:550;
            }
            .svg-icon
            {
                width:80px;height:80px;object-fit:cover;
            }
            .svg-icon:hover
            {
                cursor: pointer;
            }
            html, body
            {
                height: 100%;
                margin:0;
                font-family: 'Raleway';
                min-height:100%;
                background: #f7f7f7 url('https://futurenature.net/img/noisy-texture-80x80-o3-d18-c-f7f7f7-t1.png');
            }
            .footer-black{
                 background-color:#dc3545;
            }
            .premium-logo
            {
                background-image:url('{{Voyager::image($admin_logo_img)}}');
                width:250px;
                height: 250px;
                background-size:contain;
                background-repeat: no-repeat;
                background-position: center center;
                box-shadow: 1px 1px 2px 2px rgba(0,0,0,.35);
                border-radius: 100%;
            }
            .posi
            {
              top:0px !important;
            }
            .navbar-logo
            {
                background-image:url('{{Voyager::image($admin_logo_img)}}');
                width:100px;
                line-height:70px;
                background-color:white;
                height: 100px;
                border-radius: 100%;
                border:15px solid white;
                background-position: center center;
                background-repeat: no-repeat;
                background-size: contain;

            }
            nav{
                background-color: transparent !important;
                box-shadow: none;
            }

            nav ul li a:hover {
                background-color: transparent !important;
            }
            nav ul li a {
                color:{{$backgroundColor}} !important;

            }
            .nav-wrapper{
                padding:0px 24px 0px 24px;
            }
            ul li
            {
                list-style-type: none;
            }
            .sausage-back
            {
                background-image:url('{{URL::to("/extra/sausage.jpg")}}');
                background-size:cover;
                background-repeat: no-repeat;
                height: 80%;
                /* background-attachment: fixed; */
                background-position: center right right right ;
                width: auto;
            }
            .steak-back
            {
                background-image:url('{{URL::to("/extra/steak.jpg")}}');
                background-size:cover;
                background-repeat: no-repeat;
                height: 80%;
                /* background-attachment: fixed; */
                background-position: center right right ;
                width: auto;
            }

            .login:hover
            {
              color:{{$backgroundColor}} !important;
            }
            #fp-nav ul li a span
            {
                background-color:{{$backgroundColor}} !important;
            }
            #slide1
            {
             background-image:url('{{URL::to("/sliders/slide1.jpg")}}')   ;
             background-size:contain;
             /* width:100%; */
             background-repeat:no-repeat;

            }
            #responsive-logo
            {
                background-image:url('{{Voyager::image($admin_logo_img)}}');
                width:100px;
                line-height:70px;
                background-color:white;
                height: 100px;
                border-radius: 100%;
                background-position: center center;
                background-repeat: no-repeat;
                background-size: contain;
            }
            #slide2
            {
             background-image:url('{{URL::to("/sliders/slide2.jpg")}}')   ;
             background-size:contain;
             background-repeat:no-repeat;

            }
            #slide3
            {
             background-image:url('{{URL::to("/sliders/slide3.jpg")}}')   ;
             background-size:contain;
             background-repeat:no-repeat;

            }
            @keyframes scroll {
            0% {
                transform: translateY(0);
            }
            30% {
                transform: translateY(60px);
            }
        }

            .owl-item:hover
            {
                cursor: grab;
            }

            .card-image  img
            {
                height:300px !important;
            }
            svg #wheel {
                animation: scroll ease 2s infinite;
            }

            // Default stuff.
            *,
            *::before,
            *::after {
                box-sizing: border-box;
                -webkit-backface-visibility: hidden;
                -webkit-transform-style: preserve-3d;
            }

            #slide4
            {
             background-image:url('{{URL::to("/sliders/slide4.jpg")}}')   ;
             background-size:contain;
             background-repeat:no-repeat;

            }
            /* .overlay
            {
             width:100%;
             height:20px;
             top:0px;
             background-color: rgba(0,0,100,0.2);
             position: relative;;
            } */
            #sausage-defination
            {
                color:white;font-size:18px;font-weight:50;font-family:Raleway;word-wrap:break-word;
            }
            #fullpage
            {
                margin-top:-2%;
            }
            .footer-last
            {
                background-color:darkslategray;
            }
            .card-margin
            {
                margin:5px;
            }
            .fp-show-active
            {
                top:80% !important;

            }


            @media only screen and (max-width: 768px)
            {
                #fp-nav ul li a span, .fp-slidesNav ul li a span
                #fp-nav ul li a span
                {
                    background-color:{{$backgroundColor}} !important;
                    border:2px solid yellow;

                    box-shadow: 0 5px 5px 0 rgba(0,0,0,0.14), 0 9px 12px 0 rgba(0,0,0,0.12), 0 6px 4px -6px rgba(0,0,0,0.2);
               }

                #meat-board
                {
                    display: none;
                }
                .footer-black
                {
                    height: 80%;
                }
                #cat-display
                {
                    margin-left:-50px;
                }
                #cat-display > div
                {
                    margin:0px 10px 0px 10px;

                }
                #cat-display > div > img
                {
                    width: 50px;
                    height: 50px;
                }
                #cat-display > div > p
                {
                    font-size:12px;
                }

                .overlay
                {
                    display: none;
                }
                #product-intro{
                    margin-bottom: -100px;
                }
                #sausage-defination
                {
                    width: 300px !important;
                    text-align: justify;
                }
                #steak-defination
                {
                    width: 300px !important;
                    text-align: justify;
                }
                .navbar-logo
                {
                    width: 120px;
                    height: 120px;
                }
                .opx
                {
                    padding:0px;
                    margin-top:-100px;
                }
                #navbar-subscription-logo
                {
                    width:50px;
                    height:50px;
                }
                #premium-plan-title
                {
                    font-size:14px;
                }
                #meat-board-text-font
                {
                    font-size:16px;
                }

            }
            #meat-board-text-font
                {
                    font-size:16px;
                }

            .product-listing-gallery
            {
                height:180px;
                width:100%;
                margin-top:60px;
            }
        </style>
    </head>
    <body id="site">
            <noscript><meta http-equiv="refresh"content="0; url=/enable-javascript"></noscript>
        @include('navbar')
    
        @yield('content')


















        <script src="{{URL::to('/js/materialize.js')}}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.7/jquery.fullpage.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
        @include('script')
        <script type="text/javascript">
        $('textarea').trigger('autoresize');
        $('.collapsible').collapsible();
        $('select').material_select({ position:'top' })
        $('.modal').modal();
        </script>
        {{-- <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script> --}}
        @include('toaster')


        <script>
                toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }

        </script>

        @if(session()->has('failed'))
          <script type="text/javascript">
          toastr.error('{!! session()->get('failed') !!}');
          </script>
        @endif
        @if(session()->has('success'))
          <script type="text/javascript">
          toastr.sucess('{!! session()->get('success') !!}');
          </script>
        @endif
          <script>
          $(document).bind("contextmenu",function(e) {
                    e.preventDefault();
                });


          </script>
    </body>
    </html>
