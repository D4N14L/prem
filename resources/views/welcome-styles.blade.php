<style>
/* body,html
{
  margin:0px !important;
  padding:0px !important;
} */
.section{
  height: 90vh;
}
#show-more:hover{
  cursor: pointer;
}

.suburb-grid {
  display:grid;
  grid-template-columns:repeat(6,1fr);
}
#dadas:hover
{
  background-color: #c21e56 !important;
}
#dadas2:hover
{
  background-color: #c21e56 !important;
}

nav ul li:hover {
background-color: white !important;
}
   .no_transition
   {
       transition: none !important;
   }
    #login:hover
    {
      color:#c21e56 !important;
    }
    .hover-custom
    {
      font-weight: 600;
    }
      .hover-custom:hover
      {
        text-shadow:0px 0px 10px #c21e56;
      }
    .product-listings
    {
        height: 205px;
        width: 100%;
        padding:0% 10% 0% 10%;
        display:flex;
        flex-flow:column;
        flex-direction: column;
    }
    .product-listings > *
    {
        flex:1 1 80px;
    }
    .title-category
    {
        font-size:18px;
        font-weight:550;
    }
    .svg-icon
    {
        width:80px;height:80px;object-fit:cover;
    }
    .svg-icon:hover
    {
        cursor: pointer;
    }
    html, body
    {
        height: 100%;
        margin:0;
        font-family: 'Raleway';
        min-height:100%;
        background: #f7f7f7 url('https://futurenature.net/img/noisy-texture-80x80-o3-d18-c-f7f7f7-t1.png');
    }
    .footer-black{
         background-color:#dc3545;
    }
    .premium-logo
    {
        background-image:url('{{Voyager::image($admin_logo_img)}}');
        width:250px;
        height: 250px;
        background-size:contain;
        background-repeat: no-repeat;
        background-position: center center;
        box-shadow: 1px 1px 2px 2px rgba(0,0,0,.35);
        border-radius: 100%;
    }
    .navbar-logo
    {
        background-image:url('{{Voyager::image($admin_logo_img)}}');
        width:100px;
        line-height:70px;
        background-color:white;
        height: 100px;
        border-radius: 100%;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: contain;

    }
    nav{
        background-color: transparent !important;
        box-shadow: none;
    }

    nav ul li a:hover {
        background-color: transparent !important;
    }
    nav ul li a {
        color:#c21e56 !important;

    }
    .nav-wrapper{
        padding:0px 24px 42px 24px !important;
        background-color: white !important;
    }
    ul li
    {
        list-style-type: none;
    }
    .sausage-back
    {
        background-image:url('{{URL::to("/extra/sausage.jpg")}}');
        background-size:contain;
        background-repeat: no-repeat;
        height: auto;
        width: 100%;
        /* background-attachment: fixed; */
        background-position: center right right right ;
    }
    .steak-back
    {
        background-image:url('{{URL::to("/extra/steak.jpg")}}');
        background-size:cover;
        background-repeat: no-repeat;
        width: 100%;
        height: auto;
        /* background-attachment: fixed; */
        background-position: center right right ;
    }


    #fp-nav ul li a span
    {
        background-color:#c21e56 !important;
    }
    #slide1
    {
     background-image:url('{{URL::to("/sliders/slide1.jpg")}}')   ;
     background-size:contain;
     /* width:100%; */
     background-repeat:no-repeat;

    }
    /* #
    {
        background-image:url('{{Voyager::image($admin_logo_img)}}');
        width:100px;
        line-height:70px;
        background-color:white;
        height: 100px;
        border-radius: 100%;
        background-position: center center;
        background-repeat: no-repeat;
        background-size: contain;
    } */
    #slide2
    {
     background-image:url('{{URL::to("/sliders/slide2.jpg")}}')   ;
     background-size:contain;
     background-repeat:no-repeat;

    }
    #slide3
    {
     background-image:url('{{URL::to("/sliders/slide3.jpg")}}')   ;
     background-size:contain;
     background-repeat:no-repeat;

    }
    @keyframes scroll {
    0% {
        transform: translateY(0);
    }
    30% {
        transform: translateY(60px);
    }
}

    .owl-item:hover
    {
        cursor: grab;
    }

    .card-image  img
    {
        height:300px !important;
    }
    svg #wheel {
        animation: scroll ease 2s infinite;
    }

    // Default stuff.
    *,
    *::before,
    *::after {
        box-sizing: border-box;
        -webkit-backface-visibility: hidden;
        -webkit-transform-style: preserve-3d;
    }

    #slide4
    {
     background-image:url('{{URL::to("/sliders/slide4.jpg")}}')   ;
     background-size:contain;
     background-repeat:no-repeat;

    }
    #sliders
    {
        width:100%;

    }
    /* .overlay
    {
     width:100%;
     height:20px;
     top:0px;
     background-color: rgba(0,0,100,0.2);
     position: relative;;
    } */
    #sausage-defination
    {
        color:white;font-size:18px;font-weight:50;font-family:Raleway;word-wrap:break-word;
    }
    #fullpage
    {
        margin-top:-2%;
    }
    .footer-last-grid {
      width:100%;padding:0px;display:grid;grid-template-columns:repeat(3,1fr);justify-content:center;align-items:center;
    }
    .footer-last
    {
        background: {{\App\FrontendCMS::get()->first()->backgroundColor}};
        /* fallback for old browsers */
        /* background: -webkit-linear-gradient(to right, #c31432, #240b36);  /* Chrome 10-25, Safari 5.1-6 */
        /* background: linear-gradient(to right, #c31432, #240b36); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */ */ */

    }
    .card-margin
    {
        margin:5px;
    }
    .fp-show-active
    {
        top:80% !important;

    }
    .scroll-down
    {
      display: block;
    }

    #fp-nav ul li a span, .fp-slidesNav ul li a span
    #fp-nav ul li a span
    {
        background-color:#c21e56 !important;
        border:2px solid yellow;
        box-shadow: 0 5px 5px 0 rgba(0,0,0,0.14), 0 9px 12px 0 rgba(0,0,0,0.12), 0 6px 4px -6px rgba(0,0,0,0.2);
    }
                    #product-intro
                    {
                      margin-top: 0px;
                    }
    @media only screen and (max-width: 768px)
    {
        .suburb-grid {
          display:grid;grid-template-columns:repeat(3,1fr);
        }
        .suburb-grid p {
          font-size:12px !important;

        }
        #fp-nav ul li a span, .fp-slidesNav ul li a span
        #fp-nav ul li a span
        {
            background-color:#c21e56 !important;
            border:2px solid yellow;

            box-shadow: 0 5px 5px 0 rgba(0,0,0,0.14), 0 9px 12px 0 rgba(0,0,0,0.12), 0 6px 4px -6px rgba(0,0,0,0.2);
       }
       .footer-last-grid {
         grid-template-columns:repeat(2,1fr);
         grid-column-gap: 30px;
         margin-top:-150px;
       }
       #service-area-footer {
          width: 100%;
          font-size:16px;
          font-weight: bold;
          border-right:0.2px solid #ddd;
          margin-left:15%;
       }
        .scroll-down
        {
          display: none;
        }
        #meat-board
        {
            display: none;
        }
        .footer-black
        {
            height: 80%;
        }
        .cat-display
        {
            margin-left:-60px;
        }
        .cat-display > div
        {
            margin:0px 10px 0px 10px;

        }
        .cat-display > div > img
        {
            width: 50px;
            height: 50px;
        }
        #cat-display > div > p
        {
            font-size:12px;
        }

        .overlay
        {
            display: none;
        }
        #product-intro{
            margin-bottom: -100px;
            margin-top:-60px;
        }
        #sausage-defination
        {
            width: 300px !important;
            text-align: justify;
        }
        #steak-defination
        {
            width: 300px !important;
            text-align: justify;
        }
        .navbar-logo
        {
            width: 120px;
            height: 120px;
        }
        .opx
        {
            padding:0px;
            margin-top:-100px;
        }
        #navbar-subscription-logo
        {
            width:50px;
            height:50px;
        }
        #premium-plan-title
        {
            font-size:14px;
        }
        #meat-board-text-font
        {
            font-size:16px;
        }
        #footer-logo{
          display: none;
        }
    }
    #meat-board-text-font
        {
            font-size:16px;
        }

    .product-listing-gallery
    {
        height:180px;
        width:100%;
        margin-top:60px;
    }
    body
    {
      overflow-x: hidden;
    }
    .menu
    {
      text-shadow: 1px 1px rgba(0,0,0,.3);
      width: auto;
      padding: 12px 0px;
      position: absolute;
      top:52%;
      right:2%;
      text-align: right;
      z-index:1000;
    }
    .menu-item:hover
    {
      cursor: pointer;
    }
    .menu-item
    {
      transition-duration: .2s;
      list-style-type: none;
      margin:10px;
      font-size:18px;
      text-align: justify;
    }
    .active
    {
       transition-timing-function:linear;
      transition-duration: .2s;
      font-size:20px;
    }


#fp-nav ul li a span, .fp-slidesNav ul li a span
{
 background-color:red !important;
 border:2px solid white !important;
 box-shadow: 0 5px 5px 0 rgba(0,0,0,0.14), 0 9px 12px 0 rgba(0,0,0,0.12), 0 6px 4px -6px rgba(0,0,0,0.2) !important;
 padding-left:4px;
 padding-top:4px;
}

#fp-nav ul li a.active span, .fp-slidesNav ul li a.active span, #fp-nav ul li:hover a.active span, .fp-slidesNav ul li:hover a.active span
{
content:"";
background-image: url("{{URL::to('/')}}/extra/dot-inner-2.png") !important;
width:12px;
background-size: cover;
display: block;
height: 12px;
}
#service-area-footer {
  width:auto;margin-left:35%;
}
</style>
