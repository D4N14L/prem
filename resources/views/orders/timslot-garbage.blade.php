<label  style="font-size:15px;font-weight:bold;display: block;margin-top:10px;margin-bottom:10px;;color:{{$backgroundColor}}" >Choose Timeslot</label>
                <?php $start_time = ""; $end_time = "" ?>
                
                @foreach (\App\GeneralTimeslot::all() as $general)
                  <?php
                
                  if($general->getTimeSlot()->shift == "morning")
                  {
                    if($general->getTimeSlot()->endTime == "12:00")
                    {
                      $end_time = "PM";
                    }
                    else 
                    {
                      $end_time = "AM";
                    }
                    if($general->getTimeSlot()->startTime == "12:00")
                    {
                      $start_time = "PM";
                    }
                    else 
                    {
                      $start_time = "AM";
                    }
                }
                  else
                  {

                    if($general->getTimeSlot()->endTime == "12:00")
                    {
                      $end_time = "AM";
                    }
                    else 
                    {
                    $end_time = "PM";
                    }

                    if($general->getTimeSlot()->startTime == "12:00")
                    {
                      $start_time = "AM";
                    }
                    else 
                    {
                      $start_time = "PM";
                    }
                  }













                   ?>
                     <p id="{{strtolower($general->getTimeSlot()->shift)}}">
                       <input onchange="highlightSelection(this)" class="with-gap timeslot-no-{{$general->id}}"  name="timeslot" value="{{$general->timeslot_id}}" type="radio" id="{{$general->id}}" />
                       <label style="font-family:Helvetica" id="timeslot-no-{{$general->id}}" for="{{strtolower($general->id)}}">{{ $general->getTimeSlot()->startTime }} <sup>{{$start_time}} </sup> - {{ $general->getTimeSlot()->endTime }} <sup>{{ $end_time }}</sup></label>
                     </p>
                 @endforeach