@extends('layouts.app2')
@section('content')
@include('styles')
  <?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>
  <style media="screen">
  .ui-widget-header{
    background: {{$backgroundColor}} !important;
    border:1px solid {{$backgroundColor}} !important;
  }

  .ui-state-default {
    border: 1px solid #cccccc !important;
    background: #f6f6f6 url("images/ui-bg_glass_100_f6f6f6_1x400.png") 50% 50% repeat-x !important;

  }
  [type="radio"]:disabled+label:hover{
    cursor:not-allowed;;
  }
  input[type="radio"]:checked + label {
    font-weight: 1000;

  }
  .dropdown-content li > a, .dropdown-content li > span {
    color: {{$backgroundColor}} !important;
}

  #pickerDate:hover{
    cursor:pointer;
  }
  </style>


  <link type="text/css" href="https://code.jquery.com/ui/1.11.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet" />

  <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script
  			  src="https://code.jquery.com/ui/1.9.1/jquery-ui.js"
  			  integrity="sha256-tXuytmakTtXe6NCDgoePBXiKe1gB+VA3xRvyBs/sq94="
  			  crossorigin="anonymous"></script>
          <script type="text/javascript" src="https://momentjs.com/downloads/moment.min.js">

          </script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.9.2/i18n/jquery-ui-i18n.js" type="text/javascript"></script>
<style media="screen">
    .page-heading
    {
      padding-top:5%
    }
[type="radio"]:checked+label:after, [type="radio"].with-gap:checked+label:before, [type="radio"].with-gap:checked+label:after{
  border:2px solid {{$backgroundColor}};
}
[type="radio"]:checked+label:after, [type="radio"].with-gap:checked+label:after{
  background: {{$backgroundColor}};
}
    @media only screen and (max-width: 768px)
    {
      .page-heading
      {
        padding-top:12%;
      }
      #ui-datepicker-div {
        position: absolute;
        top:75% !important;
      }

    }

    body
    {
      overflow-x: none;
    }
    .grid{
      display: grid;
      grid-template-areas: "extra delivery proceed";

    }

    .grid > div {
      border:1px dashed {{$backgroundColor}};
      padding:12px;
    }
    .proceed{
      grid-area: proceed;
    }
    .delivery{
      grid-area: delivery;
    }
    .extra{
      grid-area: extra;
    }
    @media screen and (max-width:768px) {
      .grid{
        grid-template-areas:
        "delivery"
        "extra"
        "proceed";

      }
    }
    label{
      color:{{$backgroundColor}} !important;
    }
</style>
  <div class="row page-heading" >
    <center>
      <div class="">
        <br><br>
        <p class="flow-text pink-text" style="color:{{$backgroundColor}} !important">
          Checkout Page
          <div class="divider" style="width:10%;height:2px;border-radius:15px;background-color:{{$backgroundColor}}"></div>
        </p>
      </div>
    </center>
  </div>

  <input type="hidden" id="deliveryCharges" name="" value="{{Auth::user()->getSuburbDeliveryCharges()}}">
  <div class="row card">
    <div class="card-content" >
      <form action="{{ route('payment.from') }}"  method="post">
        {{ csrf_field() }}

        <div class="grid">





            <div class="proceed">
              <button disabled type="button"  style="display:block;padding:8px;width:auto;margin:auto;;border:none;background: white;;color:{{$backgroundColor}};"> <b >Estimated Amount: <span style="font-family:verdana;font-size:14px">NZ $</span> <span style="font-family:Verdana">{{ (float) round($total,2) }}</span>  </b> <span style="font-weight:bold;font-family:Verdana" id="deliveryChargesHolder"> </span>  <b>  </b>
                <br>
              </button><br>
              <center>
                <a href="#" class="btn"   onclick="document.getElementById('form-delivery').click()" style="color:white;background-color:{{$backgroundColor}};border-radius:25px;font-size:12px"> <b>Proceed to checkout &nbsp; <i class="fa fa-shopping-cart" style="font-size:14px"></i> </b> </a>
              </center>
            </div>





            <div class="delivery">
              <input type="hidden" name="total" id="Total" value="{{(float) $total + (float) Auth::user()->getSuburbDeliveryCharges()}}">
              <p class="flow-text" style="font-size:16px;color:{{$backgroundColor}};font-weight:600">
                Choose Order Shipment Method
              </p><br>
              <p style="margin-top:10px">
                <input id="pickup" class="with-gap" name="delivery" type="radio" onclick="removeDeliveryCharges()"
                @if(session()->has('deliverySelected'))
                    @if(session()->get('deliverySelected') != '1')
                      {{ "checked" }}
                    @endif
                  @endif
                 value="0"  />
                <label style="color:{{$backgroundColor}};font-weight:bold" for="pickup">Self Pickup ( No delivery charges will be deducted )</label>
              </p>

              <p style="margin-top:10px">
                <input id="delivery" class="with-gap" name="delivery" type="radio" onclick="addDeliveryCharges()"
                  @if(session()->has('deliverySelected'))
                    @if(session()->get('deliverySelected') == '1')
                      {{ "checked" }}
                    @endif
                  @endif
                  value="1"  />
                <label style="color:{{$backgroundColor}};font-weight:bold" for="delivery"   >Delivery (  <span style="font-family:helvetica;font-size:14px">
                   NZ $  {{Auth::user()->getSuburbDeliveryCharges()}} 
                     will be added to the total amount based on your suburb <sup> {{ Auth::user()->getSuburbName() }} </sup>                     
                  </span>
                    )</label>
              </p>


              <hr style="border-style:dashed;border-color:{{$backgroundColor}};width:100%">
              <center style="display:block;color:{{$backgroundColor}};position:relative;left:25%;margin-top:3%">
                <p style="font-size:13px"> Delivery Timing: <i style="font-weight:bold">11:00 AM</i> <b>-</b> <i style="font-weight:bold">5:00 PM</i> </p>
              </center>

            </div>






            <div class="extra">
              <div class="input-field">
                  <select  id="orderDate" name="orderDate">
                    <option value="">Select Order Date</option>
                  </select>
              </div>


              <br>
              <div style="color:{{$backgroundColor}};font-size:12px">
                <p>No Service on <b>Monday</b> & <b>Public Holidays</b> </p>
              </div>
              
            </div>



        </div>

        <button type="submit" id="form-delivery" style="display:none"></button>
      </form>

      <br>
      <div class="col s12 m12 l12">
        <table class="table responsive-table bordered hover">
          <thead>
            <th>#</th>
            <th>Name</th>
            <th>Per Unit Price</th>
            <th>Quantity</th>
            <th>Sub Total</th>
            <th>Additional Information</th>
            <th>Service Type</th>
            <th>Action</th>
          </thead>
          <tbody>
            @foreach ($cart as $item)
              <?php
                $product = \App\Product::where('id','=',$item[0])->first();
                static $i = 0;
              ?>

              <tr id="tr-{{$product->id}}">
                <td>{{ $i }}</td>
                <td>{{ $product->name }}</td>
                <td> <span style="font-family:Verdana;font-size:14px"> NZ $ &nbsp; {{ $product->price }}</span>  / {{ ucfirst( strtolower($product->sellingUnit->name) == "pieces" || strtolower($product->sellingUnit->name) == "piece" ? 'kg': $product->sellingUnit->name ) }}
                </td>
                <td>
                    &nbsp;<span style="font-family:Verdana"> {{ (float) $item['quantity'] }}</span> &nbsp; {{$product->sellingUnit->name}}
                    {{-- {{ ucfirst( strtolower($product->sellingUnit->name) == "pieces" || strtolower($product->sellingUnit->name) == "piece" ? "kg":$product->sellingUnit->name  )  }} --}}
                </td>
                <td>
                  <span style="font-family:Verdana;font-size:14px">NZ $ &nbsp;{{ $item['priceMade'] }}</span>

                </td>
                <td>
                  <b>{{ is_null($item['description']) ? 'N/L':$item['description'] }}</b>
                </td>
                
                @if($item['serviceOption'] == '0')
                  <td style="font-weight:bold">No Service Needed!</td>
                @else
                  <td>
                    <ol>
                    @foreach ($item['serviceOption'] as $id)
                        <li style="font-weight:bold">{{ \App\ServiceType::where('id','=',$id)->first()->name  }}</li>
                    @endforeach
                  </ol>
                  </td>
                @endif
                <td> <button type="button" data-product-id="{{ $product->id }}" class="btn btn-floating red waves-effect" onclick="deleteOrder(this)"> <i class="fa fa-trash"></i> </button> </td>
              </tr>

              <?php $i++; ?>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>














<script type="text/javascript">


  $(document).ready(function(){
        /* $('#pickup').click() */

    /* $('select').material_select(); */
    /* $('select').on('contentChange',function(){
      $(this).material_select()
    }) */

    $(".dropdown-content>li>a").css("color", "{{$backgroundColor}}");

    var time = new moment()
    var current_dates = []
    var hour =  time.format('H')

    if(hour >= 10)
    {
      time.add(1,'day')
    }

    let startDate = time
    let endDate = new moment(time)
    endDate.add(3,'days')
    let diff = endDate.diff(startDate, 'days')
    let range = []
    for (let i = 0; i < diff; i++) {
      range.push(moment(startDate).add(i, 'days').format('Y-M-D'))
    }
    range.forEach(function(d){
      let opt = `<option value="${d}">${d}</option>`
      $('#orderDate').append(opt)
      $('#orderDate').material_select()
    });








  })
</script>

  <script type="text/javascript">
  let totalChargesWithDelivery = document.querySelector('#Total').value
    function addDeliveryCharges(){
      let dCharges = document.querySelector('#deliveryCharges').value

      document.querySelector('#deliveryChargesHolder').innerHTML =  ` + ${dCharges}`
      document.querySelector('#Total').value = totalChargesWithDelivery
    }
    function removeDeliveryCharges()
    {
      document.querySelector('#deliveryChargesHolder').innerHTML =  ``
      document.querySelector('#Total').value = {{$total}}
    }

  </script>

<script>

  function deleteOrder($el)
{
  $el.disabled = true;
  let product_id = $el.getAttribute('data-product-id');
  axios.post('{{ URL::to('/deleteSingleOrder') }}',{
      product_id:product_id,
      _token:"{{ csrf_token() }}"
  }).then(response => {
      document.querySelector(`#tr-${product_id}`).remove();
      toastr.success('Deleted From Cart');
      location.reload();
  }).catch(err => {
      toastr.error('Failed to delete this product from cart');
    })
}

</script>


/* <script>




  function choosedate()
{
  var time = new moment();

  var startAt10 =   new moment().format('HH');
  startAt10 = parseInt(startAt10)
  if(startAt10 >= 10)
  {
    $('#pickerDate').datepicker({
      minDate:'+1',
      maxDate:'+3r',
      dateFormat:'yy-mm-d',
      onSelect:function(dateText){
        this.focus()
        this.blur()
        changeTimeslot(dateText)
      }
    }, $.datepicker.regional[ "" ]).datepicker("setDate", new Date());
  } 
  else {
    $('#pickerDate').datepicker({
      minDate:'0',
      maxDate:'+2r',
      dateFormat:'yy-mm-d',
      onSelect:function(dateText){
        this.focus()
        this.blur()
        changeTimeslot(dateText)
      }
    }, $.datepicker.regional[ "" ]).datepicker("setDate", new Date());

  }
 
  
}
</script>  */




@endsection
