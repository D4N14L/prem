<?php $admin_logo_img = Voyager::setting('site.logo'); ?>
<?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>






<!doctype html>


<html lang="{{ app()->getLocale() }}">
    <head>
        <noscript><meta http-equiv="refresh"content="0; url=/enable-javascript"></noscript>

      {{-- @include('toaster') --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
        
        <script>

            $(document).keydown(function(e){
                if(e.which === 123){
                   return false;
                }
            });
            $(document).bind("contextmenu",function(e) {
                  e.preventDefault();
            });
            
            
            document.onkeydown = function(e) {
            if(event.keyCode == 123) {
            return false;
            }
            if(e.ctrlKey && e.keyCode == 'E'.charCodeAt(0)){
            return false;
            }
            if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
            return false;
            }
            if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
            return false;
            }
            if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
            return false;
            }
            if(e.ctrlKey && e.keyCode == 'S'.charCodeAt(0)){
            return false;
            }
            if(e.ctrlKey && e.keyCode == 'H'.charCodeAt(0)){
            return false;
            }
            if(e.ctrlKey && e.keyCode == 'A'.charCodeAt(0)){
            return false;
            }
            if(e.ctrlKey && e.keyCode == 'F'.charCodeAt(0)){
            return false;
            }
            if(e.ctrlKey && e.keyCode == 'E'.charCodeAt(0)){
            return false;
            }
            }
            </script>
            
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
	      <meta name="google-site-verification" content="L1-CIHVtDLBsreI9YiimEGBwAVWziuQ5MY5bBzC1TLM" />
        <title>Premium Meat</title>
        <link rel="shortcut icon" href="{{URL::to("/extra/logo.png")}}" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
        

{{--
        <link rel="shortcut icon" href="{{Voyager::image($admin_logo_img)}}" /> --}}

        <!-- Fonts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.green.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.7/jquery.fullpage.css">
        <link href="https://fonts.googleapis.com/css?family=Great+Vibes|Raleway|Lobster" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.7/jquery.fullpage.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://yarnpkg.com/en/package/normalize.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
        <!-- Styles -->

        @include('welcome-styles')
    </head>
    <body>

        @include('app-badges')
        @include('navbar')
        <br><br>
        @include('notifications')
        <div id="modal1" class="modal bottom-sheet" style="z-index:1000000;max-height:90%;height:90%">

                <div class="modal-content" >
                  <a href="#!"  class="modal-action modal-close waves-effect btn-small waves-black white-text right btn-floating" style="background-color:{{$backgroundColor}};" > <i class="fa fa-close" style="font-size:14px"></i> </a>

                  <div  style="text-align:center">
                          <span class="black-text"  >
                              <p class="flow-text" id="premium-plan-title" >
                                Auckland Service Areas
                                <center>
                                  <div class="divider" style="background-color:{{$backgroundColor}};width:10%"></div>
                                </center>
                                  </p>
                          </span>
                          <div class="suburb-grid">
                            @foreach (\App\Suburb::orderBy('suburbName','asc')->get() as $suburb)
                              <p style="color:{{$backgroundColor}};font-size:14px;width:100%;text-align:left;">{{$suburb->suburbName}}</p>
                            @endforeach
                          </div>

                      </div>
                    </div>

              </div>

        <div id="fullpage" >
            @include('carousel')
            @include('categories')
            @include('sausage')
            @include('steak')
            @include('footer')
            @include('footer-black')
            
    
    
        </div>


        @include('script')
        @include('toaster')

        @if(session()->has('failed'))
          <script type="text/javascript">
          toastr.error('{!! session()->get('failed') !!}');
          </script>
        @endif
        @if(session()->has('success'))
          <script type="text/javascript">
          toastr.success('{!! session()->get('success') !!}');
          </script>
        @endif

        </body>

        </html>
