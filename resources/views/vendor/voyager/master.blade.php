<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" @if (config('voyager.multilingual.rtl')) dir="rtl" @endif>
<head>
    {{-- @include('toaster') --}}
    <title>@yield('page_title', setting('admin.title') . " - " . setting('admin.description'))</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ voyager_asset('images/logo-icon.png') }}" type="image/x-icon">

    {{-- Data Tables --}}
    {{-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css"> --}}

    <!-- App CSS -->

    <link rel="stylesheet" href="{{ URL::to('').'/css/voyager.css' }}">
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

    @yield('css')
    @if(config('voyager.multilingual.rtl'))
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-rtl.css">
        <link rel="stylesheet" href="{{ voyager_asset('css/rtl.css') }}">
    @endif

    <!-- Few Dynamic Styles -->
    <style type="text/css">
        .voyager .side-menu .navbar-header {
            background:{{ config('voyager.primary_color','#7f0000') }};
            border-color:{{ config('voyager.primary_color','#7f0000') }};
        }
        .widget .btn-primary{
            border-color:{{ config('voyager.primary_color','#7f0000') }};
        }
        .widget .btn-primary:focus, .widget .btn-primary:hover, .widget .btn-primary:active, .widget .btn-primary.active, .widget .btn-primary:active:focus{
            background:{{ config('voyager.primary_color','#7f0000') }};
        }
        .voyager .breadcrumb a{
            color:{{ config('voyager.primary_color','#7f0000') }};
        }
    </style>

    @if(!empty(config('voyager.additional_css')))<!-- Additional CSS -->
        @foreach(config('voyager.additional_css') as $css)<link rel="stylesheet" type="text/css" href="{{ asset($css) }}">@endforeach
    @endif

    @yield('head')
</head>

<body class="voyager @if(isset($dataType) && isset($dataType->slug)){{ $dataType->slug }}@endif">
        {!! Toastr::render() !!}

<div id="voyager-loader">
    <?php $admin_loader_img = Voyager::setting('admin.loader', ''); ?>
    @if($admin_loader_img == '')
        <img src="{{ voyager_asset('images/logo-icon.png') }}" alt="Voyager Loader">
    @else
        <img src="{{ Voyager::image($admin_loader_img) }}" alt="Voyager Loader">
    @endif
</div>
<audio src="{{URL::to('/')}}/assets/slow-spring-board.mp3" id="notification-sound" type="audio/mp3"></audio>
<?php
if (starts_with(Auth::user()->avatar, 'http://') || starts_with(Auth::user()->avatar, 'https://')) {
    $user_avatar = Auth::user()->avatar;
} else {
    $user_avatar = Voyager::image(Auth::user()->avatar);
}
?>

<div class="app-container">
    <div class="fadetoblack visible-xs"></div>
    <div class="row content-container">
        @include('voyager::dashboard.navbar')
        @include('voyager::dashboard.sidebar')
        <script>
            (function(){
                    var appContainer = document.querySelector('.app-container'),
                        sidebar = appContainer.querySelector('.side-menu'),
                        navbar = appContainer.querySelector('nav.navbar.navbar-top'),
                        loader = document.getElementById('voyager-loader'),
                        hamburgerMenu = document.querySelector('.hamburger'),
                        sidebarTransition = sidebar.style.transition,
                        navbarTransition = navbar.style.transition,
                        containerTransition = appContainer.style.transition;

                    sidebar.style.WebkitTransition = sidebar.style.MozTransition = sidebar.style.transition =
                    appContainer.style.WebkitTransition = appContainer.style.MozTransition = appContainer.style.transition =
                    navbar.style.WebkitTransition = navbar.style.MozTransition = navbar.style.transition = 'none';

                    if (window.localStorage && window.localStorage['voyager.stickySidebar'] == 'true') {
                        appContainer.className += ' expanded no-animation';
                        loader.style.left = (sidebar.clientWidth/2)+'px';
                        hamburgerMenu.className += ' is-active no-animation';
                    }

                   navbar.style.WebkitTransition = navbar.style.MozTransition = navbar.style.transition = navbarTransition;
                   sidebar.style.WebkitTransition = sidebar.style.MozTransition = sidebar.style.transition = sidebarTransition;
                   appContainer.style.WebkitTransition = appContainer.style.MozTransition = appContainer.style.transition = containerTransition;
            })();
        </script>
        <!-- Main Content -->
        <div class="container-fluid">
            <div class="side-body padding-top">
                @yield('page_header')
                <div id="voyager-notifications"></div>
                @yield('content')
            </div>
        </div>
    </div>
</div>
@include('voyager::partials.app-footer')

<!-- Javascript Libs -->

<script type="text/javascript" src="{{ URL::to('').'/js/voyager.js' }}"></script>



<script>
    // @if(Session::has('alerts'))
    //     let alerts = {!! json_encode(Session::get('alerts')) !!};
    //     helpers.displayAlerts(alerts, toastr);
    // @endif

    // @if(Session::has('message'))

    // // TODO: change Controllers to use AlertsMessages trait... then remove this
    // var alertType = {!! json_encode(Session::get('alert-type', 'info')) !!};
    // var alertMessage = {!! json_encode(Session::get('message')) !!};
    // var alerter = toastr[alertType];

    // if (alerter) {
    //     alerter(alertMessage);
    // } else {
    //     toastr.error("toastr alert-type " + alertType + " is unknown");
    // }

    // @endif
</script>

@yield('javascript')

@if(!empty(config('voyager.additional_js')))<!-- Additional Javascript -->
    @foreach(config('voyager.additional_js') as $js)  <script type="text/javascript" src="{{ asset($js) }}"></script>@endforeach
@endif


{{-- <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script> --}}
{{-- <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script> --}}
<script src="//js.pusher.com/3.1/pusher.min.js"></script>


<script type="text/javascript">
  $(document).ready(function(){
//    delete window.Pusher;
document.querySelector('#notification-sound').onended = function()
{
  if(window.location.pathname = "/admin/orders")
  {
    setTimeout(function () {
      window.location.reload()
    }, 1000);
  }
}

    var pusher = new Pusher('af36034a25fc49f29c41', {
         encrypted: true
       });
    let channel = pusher.subscribe('New.Order')
    channel.bind('App\\Events\\NotifyAdminNewOrder', function(data) {


      document.querySelector('#notification-sound').play()
      toastr.success('New Order Has Been Placed!');
    })
    // $('table').addClass('compact')
    // $('html').css({'zoom':'90%'}) 
    // $('table').DataTable({'autoWidth':false,'paginate':false,'order':[]});
    // $('.dataTables_wrapper').css({'marginLeft':'-14%','width':'80%'})
    // $('#DataTables_Table_0_filter').css({'align-text':'right'})
    // $('.page-content').css({'width':'100%'})
  
  });
</script>



















</body>
</html>
