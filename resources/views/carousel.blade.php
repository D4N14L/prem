<div id="sliders"  style="display:block;visibility:visible;opacity:1" class="section"   data-anchor="carousel">
  @foreach (\App\Slider::orderBy('arrange')->get() as $slide)
    <div class="slide"  data-anchor="slide1">
        <img style="object-fit:cover;width:100%;height:100vh" src="{{URL::to("/sliders/")}}/{{$slide->image}}" alt="">
    </div>
  @endforeach

        <center >
  <a href="{{URL::to('/#products')}}">
    
    <div class="scroll-down"  style="position:relative;top:-170px;z-index:100;color:#fff;background-color:rgb(194, 30, 86,0.35);width:120px;height:120px;border-radius:100%;">
                    <svg width="40px" height="100%"  viewBox="0 0 247 390" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:1.5;">
                            <path id="wheel" d="M123.359,79.775l0,72.843" style="fill:none;stroke:#fff;stroke-width:20px;"/>
                            <path id="mouse" d="M236.717,123.359c0,-62.565 -50.794,-113.359 -113.358,-113.359c-62.565,0 -113.359,50.794 -113.359,113.359l0,143.237c0,62.565 50.794,113.359 113.359,113.359c62.564,0 113.358,-50.794 113.358,-113.359l0,-143.237Z" style="fill:none;stroke:#fff;stroke-width:20px;"/>
                    </svg><br>
                    <span style="color:rgb(194, 30, 86);font-size:18px;color:white;text-shadow:0px 0px 1px white">Scroll down</span>
    </div>
  </a>
        </center>
</div>
