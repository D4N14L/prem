@extends('layouts.app')

@section('content')
  <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    {!! Toastr::render() !!}

  <?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>
  <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}"></script>
  <?php
    $userSuburb;
    if(\App\Suburb::where('id','=',Auth::user()->suburb_id)->exists())
    {
      $userSuburb = \App\Suburb::where('id','=',Auth::user()->suburb_id)->first();
    }
    $userCity;
    if($userSuburb){
      $userCity =   \App\Area::where('id','=',$userSuburb->areas_id)->first();
    }
  ?>

  <style media="screen">
  .select-dropdown{
    color:{{$backgroundColor}} !important;
  }
  </style>
  <?php $ethnics = [
    'NZ European',
    "Chinese",
    "Arabs",
    "Pakistanies",
    "Indians",
    "Fiji Indians",
    "Afghanies",
    "Bangalies",
    "Pacific Islands",
  ];?>
<style media="screen">
  body{
    overflow-y: scroll;;
  }
  input[disabled] {
    cursor: no-drop !important;
  }
  a:hover
  {
    cursor: pointer;
  }
  label
  {
    color:{{$backgroundColor}} !important;

  }
  @if(url()->current() == URL::to('/').'/register')
  #logo-main
  {
    display: none;
  }
  @endif
  .invalid-feedback{
    color:red;
  }
  .invalid-feedback::before{
    content:'*';
  }
</style>
<div class="container">
  <div id="eula" class="modal">
      <div class="modal-content"  style="word-wrap:break-word;text-align:justify !important;;">
        <p >
           {!! \App\Portions::where('portion_1','=',5)->first()->body !!}

        </p>
      </div>
      <div class="modal-footer" >
        <a style="position:absolute;top:0%;right:0%" href="#!" class="modal-close waves-effect  pink btn-floating"> <i class="fa fa-times"></i> </a>
      </div>
    </div>

    <div class="row justify-content-center" style="padding-top:10%">
        <div class="col-md-8">
            <div class="card">
                <div class="card-content">
                  <a style="background-color:{{$backgroundColor}} !important" onclick="history.back()"  class="btn btn-floating pink"> <i class="fa fa-arrow-circle-left white-text"></i> </a>
                  <center>
                    <div class="navbar-logo  z-depth-2 hide-on-med-and-down">
                    </div>
                  </center><br>

                    <center>
                      <br>
                      <div class="card-title pink-text"> <h5 style="font-size:14px;color:{{$backgroundColor}}">Edit / Update Profile Information <br>                      </h5>
                        <div class="divider pink"  style="width:10%;background-color:{{$backgroundColor}} !important">

                        </div>
                     </div>
                   </center>


                    <form  method="POST" action="{{ route('user.edit.profile') }}">
                        @csrf

                        <div class=" row">

                            <div class="input-field col s12 m6 l6">
                              <label for="name">{{ __('Name') }}</label>
                                <input id="name" type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ Auth::user()->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col l6 m6 s12 input-field">
                              <label for="email">E-Mail Address <sup style="font-weight:bold;font-size:12px">* This field can't be edited</sup> </label>
                                <input id="email" type="email"  disabled  class="{{ $errors->has('email') ? ' is-invalid' : '' }} " name="email" value="{{ Auth::user()->email }}" >

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class=" row">


                          <div class="input-field col s12 m4 l4">
                              <select id="cityList" onchange="updateSuburbList(this)" >
                                <option value="0" style="color:{{$backgroundColor}}" disabled selected >Choose City</option>
                                @foreach (\App\Area::orderBy('cityName','asc')->get() as $city)
                                  @if($userCity)
                                      @if($userCity->id == $city->id)
                                      <option value="{{$city->id}}" selected>{{$city->cityName}}</option>
                                      @else
                                        <option value="{{$city->id}}">{{$city->cityName}}</option>
                                      @endif
                                  @else
                                    <option value="{{$city->id}}">{{$city->cityName}}</option>
                                @endif

                                @endforeach
                              </select>


                              @if ($errors->has('suburb_id'))
                                  <span class="invalid-feedback" role="alert">
                                      <strong>Choose Your City</strong>
                                  </span>
                              @endif
                          </div>



                            {{-- SUBURB LIST --}}


                          <div class="input-field col s12 m4 l4">

                              <select id="suburbList" name="suburb_id" >
                                  @if(!$userSuburb)
                                    <option value="" selected disabled >Select Suburb</option>
                                  @else
                                  <option value="{{$userSuburb->id}}" selected  >{{$userSuburb->suburbName}}</option>
                                @endif
                              </select>
                              @if ($errors->has('suburb_id'))
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('suburb_id') }}</strong>
                                  </span>
                              @endif
                          </div>









                            <div class="input-field col s12 m4 l4">
                              <label for="name">{{ __('Street, House Address') }}</label>
                                <input id="address" type="text" class="{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ Auth::user()->address }}" required>

                                @if ($errors->has('address'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col l4 m4 s12 input-field">
                              <label for="email">{{ __('Landline') }}</label>
                                <input id="landline" type="text" class="{{ $errors->has('landline') ? ' is-invalid' : '' }}" name="landline" value="{{ Auth::user()->landline }}" required>

                                @if ($errors->has('landline'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('landline') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col l4 m4 s12 input-field">
                                  <select name="ethnic" required>
                                    @foreach ($ethnics as $ethnic)
                                        <option value="{{$ethnic}}" <?php trim(Auth::user()->ethnic) == $ethnic ? 'selected':'';  ?> > {{ $ethnic }} </option>
                                    @endforeach
                                  </select>
                                  <label>{{ __('Ethnic') }}</label>

                                @if ($errors->has('ethnic'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ethnic') }}</strong>
                                    </span>
                                @endif
                            </div>


                        </div>

                        <div class="row">
                          <div class="col l8 m8 s12 input-field">
                            <div style="font-weight:bold;color:{{$backgroundColor}}">*Enter new password Or leave the fields empty if you don't want to change password</div>
                          </div>
                          <div class="col l4 m4 s12 input-field">
                            <button is-edit="0" type="button" onclick="toggleEdit(this)"  class="right btn flat" style="background:{{$backgroundColor}};text-transform:none !important;text-align:justify"> <i class="fa fa-pencil" ></i> Edit Password</button>
                          </div>
                            <div class="col l6 m6 s12 input-field">
                              <label for="password">Enter New Password</label>
                                <input id="password" type="password" disabled class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col m6 l6 s12 input-field">
                              <label for="password-confirm" >Re-type New Password</label>
                                <input id="password-confirm" disabled type="password" class="" name="password_confirmation" required>
                            </div>
                        </div>
                        <div class=" row ">
                            <div class="col-md-6 offset-md-4">
                                <button style="margin-bottom:12px;width:210.64px;background-color:{{$backgroundColor}} !important" type="submit" class="btn pink white-text left" >
                                    {{ __('Updadte Profile') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>


<style media="screen">
  ul.dropdown-content.select-dropdown li span {
      color: #e91e63; /* no need for !important :) */
  }
</style>


<script type="text/javascript">
  $(document).ready(function(){
    $('select').material_select();
  });
  function toggleEdit($el){
    let edit = '<i class="fa fa-pencil" ></i> Edit Password';
    let noEdit = '<i class="fa fa-check" ></i> Finish Editing';
    let editStatus = $el.getAttribute('is-edit');
    if(editStatus == 0){
      $('#password').prop('disabled',false);
      $('#password-confirm').prop('disabled',false);
      $el.setAttribute('is-edit',1)
      $el.innerHTML = noEdit;
    }
    else {
      $('#password').prop('disabled',true);
      $('#password-confirm').prop('disabled',true);
      $el.setAttribute('is-edit',0)
      $el.innerHTML = edit;

    }
  }

</script>


<script type="text/javascript">
  function updateSuburbList($el){
    let area_id = $el.options[$el.selectedIndex].value;
    let suburbList =   document.querySelector('#suburbList');
        suburbList.innerHTML = "<option disabled selected>Loading Suburbs...</option>";
        $('select').material_select();
          axios.get(`{{route('get.suburb.by.area')}}`,{
            params:{
              area_id
            }
          }).then( ({data}) => {
              suburbList.innerHTML = "<option disabled selected>Choose Suburbs</option>";
              data.forEach( suburb => {
                  let option = document.createElement('option');
                  option.value = suburb.id;
                  option.innerHTML = suburb.suburbName;
                  document.querySelector('#suburbList').appendChild(option);
              });
              $('select').material_select();
          });
  }
</script>








@endsection
