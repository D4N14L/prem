@extends('layouts.app')
@section('content')
  <?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>
  <style media="screen">
    .amount-font{
      font-family: Helvetica;
      font-weight: 600;
    }
    td{
      text-align: justify;
    }
    .modal-trigger:hover {
      cursor: pointer;
    }
    th,td{
      text-align: center;
    }
    .container {
      font-size:12px;
    }
    .fixed-action-btn:hover {
      background-color: transparent;
      outline:0px !important;
    }
  </style>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://cdn.jsdelivr.net/npm/clipboard@2/dist/clipboard.min.js"></script>
  <script src="https://unpkg.com/tippy.js@3/dist/tippy.all.min.js"></script>

  <a class="btn btn-large pink" style="position:absolute;top:12%;;font-size:12px;right:3%;z-index:1000;background-color:{{$backgroundColor}} !important;color:white" href="{{route('user.get.edit.profile')}}" >
    <i style="font-size:12px" class="fa fa-pencil"></i> Edit Profile
  </a>

    <div  class="hide-on-large-only">
        <br><br><br><br>
    </div>
  <div class="container" style="padding-top:5%;margin-bottom:0px;">

    <div class="row">
      @if(Auth::user()->isVerified == 0)
        <div class="col s12 m12 l12">
          <div class="card z-depth-2">
            <div class="card-content red" style="background-color:{{$backgroundColor}} !important">
            <p class="flow-text" style="font-size:14px;color:white;font-weight:bold">
            <i class="fa fa-envelope" style="font-size:24px"></i> &nbsp;
            Confirmation mail was sent to  <b>{{ Auth::user()->email }} </b>. &nbsp;Unverified accounts are expired after 2 business days
            </p>
          </div>
        </div>

      </div><br>
      @endif
      @if(is_countable(session()->get('cart')) && count(session()->get('cart')) > 0)
        <div class="col s12 m12 l12">
          <div class="card z-depth-2">
            <div class="card-content pink" style="background-color:{{$backgroundColor}} !important">
                    <p class="flow-text" style="font-size:14px;color:white;font-weight:bold">
                    <i class="fa fa-shopping-cart" style="font-size:24px"></i> &nbsp;
                    You Have Pending Items In Cart, Please Proceed To Checkout To Grab That Items, <a href="/checkout" style="border-radius:25px;text-transform:none" class="btn white black-text right">Click Here</a>
                    </p>
          </div>

        </div>

        </div>

      @endif
      <div class="col s12 m3 l3">
        <div class="card">
            <div class="card-content">
              <div class="row">
                <div class="col s4 l4 m4">
                  <p >
                    <i class="fa fa-shopping-cart" style="font-size:32px"></i>
                  </p>
                </div>
                <div class="col s8 m8 l8">
                  <p  style="text-align:center">
                    <center>
                      <span style="font-weight:bold">

                        Total Orders
                      </span>
                      <span class="white-text pink" style="background-color:{{$backgroundColor}} !important;width:25px;height:25px;border-radius:100%;padding:2px;display:block;text-align:center">
                       @php $counts = \App\Order::where('customer_id','=',Auth::user()->id)->get() @endphp
                        {{ $counts && is_countable($counts) ? $counts->count() : 0 }}
                      </span>

                    </center>
                  </p>

                </div>
              </div>
              <p>
                <i class="fa fa-info-circle grey-text"></i>

                <span style="font-size:12px;" class="grey-text"> Total  Orders</span>
              </p>

            </div>
        </div>

      </div>









      <div class="col s12 m3 l3">
        <div class="card">
            <div class="card-content">
              <div class="row">
                <div class="col s4 l4 m4">
                  <p >
                    <i class="fa fa-users" style="font-size:32px"></i>
                  </p>
                </div>
                <div class="col s8 m8 l8">
                  <p  style="text-align:center">
                    <center>
                      <span style="font-weight:bold">

                        Total Referals
                      </span>
                      <span class="white-text pink" style="background-color:{{$backgroundColor}} !important;width:25px;height:25px;border-radius:100%;padding:2px;display:block;text-align:center">
                        @php $counts = \App\Referal::where('referer_id','=',Auth::user()->id)->get() @endphp
                        {{ $counts && is_countable($counts) ? $counts->count() : 0 }}
                      </span>

                    </center>
                  </p>

                </div>
              </div>
              <p>
                <i class="fa fa-info-circle grey-text"></i>

                <span style="font-size:12px;" class="grey-text"> Total  Referals</span>
              </p>

            </div>
        </div>

      </div>









      <div class="col s12 m3 l3">
        <div class="card">
            <div class="card-content">
              <div class="row">
                <div class="col s4 l4 m4">
                  <p >
                    <i class="fa fa-paper-plane" style="font-size:32px"></i>
                  </p>
                </div>
                <div class="col s8 m8 l8">
                  <p  style="text-align:center">
                    <center>
                      <span style="font-weight:bold">
                         Delivered
                      </span>
                      <span class="white-text pink modal-trigger" data-target="delivered-modal" style="background-color:{{$backgroundColor}} !important;width:25px;height:25px;border-radius:100%;padding:2px;display:block;text-align:center">
                        @php $counts = \App\Order::where('customer_id','=',Auth::user()->id)->where('order_status','=',1)->get() @endphp
                        {{ $counts && is_countable($counts) ? $counts->count() : 0 }}
                      </span>

                    </center>
                  </p>

                </div>
              </div>
              <p>
                <i class="fa fa-info-circle grey-text"></i>

                <span style="font-size:12px;" class="grey-text"> Delivered Orders</span>
              </p>

            </div>
        </div>

      </div>



      <div class="col s12 m3 l3">
        <div class="card">
            <div class="card-content">
              <div class="row">
                <div class="col s4 l4 m4">
                  <p >
                    <i class="fa fa-clock" style="font-size:32px"></i>
                  </p>
                </div>
                <div class="col s8 m8 l8">
                  <p  style="text-align:center">
                    <center>
                      <span style="font-weight:bold">
                         Pending
                      </span>
                      <span class="white-text pink modal-trigger" data-target="pending-modal" style="background-color:{{$backgroundColor}} !important;width:25px;height:25px;border-radius:100%;padding:2px;display:block;text-align:center">
                        @php $counts = \App\Order::where('customer_id','=',Auth::user()->id)->where('order_status','=',0)->get() @endphp
                        {{ $counts && is_countable($counts) ? $counts->count() : 0 }}
                      </span>

                    </center>
                  </p>

                </div>
              </div>
              <p>
                <i class="fa fa-info-circle grey-text"></i>

                <span style="font-size:12px;" class="grey-text"> Pending Orders</span>
              </p>

            </div>
        </div>

      </div>

      <div class="col s12 m12 l12">
        <div class="card z-depth-2">
          <div class="card-content red" style="background-color:{{$backgroundColor}} !important">
                  <p class="flow-text" style="font-size:14px;color:white;font-weight:bold">
                  <i class="fa fa-arrow-up" style="font-size:24px"></i> &nbsp; <span style="color:white;font-size:16px">Promotion Link                     <span style="font-size:12px">(Share Below Link To Get Discounts)</span>  </span>
                  <br><br>
                  <p class="z-depth-1" style="padding:12px 24px;border-radius:25px;text-shadow:#000 1px 1px 2px ;font-size:13px;color:white;word-wrap:break-word;display:block" id="referal_link_text">
                    {{URL::to('/')}}/refer/{{ str_replace('/','.',Auth::user()->referral_link) }}
                  </p><br><br>
                  <button  type="button" style="color:white !important;background-color:{{$backgroundColor}} !important;border-radius:25px;font-size:bold" id="trigger-copy" data-clipboard-action="copy" data-clipboard-target="#referal_link_text" class="right z-depth-2 btn white black-text" name="button">Click To Copy</button>
                      <br><br>
                  </p>
        </div>


      </div>

      </div><br>









    </div>
    <div class="row card">
      <div class="col s12 m12 l12 card-content">
        <h5 class="pink-text" style="color:{{$backgroundColor}} !important;font-weight:bold">
          Your Orders
        </h5>
        <br>
        <table class="bordered responsive-table">
          <thead>
            <tr>
              <th>  #
              </th>
              <th>
                Status
              </th>
              <th>
                Shipment
              </th>
              <th>Estd. Amount</th>
              <th>Extra Amount</th>
              <th>
                Actual Amount
              </th>
              <th style="text-align:center">Total Amount <br> + <br> Shipment</th>
              <th>
                Placed On
              </th>
            </tr>
          </thead>
          <tbody>
            <?php $i=0; ?>
            @if(is_countable($orders) && $orders->count() > 0)
              @foreach ($orders as $order)
                <?php $i++; ?>
                <?php $amount = 0; ?>
                <tr>
                  <td class="font-weight-medium">
                    {{ $i }}
                  </td>
                  <td>
                    <i>{!! $order->order_status == 0 ? '<i style="font-weight:600;color:orange">Pending</i>':'<i style="font-weight:600;color:green">Completed</i>' !!}</i>
                  </td>
                  <td>
                    @php $isDelivery = false; @endphp
                    @if($order->isDelivery == 0)
                      <span > <i style="font-weight:600" class="indigo-text">Self</i> </span>
                    @else
                    @php $isDelivery = true @endphp
                      <span> <i style="font-weight:600" class="blue-text">Delivery</i> </span>
                    @endif
                  </td>


                  <td class="amount-font">
                    <span style="font-size:12px">NZ $ &nbsp;</span>
                    {{$order->total_amount}}
                  </td>
                  <td class="amount-font">
                    <span style="font-size:12px">NZ $ &nbsp;</span>
                    <?php $totalExtraAmount = 0;
                        foreach ($order->OrderInfo() as $orderInfo) {
                          $totalExtraAmount += $orderInfo->extra_payment;
                        }
                    ?>
                    {{ $totalExtraAmount }}
                  </td>
                  <td class="amount-font">
                    <span style="font-size:12px">NZ $ &nbsp;</span>
                    <?php
                    $ActualAmount = 0;
                    if($order->total_actual_amount <= 0){
                      $ActualAmount = $totalExtraAmount + $order->total_amount;
                    }else {
                      $ActualAmount = $order->total_actual_amount;
                    }
                    ?>
                    {{ $ActualAmount }}

                  </td>
                  <td class="amount-font">
                    <span style="font-size:12px">NZ $ &nbsp;</span>
                    @if($isDelivery)
                  {{(float) $ActualAmount + (float) \App\Miscellaneous::first()->deliveryCharges}}
                    @else
                      {{(float) $ActualAmount + (float) 0}}

                    @endif
                  </td>




                  <td style="font-weight:600">
                    {{ $order->created_at->format('l d, F') }}
                  </td>
                </tr>
                <tr>
                  <td colspan="9">
                    <ul class="collapsible white"  >
                        <li>
                          <div class="collapsible-header" style="font-weight:bold;background-color:{{$backgroundColor}};color:white;border:none">Click To View Details</div>
                          <div class="collapsible-body white">
                            <table class="table-responsive bordered">
                              <thead>
                                <th>Product Name</th>
                                <th>Product Price</th>
                                <th>Service Options</th>
                                <th>Quantity</th>
                                <th>Avg. Weight</th>
                                <th>Estd. Amount</th>
                                <th>Extra Quantity</th>
                                <th>Extra Payment</th>
                                <th>Act. Amount</th>
                              </thead>
                              <tbody>
                                @foreach (\App\OrderInfo::where('order_id','=',$order->id)->get() as $oi)
                                  <tr>

                                    <td style="font-weight:600">{{ $oi->Product()->name }}</td>
                                    <?php $sellingUnit = (strtolower($oi->Product()->sellingUnit->name) == "piece" || strtolower($oi->Product()->sellingUnit->name) == "pieces") ? true : false;   ?>
                                    <td style="font-weight:600">$ {{ $oi->Product()->price }} / <b>{{  $sellingUnit ? 'Kg' : $oi->Product()->sellingUnit->name }}</b></td>
                                      <td style="font-weight:600">
                                      @if(is_countable(json_decode($oi->service_type_option)) && count(json_decode($oi->service_type_option)) <= 0)

                                          <i style="font-size:12px">{{ 'No Service Requested' }}</i>

                                        @else
                                        @php 
                                          // dd(json_decode($oi->service_type_option));
                                        @endphp
                                          <ol>
                                            @if(is_countable(json_decode($oi->service_type_option)))
                                            @foreach (json_decode($oi->service_type_option) as  $serviceOptionItem)
                                              <li>
                                                {{ \App\ServiceType::where('id','=',$serviceOptionItem)->first()->name  }}</b> </sup>
                                              </li>
                                            @endforeach
                                            @endif
                                              </ol>
                                      @endif
                                    </td>


                                    <td style="font-weight:600" id="table-quantity-{{$oi->id}}">
                                        {{ $oi->quantity }}  <b>{{ $oi->Product()->sellingUnit->name }}</b>
                                    </td>

                                    <td style="font-weight:600">
                                      {!! is_null($oi->Product()->avg_weight) ? "<i>N/A</i>" : $oi->Product()->avg_weight.'<sup>Kg</sup>' !!}
                                    </td>

                                    {{-- ESTIMATED AMOUNT --}}

                                    <td style="font-weight:600" class="product-subtotal" id="table-subtotal-{{$oi->id}}">
                                      NZ $ {{$oi->amount}}
                                    </td>


                                    <td style="font-weight:600">

                                        {{ (float)$oi->extra_quantity}}
                                   </td>

                                    {{-- EXTRA AMOUNT --}}

                                    <td style="font-weight:600" class="extra-payments-column" id="extra_payment_{{$oi->id}}">
                                      NZ $ {{(float)$oi->extra_payment}}
                                    </td>

                                    {{-- ACTUAL AMOUNT --}}

                                  <td style="font-weight:600" class="actual-payments-column"  id="actual_amuount_column_{{$oi->id}}">{{ (float) $oi->amount + (float) $oi->extra_payment }}</td>
                                  </tr>
                                @endforeach
                              </tbody>
                            </table>
                          </div>
                        </li>
                    </ul>
                  </td>
                </tr>
              @endforeach
            @endif
          </tbody>
        </table>

        <br>
        <center>
          {!! $orders->links('vendor.pagination.materializecss') !!}
        </center>
      </div>
    </div>
    <div class="row card">
      <div class="col s12 m12 l12 card-content">
        <h5 class="pink-text" style="font-weight:bold;color:{{$backgroundColor}} !important">
          Your Referals
        </h5>
        <br>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>  #
              </th>
              <th>
                Name
              </th>
              <th>
                Email
              </th>
              <th>
                Last Order
              </th>
              <th>
                Refered On (ago)
              </th>
              <th>
                Refered On (Date)
              </th>

            </tr>
          </thead>
          <tbody>
            <?php $i=1; ?>
    @foreach (\App\Referal::where('referer_id','=',Auth::user()->id)->get() as $referal)
      <tr>
        <td>{{$i}}</td>
        <td>{{'@'.$referal->info()->name}}</td>
        <td>{{ $referal->info()->email }}</td>
        <td>{!! $referal->getLastOrder() !!}</td>
        <td>{{  $referal->info()->created_at->diffForHumans()  }}</td>
        <td>{{  $referal->info()->created_at->format('D M Y')  }}</td>
      </tr>
      <?php $i++; ?>
    @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>




</div>



<style media="screen">
  .modal {
    max-width: 100% !important;
    width:80%;
  }
  .modal-content * {
    font-size:12px !important;
    font-family: Helvetica !important;
  }
</style>






  {{-- PENDING ORDERS MODAL START --}}










<div id="pending-modal" class="modal">
  <div class="modal-content">
    <center><h4 style="font-size:16px;font-weight:bold">Pending Orders Details</h4></center>

    <table class="bordered responsive-table">
      <thead>
        <tr>
          <th>  #
          </th>
          <th>
            Status
          </th>
          <th>
            Shipment
          </th>
          <th>Estd. Amount</th>
          <th>Extra Amount</th>
          <th>
            Actual Amount
          </th>
          <th style="text-align:center">Total Amount <br> + <br> Shipment</th>
          <th>
            Placed On
          </th>
        </tr>
      </thead>
      <tbody>

            <?php $i=0; ?>
            @foreach ($orders as $order)
              @if($order->order_status == 0)
              <?php $i++; ?>
              <?php $amount = 0; ?>
              <tr>
                <td class="font-weight-medium">
                  {{ $i }}
                </td>
                <td>
                  <i>{!! $order->order_status == 0 ? '<i style="font-weight:600;color:orange">Pending</i>':'<i style="font-weight:600;color:green">Completed</i>' !!}</i>
                </td>
                <td>
                  @php $isDelivery = false; @endphp
                  @if($order->isDelivery == 0)
                    <span > <i style="font-weight:600" class="indigo-text">Self</i> </span>
                  @else
                  @php $isDelivery = true @endphp
                    <span> <i style="font-weight:600" class="blue-text">Delivery</i> </span>
                  @endif
                </td>


                <td class="amount-font">
                  <span style="font-size:12px">NZ $ &nbsp;</span>
                  {{$order->total_amount}}
                </td>
                <td class="amount-font">
                  <span style="font-size:12px">NZ $ &nbsp;</span>
                  <?php $totalExtraAmount = 0;
                      foreach ($order->OrderInfo() as $orderInfo) {
                        $totalExtraAmount += $orderInfo->extra_payment;
                      }
                  ?>
                  {{ $totalExtraAmount }}
                </td>
                <td class="amount-font">
                  <span style="font-size:12px">NZ $ &nbsp;</span>
                  <?php
                  $ActualAmount = 0;
                  if($order->total_actual_amount <= 0){
                    $ActualAmount = $totalExtraAmount + $order->total_amount;
                  }else {
                    $ActualAmount = $order->total_actual_amount;
                  }
                  ?>
                  {{ $ActualAmount }}

                </td>
                <td class="amount-font">
                  <span style="font-size:12px">NZ $ &nbsp;</span>
                  @if($isDelivery)
                {{(float) $ActualAmount + (float) \App\Miscellaneous::first()->deliveryCharges}}
                  @else
                    {{(float) $ActualAmount + (float) 0}}

                  @endif
                </td>




                <td style="font-weight:600">
                  {{ $order->created_at->format('l d, F') }}
                </td>
              </tr>
              <tr>
                <td colspan="9">
                  <ul class="collapsible white"  >
                      <li>
                        <div class="collapsible-header" style="font-weight:bold;background-color:{{$backgroundColor}};color:white;border:none">Click To View Details</div>
                        <div class="collapsible-body white">
                          <table class="table-responsive bordered">
                            <thead>
                              <th>Product Name</th>
                              <th>Product Price</th>
                              <th>Service Options</th>
                              <th>Quantity</th>
                              <th>Avg. Weight</th>
                              <th>Estd. Amount</th>
                              <th>Extra Quantity</th>
                              <th>Extra Payment</th>
                              <th>Act. Amount</th>
                            </thead>
                            <tbody>
                              @foreach (\App\OrderInfo::where('order_id','=',$order->id)->get() as $oi)
                                <tr>

                                  <td style="font-weight:600">{{ $oi->Product()->name }}</td>
                                  <?php $sellingUnit = (strtolower($oi->Product()->sellingUnit->name) == "piece" || strtolower($oi->Product()->sellingUnit->name) == "pieces") ? true : false;   ?>
                                  <td style="font-weight:600">$ {{ $oi->Product()->price }} / <b>{{  $sellingUnit ? 'Kg' : $oi->Product()->sellingUnit->name }}</b></td>
                                    <td style="font-weight:600">
                                    @if( is_countable(json_decode($oi->service_type_option)) && count(json_decode($oi->service_type_option)) <= 0)

                                        <i style="font-size:12px">{{ 'No Service Requested' }}</i>

                                      @else

                                        <ol>
                                          @if(is_countable(json_decode($oi->service_type_option)))
                                          @foreach (json_decode($oi->service_type_option) as  $serviceOptionItem)
                                            <li>
                                              {{ \App\ServiceType::where('id','=',$serviceOptionItem)->first()->name  }}</b> </sup>
                                            </li>
                                          @endforeach
                                          @endif
                                        </ol>
                                    @endif
                                  </td>


                                  <td style="font-weight:600" id="table-quantity-{{$oi->id}}">
                                      {{ $oi->quantity }}  <b>{{ $oi->Product()->sellingUnit->name }}</b>
                                  </td>

                                  <td style="font-weight:600">
                                    {!! is_null($oi->Product()->avg_weight) ? "<i>N/A</i>" : $oi->Product()->avg_weight.'<sup>Kg</sup>' !!}
                                  </td>

                                  {{-- ESTIMATED AMOUNT --}}

                                  <td style="font-weight:600" class="product-subtotal" id="table-subtotal-{{$oi->id}}">
                                    NZ $ {{$oi->amount}}
                                  </td>


                                  <td style="font-weight:600">

                                      {{ (float)$oi->extra_quantity}}
                                 </td>

                                  {{-- EXTRA AMOUNT --}}

                                  <td style="font-weight:600" class="extra-payments-column" id="extra_payment_{{$oi->id}}">
                                    NZ $ {{(float)$oi->extra_payment}}
                                  </td>

                                  {{-- ACTUAL AMOUNT --}}

                                <td style="font-weight:600" class="actual-payments-column"  id="actual_amuount_column_{{$oi->id}}">{{ (float) $oi->amount + (float) $oi->extra_payment }}</td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                      </li>
                  </ul>
                </td>
              </tr>
            @endif
            @endforeach
      </tbody>
    </table>

  </div>
</div>

 {{-- DELIVERED ORDERS --}}

<div id="delivered-modal" class="modal">
  <div class="modal-content">

    <center><h4 style="font-size:16px;font-weight:bold">Delivered Orders Details</h4></center>

    <table class="bordered responsive-table">
      <thead>
        <tr>
          <th>  #
          </th>
          <th>
            Status
          </th>
          <th>
            Shipment
          </th>
          <th>Estd. Amount</th>
          <th>Extra Amount</th>
          <th>
            Actual Amount
          </th>
          <th style="text-align:center">Total Amount <br> + <br> Shipment</th>
          <th>
            Placed On
          </th>
        </tr>
      </thead>
      <tbody>

            <?php $i=0; ?>
            @foreach ($orders as $order)
              @if($order->order_status == 1)
              <?php $i++; ?>
              <?php $amount = 0; ?>
              <tr>
                <td class="font-weight-medium">
                  {{ $i }}
                </td>
                <td>
                  <i>{!! $order->order_status == 0 ? '<i style="font-weight:600;color:orange">Pending</i>':'<i style="font-weight:600;color:green">Completed</i>' !!}</i>
                </td>
                <td>
                  @php $isDelivery = false; @endphp
                  @if($order->isDelivery == 0)
                    <span > <i style="font-weight:600" class="indigo-text">Self</i> </span>
                  @else
                  @php $isDelivery = true @endphp
                    <span> <i style="font-weight:600" class="blue-text">Delivery</i> </span>
                  @endif
                </td>


                <td class="amount-font">
                  <span style="font-size:12px">NZ $ &nbsp;</span>
                  {{$order->total_amount}}
                </td>
                <td class="amount-font">
                  <span style="font-size:12px">NZ $ &nbsp;</span>
                  <?php $totalExtraAmount = 0;
                      foreach ($order->OrderInfo() as $orderInfo) {
                        $totalExtraAmount += $orderInfo->extra_payment;
                      }
                  ?>
                  {{ $totalExtraAmount }}
                </td>
                <td class="amount-font">
                  <span style="font-size:12px">NZ $ &nbsp;</span>
                  <?php
                  $ActualAmount = 0;
                  if($order->total_actual_amount <= 0){
                    $ActualAmount = $totalExtraAmount + $order->total_amount;
                  }else {
                    $ActualAmount = $order->total_actual_amount;
                  }
                  ?>
                  {{ $ActualAmount }}

                </td>
                <td class="amount-font">
                  <span style="font-size:12px">NZ $ &nbsp;</span>
                  @if($isDelivery)
                {{(float) $ActualAmount + (float) \App\Miscellaneous::first()->deliveryCharges}}
                  @else
                    {{(float) $ActualAmount + (float) 0}}

                  @endif
                </td>




                <td style="font-weight:600">
                  {{ $order->created_at->format('l d, F') }}
                </td>
              </tr>
              <tr>
                <td colspan="9">
                  <ul class="collapsible white"  >
                      <li>
                        <div class="collapsible-header" style="font-weight:bold;background-color:{{$backgroundColor}};color:white;border:none">Click To View Details</div>
                        <div class="collapsible-body white">
                          <table class="table-responsive bordered">
                            <thead>
                              <th>Product Name</th>
                              <th>Product Price</th>
                              <th>Service Options</th>
                              <th>Quantity</th>
                              <th>Avg. Weight</th>
                              <th>Estd. Amount</th>
                              <th>Extra Quantity</th>
                              <th>Extra Payment</th>
                              <th>Act. Amount</th>
                            </thead>
                            <tbody>
                              @foreach (\App\OrderInfo::where('order_id','=',$order->id)->get() as $oi)
                                <tr>

                                  <td style="font-weight:600">{{ $oi->Product()->name }}</td>
                                  <?php $sellingUnit = (strtolower($oi->Product()->sellingUnit->name) == "piece" || strtolower($oi->Product()->sellingUnit->name) == "pieces") ? true : false;   ?>
                                  <td style="font-weight:600">$ {{ $oi->Product()->price }} / <b>{{  $sellingUnit ? 'Kg' : $oi->Product()->sellingUnit->name }}</b></td>
                                    <td style="font-weight:600">
                                    @if( is_countable(json_decode($oi->service_type_option)) && count(json_decode($oi->service_type_option)) <= 0)

                                        <i style="font-size:12px">{{ 'No Service Requested' }}</i>

                                      @else
                                        <ol>
                                            @if(is_countable(json_decode($oi->service_type_option)))
                                            @foreach (json_decode($oi->service_type_option) as  $serviceOptionItem)
                                              <li>
                                                {{ \App\ServiceType::where('id','=',$serviceOptionItem)->first()->name  }}</b> </sup>
                                              </li>
                                            @endforeach
                                            @endif
                                          </ol>
                                    @endif
                                  </td>


                                  <td style="font-weight:600" id="table-quantity-{{$oi->id}}">
                                      {{ $oi->quantity }}  <b>{{ $oi->Product()->sellingUnit->name }}</b>
                                  </td>

                                  <td style="font-weight:600">
                                    {!! is_null($oi->Product()->avg_weight) ? "<i>N/A</i>" : $oi->Product()->avg_weight.'<sup>Kg</sup>' !!}
                                  </td>

                                  {{-- ESTIMATED AMOUNT --}}

                                  <td style="font-weight:600" class="product-subtotal" id="table-subtotal-{{$oi->id}}">
                                    NZ $ {{$oi->amount}}
                                  </td>


                                  <td style="font-weight:600">

                                      {{ (float)$oi->extra_quantity}}
                                 </td>

                                  {{-- EXTRA AMOUNT --}}

                                  <td style="font-weight:600" class="extra-payments-column" id="extra_payment_{{$oi->id}}">
                                    NZ $ {{(float)$oi->extra_payment}}
                                  </td>

                                  {{-- ACTUAL AMOUNT --}}

                                <td style="font-weight:600" class="actual-payments-column"  id="actual_amuount_column_{{$oi->id}}">{{ (float) $oi->amount + (float) $oi->extra_payment }}</td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                      </li>
                  </ul>
                </td>
              </tr>
            @endif
            @endforeach
      </tbody>
    </table>

  </div>
</div>




<script type="text/javascript">
let clipboard =   new ClipboardJS('#trigger-copy')

clipboard.on('success',function(e){
  let tip = tippy('#trigger-copy',{
    content:'Copied!',
    arrow:true,
    arrowType:'round',
    size:'large',
    duration:'100',
    animation:'shift-away'
  })
})
window.onload = function(){
  $('.modal').modal()

}
</script>





<style media="screen">
.active:{
  background-color:transparent;
}

</style>






@endsection
