<?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>

<style media="screen">

.news{
  display: block;
  position: fixed;
  top:5%;
  height: 40px;
  line-height: 40px;
  z-index:1000;
  width:100%;
  background-color:{{$backgroundColor}};
  font-size:18px;
  color:white;
  font-weight: bold;
  padding-top:5px;
  font-family: 'Raleway';
  text-align:center;
  text-shadow: 1px 1px 1px rgba(0,0,0,0.75);
  letter-spacing: 2px;
}
.news b {
  padding:none;
}
@media only screen and (max-width: 768px)
{
  .news{
    font-family: 'Raleway';
    font-size:10px;
    padding-top:0px;
    color:white;
    font-weight: bold;
    top:92.5%;
    letter-spacing: 1px;

  }
}

</style>
<p class="news"><b style="color:white;color:white ">Premium Meats</b> at your door steps, order online </p>
