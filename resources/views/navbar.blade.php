<?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>

    <?php $admin_logo_img = Voyager::setting('site.logo'); ?>
<style media="screen">

#sidenav-overlay {
z-index: -1 !important;
}
.nav-wrapper{
    padding:0px 24px 42px 24px !important;
    background-color: white !important;
}

nav ul a:hover {
    background-color: rgba(0, 0, 0, 0) !important;
}

#dadas:hover
{
  background-color: #c21e56 !important;
}
.hover-custom
{
  font-weight: 600;
}
  .hover-custom:hover
  {
    text-shadow:0px 0px 10px #c21e56;
  }
  #dadas:hover
  {
    background-color: #c21e56 !important;
  }
  #dadas2:hover
  {
    background-color: #c21e56 !important;
  }
</style>
<div class="navbar-fixed" style="z-index:100000000!important;position:fixed;top:0px;" >

        {{-- navbar --}}
     <nav>
    <div class="nav-wrapper">
      <a href="/" class="brand-logo left">
{{-- <div id="logo-main" class="navbar-logo hide-on-med-and-down z-depth-2"></div><div class="hide-on-large-only z-depth-2" id="responsive-logo"></div>  --}}
      <img src="{{ Voyager::image($admin_logo_img) }}" style="width:auto;height:57px" alt="">

      </a>
      <a href="#" type="button" id="left-dots" data-activates="slide-out" class="button-collapse btn right btn-floating  hide-on-large-only" style="line-height:70px !important;width:60px;height:60px;background-color:{{ $backgroundColor }};margin-right:-7px"><i style="font-size:28px;" class="fa fa-bars white-text"></i> </a>


      <ul id="nav-mobile" class="right hide-on-med-and-down" style="background-color:transparent;border-radius:25px;margin-top:-5px;margin-bottom:10px">
        {{-- <li class="badges"><a class="black-text hover-custom" style="margin-top:10%"  href="https://apps.apple.com/pk/app/premium-meat/id1490414041"><img src="https://s1.ticketm.net/en-us/img/static/mobile/img-appstore@3x.png"  style="height:auto;width:100px"></a></li> --}} --}}

        <li><a href="{{URL::to('/#carousel')}}" class="black-text hover-custom "  style="border-radius:25px;" >Home</a></li>
        <li><a href="{{URL::to('/#products')}}" class="black-text hover-custom "  style="border-radius:25px;" >Products</a></li>
        <li><a href="{{URL::to('/#footer')}}" class="black-text hover-custom "  style="border-radius:25px;" >About Us</a></li>
        <li><a href="{{URL::to('/#footer-last')}}" class="black-text hover-custom "  style="border-radius:25px;" >Contact Us</a></li>
        @if(Auth::guest())

          <li><a href="/register" class="btn white-text  waves-effect waves-light z-depth-3" id="dadas"  style="background-color:{{ $backgroundColor }};border-radius:25px;font-weight:bold;text-transform:none" >Sign Up</a></li>
          <li ><a class=" btn black-text white z-depth-3" style="border-radius:25px;font-weight:bold;text-transform:none"  href="/login">Login</a></li>

        @else
          <li ><a class="btn white-text pink waves-effect waves-light z-depth-3" id="dadas2"  style="border-radius:25px;background-color: {{ $backgroundColor }} !important;font-weight:bold;text-transform:none"  href="/logout">{{ ucfirst("Logout") }}</a></li>
          <li ><a class=" btn  white black-text z-depth-3" style="border-radius:25px;font-weight:bold;text-transform:none"  href="{{ route('user.getUserManager') }}">{{ ucfirst("Profile") }}</a></li>

        @endif
        <li><a href="{{ route('checkout') }}" id="cart-icon-btn" class="btn btn-floating z-depth-2 black-text white"> <i style="line-height:42px;font-size:18px" class="fa fa-shopping-cart black-text"></i></a>
        <a  class="z-depth-2 cart-counter white btn btn-floating" onclick="document.querySelector('#cart-icon-btn').click()"  style="background-color:{{ $backgroundColor }}   !important;display:inline-block;width:25px;position:absolute;z-index:1000;top:57% !important;right:2% !important;height:25px;line-height:25px;color:#fff !important">{{ count(session()->get('cart')) }}</a>

      </li>
      </ul>
      <ul  class="right show-on-med-and-down hide-on-large-only" style="background-color:transparent;border-radius:25px;margin-top:-5px;margin-bottom:10px">
  
        <li><a style="margin-right:-60px" href="{{ route('checkout') }}" id="cart-icon-btn" class="btn btn-floating z-depth-2 black-text white"> <i style="line-height:42px;font-size:18px;" class="fa fa-shopping-cart black-text"></i></a>
        <a  class="z-depth-2 cart-counter white btn btn-floating" onclick="document.querySelector('#cart-icon-btn').click()"  style="background-color:{{ $backgroundColor }}   !important;display:inline-block;width:25px;position:relative;z-index:1000;top:10px !important;left:45%;height:25px;line-height:25px;color:#fff !important">{{ count(session()->get('cart')) }}</a>
      </li>
      </ul>

      <ul class="side-nav" id="slide-out">
        <li>
          <center>
            <div class="navbar-logo z-depth-2" style="width:150px;height:150px"></div>
          </center>
        </li>
        <li  class="divider"></li>
        <li ><a class="btn white login z-depth-2 sidebar-item " style="text-align:left;color:{{ $backgroundColor }} !important;border-radius:10px"    href="/#carousel">Home</a></li>
        <li ><a class="btn white login z-depth-2 sidebar-item" style="text-align:left;color:{{ $backgroundColor }} !important;border-radius:10px"    href="/#products">Product</a></li>
        <li ><a class="btn white login z-depth-2 sidebar-item" style="text-align:left;color:{{ $backgroundColor }} !important;border-radius:10px"    href="/#footer">About Us</a></li>
        <li ><a class="btn white login z-depth-2 sidebar-item" style="text-align:left;color:{{ $backgroundColor }} !important;border-radius:10px"    href="/#footer-last">Contact Us</a></li>
        @if(Auth::guest())
          <li ><a class="btn white z-depth-2 sidebar-item" style="text-align:left;color:{{ $backgroundColor }} !important;border-radius:10px"   href="/register">Sign Up</a></li>
          <li ><a class="btn white login z-depth-2 sidebar-item" style="text-align:left;color:{{ $backgroundColor }} !important;border-radius:10px"    href="/login">Login</a></li>
        @else
          <li ><a class="btn white login z-depth-2 sidebar-item" style="text-align:left;color:{{ $backgroundColor }} !important;border-radius:10px;text-transform:none"    href="{{ route('user.getUserManager') }}">Profile</a></li>
          <li ><a class="btn white login z-depth-2 sidebar-item" style="text-align:left;color:{{ $backgroundColor }} !important;border-radius:10px;text-transform:none"    href="/logout">Logout</a></li>
        @endif
        <li><a class="btn white sidebar-item" style="text-align:left"   href="{{route('checkout')}}"> <span>Cart</span>
          <div class="right">
            <button href="" class="cart-counter z-depth-2 btn-floating white btn btn-white right" style="border-radius:10px;margin:0px;background-color:{{ $backgroundColor }} !important;;display:inline-block;width:25px;position:relative;z-index:1000;top:20% !important;right:0%;left:0% !important;height:25px;line-height:25px;color:#fff !important">{{ count(session()->get('cart')) }} </button>
            <i style="color:{{ $backgroundColor }}" class="fa fa-shopping-cart right"></i>
          </div>
        </a>
      </li>
      </ul>
      {{-- <ul class="side-nav" id="slide-left">
        <li>
          <center>
            <div class="navbar-logo z-depth-2" style="width:150px;height:150px"></div>
          </center>
        </li>
        <li  class="divider"></li>
        <li ><a class="btn white pink-text" onclick="$('#right-dots').sideNav('hide')" href="#carousel"  data-name="1"> <i class="fa fa-clone fa-fw"></i> Home</a></li>
        <li ><a class="btn white pink-text" onclick="$('#right-dots').sideNav('hide')" href="#products" data-name="1"> <i class="fa fa-cubes fa-fw"></i> Products</a></li>
        <li ><a class="btn white pink-text" onclick="$('#right-dots').sideNav('hide')" href="#additional-products"  data-name="1"> <i class="fa fa-line-chart fa-fw"></i> Special</a></li>
        <li ><a class="btn white pink-text" onclick="$('#right-dots').sideNav('hide')" href="#sausages"  data-name="1"> <i class="fa fa-cutlery fa-fw"></i> Sausages</a></li>
        <li ><a class="btn white pink-text" onclick="$('#right-dots').sideNav('hide')" href="#steak" data-name="1"> <i class="fa fa-sign-language fa-fw"></i> Steak</a></li>
        <li ><a class="btn white pink-text" onclick="$('#right-dots').sideNav('hide')" href="#footer"  data-name="1"> <i class="fa fa-graduation-cap fa-fw"></i> About Us</a></li>
        <li ><a class="btn white pink-text" onclick="$('#right-dots').sideNav('hide')" href="#footer-last" data-name="1"> <i class="fa fa-microphone fa-fw"></i> Contact Us</a></li>
      </ul> --}}
    </div>


  </nav>
</div>
