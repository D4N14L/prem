<?php $admin_logo_img = Voyager::setting('site.logo');
			$admin_logo_img = '/storage/'.$admin_logo_img;
?>

<!DOCTYPE html>
<html>
<head>
	<title>Order Report Generation </title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
  <?php $admin_logo_img = Voyager::setting('site.logo'); ?>
  <style media="screen">
    body,html{
			font-family: 'Raleway', sans-serif !important;
      font-size:14px !important;
      color:black !important;
    }
    table{
      border-collapse:collapse;
    }
		tr,th,td{
			font-family: 'Raleway', sans-serif !important;
      text-align: center;
      border:1px solid #aaa !important;
      padding:12px 24px;
		}

  </style>
</head>
<body>
<div class="container">
	<img src="{{Voyager::image($admin_logo_img)}}" style="width:auto;height:40px;" alt="Premium Meat Logo" /> <br/>
  <h4>Dear {{$user->name}}</h4> <br>

  {!! $body !!}

  <br/>  <br/>

  Regards,<br/>
  <b>Premium Meat</b>
  <br>
   <ul>
        <li>
           <b>Address</b>  5/64 Stoddard Road, Mt. Roskill, Auckland, New Zealand.
        </li>
        <li>
          <b>Phone</b>  (+649) 629-6209
        </li>
        <li>
         <b>Email</b>   info@premiummeat.co.nz
        </li>
    </ul>

  </body>
</html>
