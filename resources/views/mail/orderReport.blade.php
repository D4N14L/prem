<?php $admin_logo_img = Voyager::setting('site.logo');
			$admin_logo_img = '/storage/'.$admin_logo_img;
			$customerSubrub = $customer->suburbName;
?>

<!DOCTYPE html>
<html>
<head>
	<title>Order Report Generation </title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
  <style media="screen">
    body,html{
			font-family: 'Raleway', sans-serif !important;
      font-size:14px !important;
      color:black !important;
    }
    table{
      border-collapse:collapse;
    }
		tr,th,td{
			font-family: 'Raleway', sans-serif !important;
      text-align: center;
      border:1px solid #aaa !important;
      padding:12px 24px;
		}

  </style>
</head>
<body>
<div class="container">
	<img src="{{Voyager::image($admin_logo_img)}}" style="width:auto;height:40px;" alt="Premium Meat Logo" /> </br>

  <?php $i=0; $product_quantities = [];$ei=0;$totalActualAmount = 0 ?>

  <h4>Customer Information</h4>
  <hr>
  <table class="table table-bordered table-striped">
      <tr>
        <th>Name</th>
        <th>Email</th>
        <th>Landline</th>
        <th>Address</th>
        <th>Order Date</th>
      </tr>
      <tr>
        <th>{{$customer->name}}</th>
        <th>{{$customer->email}}</th>
        <th>{{$customer->landline}}</th>
				<?php
						$customerSubrub = !(is_null($customerSubrub)) ? $customerSubrub->suburbName : "No Suburb Provided";
				 ?>

        <th style="font-size:10px;font-weight:500;"><i>{{ $customerSubrub }}</i> , {{$customer->address}} </th>
        <th>{{ \App\Order::where('id',$order_id)->first()->created_at->format('d/M/Y') }}</th>
			</tr>


	</table>
  <h4>Order Information</h4>
	<hr>
  <table class="table table-responsive table-striped table-bordered">
    <tr>
      <th>Product Name</th>
      <th>Product Price</th>
      <th>Service Options</th>
      <th>Quantity</th>
      <th>Average Weight</th>
      <th>Estd. Amount</th>
      <th>Extra Quantity</th>
      <th>Extra Pay ($ NZ)</th>
      <th>Act. Amount</th>
    </tr>
      <?php $OrderInfo = \App\OrderInfo::where('order_id','=',$order_id)->get(); ?>
      @foreach ($OrderInfo as $oi)
        <tr>
          <td>{{ $oi->Product()->name }}</td>
          <td>$ {{ $oi->Product()->price }} / <b>{{ strtolower($oi->Product()->sellingUnit->name) == "pieces" || strtolower($oi->Product()->sellingUnit->name) == "piece" ? "Kg" : $oi->Product()->sellingUnit->name   }}</b></td>
          <td>
            @if($oi->service_type_option == '0' || (is_countable($oi->service_type_option) && count(json_decode($oi->service_type_option)) <= 0))
              <i style="font-size:12px"> {{ 'No Service Requested' }} </i>
            @else
              <ol>
                @foreach (json_decode($oi->service_type_option) as  $serviceOptionItem)
                  <li>
                    {{ \App\ServiceType::where('id','=',$serviceOptionItem)->first()->name  }}</b> </sup>
                  </li>
                @endforeach
              </ol>
          @endif
          </td>


          <td id="table-quantity-{{$oi->id}}">
              {{ $oi->quantity }}  <b>{{ $oi->Product()->sellingUnit->name }}</b>
          </td>

          <td>
            {!! is_null($oi->Product()->avg_weight) ? "<i>N/A</i>" : $oi->Product()->avg_weight.'<sup> &nbsp; Kg</sup>' !!}
          </td>

          {{-- ESTIMATED AMOUNT --}}

          <td class="product-subtotal" id="table-subtotal-{{$oi->id}}">
            {{$oi->amount}}
          </td>


          <td>
            {{ (float)$oi->extra_quantity }}
          </td>

          {{-- EXTRA AMOUNT --}}

          <td class="extra-payments-column" id="extra_payment_{{$oi->id}}">
             {{(float)$oi->extra_payment}}
          </td>

          {{-- ACTUAL AMOUNT --}}

        <td class="actual-payments-column"  id="actual_amuount_column_{{$oi->id}}">{{ (float) $oi->amount + (float) $oi->extra_payment }}</td>
        </tr>

        <?php $i += round(((float) $oi->Product()->price * ((float) $oi->quantity + (float) $oi->extra_quantity)),2);?>
        <?php $ei += round(((float) $oi->extra_payment ),2);?>
        <?php $totalActualAmount += (float) $oi->amount + (float) $oi->extra_payment;  ?>

      @endforeach
      <tr >
        <th colspan="7"></th>
        <td  style="text-align:left" > <b>NZ $ <span id="total-extra-price-holder"><?php echo $ei; ?></span>  </b> </td>
        <td style="text-align:left" > <b>NZ $ <span id="total-actual-price-holder"><?php echo round($totalActualAmount,2); ?></span>  </b> </td>
      </tr>
  </table>

  <table class="table table-responsive table-striped table-bordered">
    <tr>
      <td style="text-align:center" >Delivery Charges</td >
      <td style="text-align:center" >Total Amount Charged</td >
    </tr>
      <tr >
        <?php
          $isDelivery = 0;
          $isDelivery = \App\Order::where('id','=',$OrderInfo->first()->order_id)->first()->isDelivery;
          $deliveryCharges = 0;
          if($isDelivery == 1){
            $deliveryCharges= (float) \App\Miscellaneous::first()->deliveryCharges;
          }
         ?>
        <td  style="text-align:center" > <b> NZ $ {{ $deliveryCharges }} </b> </td>
        <td   style="text-align:center" > <b>NZ $ <span id="amount_to_be_paid"> {{ (float)($deliveryCharges + $totalActualAmount)}}</span> </b> </td>
      </tr>
  </table>
  <center> <a href="https://www.premiummeat.co.nz">https://www.premiummeat.co.nz</a></center>
</div>
</body>
</html>
