<?php $admin_logo_img = Voyager::setting('site.logo');
			$admin_logo_img = '/storage/'.$admin_logo_img;
?>

<!DOCTYPE html>
<html>
<head>
	<title>Order Report Generation </title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
  <?php $admin_logo_img = Voyager::setting('site.logo'); ?>
  <style media="screen">
    body,html{
			font-family: 'Raleway', sans-serif !important;
      font-size:14px !important;
      color:black !important;
    }
    table{
      border-collapse:collapse;
    }
		tr,th,td{
			font-family: 'Raleway', sans-serif !important;
      text-align: center;
      border:1px solid #aaa !important;
      padding:12px 24px;
		}

  </style>
</head>
<body>
<div class="container">
  <?php $i=0; $product_quantities = [];$ei=0;$totalActualAmount = 0 ?>
	<img src="{{Voyager::image($admin_logo_img)}}" style="width:auto;height:40px;" alt="Premium Meat Logo" />
  <p style="text-align:left;border:none;">
		Dear <strong>{{ $user->name }}</strong>, <br>
		@if($is_delivery)
	    You have successfully been charged and your order has also been  dispatched. </br>
	    It should be delivered to you today anytime before 5.30pm. Please be assured someone is present at the provided delivery address to receive the delivery. <br><br>
	     <br> In the event of failure of any receiver at the given address, customer has to arrange a pick up within 24 hours. After 24 hours company will hold no responsibilty for that particular order. 
       <br> Please find the attached invoice of your orders. 
       <br> Thanks
       @else
			You have successfully been charged and your order is ready to be collected from our retail store now. </br>
			Please also find the attached invoice. Thanks

		@endif
  </p>
	Regards, <br>
	Premium Meat
  <h4>Reach Us At</h4>
  <hr>
  <ul>
    <li>
       <b>Address</b>  5/64 Stoddard Road, Mt. Roskill, Auckland, New Zealand.
    </li>
    <li>
      <b>Phone</b>  (+649) 629-6209
    </li>
    <li>
     <b>Email</b>   info@premiummeat.co.nz
    </li>
</ul>

  </body>
</html>
