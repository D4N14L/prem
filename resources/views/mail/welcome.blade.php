<?php $admin_logo_img = Voyager::setting('site.logo'); ?>


<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <meta charset="utf-8">
    <title></title>
    <style media="screen">
      body,html{
        padding:0;
        margin:0;
        font-family: 'Raleway', sans-serif;
        color:white;
        background-image:url('{{URL::to('/')}}/assets/email-back.svg');
        background-size:cover;
        background-repeat: no-repeat;
          height: 100vh;
          overflow: hidden;;
      }
    </style>

  </head>
  <body>
    <div style="padding:0% 25% 0% 25%;">
    <center>
      <img src="{{Voyager::image($admin_logo_img)}}" style="width:150px;height:150px;object-fit:contain"  >
    </center>
      <p class="flow-text" style="font-size:18px">
        Warm Welcome <b>{{ $user->name }}</b>, <br><br> Thanks for registering with us. <br><br> Premium Meats we are as good as it sounds. Premium Meats is one of fastest growing retail and wholesale meat supplier in Auckland because we offer the service and quality that has never been experienced.
      </p><br><br>
      <center>
      <p style="font-size:18px;font-weight:100" class="flow-text pink-text" >
        Please Verify Your Account, By Clicking the button below
      </p><br>
      <br>
      <a href="{{ route('email.verify',['id' => $user->verification_token]) }}" style="padding:12px 24px;text-decoration:none;background-color:orange;color:white;border:none">Verify Account</a>
      <br>
      </center>


    </div>

  </body>
</html>
