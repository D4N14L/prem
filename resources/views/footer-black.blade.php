<?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>

<div class="section footer-last"  data-anchor="footer-last">

  {{-- <div class="premium-logo"></div> --}}<br><br><br><br>


  <div class="row footer-last-grid" >
    <div id="service-area-footer" >
      <p style="color:white">Auckland Service Areas</p>
      <div style="max-height:200px">
        <ul>
          @foreach (\App\Suburb::orderBy('suburbName','asc')->limit(8)->get() as $suburb)
            <li style="color:white;font-size:14px;width:100%">{{$suburb->suburbName}}</li>

          @endforeach
          @if(\App\Suburb::all()->count() > 8)
            <li style="color:white;font-size:14px;width:100%">
              <a data-target="modal1"  class="modal-trigger" style="color:white;text-decoration:underline;background-color:{{$backgroundColor}};font-weight:bold;border-radius:25px;text-shadow:1px 1px 1px #000;font-size:16px" id="show-more">Show More</a>
            </li>
          @endif
        </ul>
      </div>
    </div>



    <div  id="footer-logo">
      <center>      <div  class="navbar-logo" style="height:100px;"></div></center>
    </div>



    <div >
      <div >
            <ul class="white-text center" style="text-align:left;font-size:14px;">
                    <li>
                         <i class="fa fa-building fa-fw"></i>{{ \App\FooterCMS::where('id','=','1')->first()->address }}</li>
                    <li>
                         <i class="fa fa-phone fa-fw"></i>{{ \App\FooterCMS::where('id','=','1')->first()->phone }}</li>
                    <li>
                       <i class="fa fa-fax fa-fw"></i>{{ \App\FooterCMS::where('id','=','1')->first()->landline }}</li>
                    <li>
                        <i class="fa fa-envelope fa-fw"></i>{{ \App\FooterCMS::where('id','=','1')->first()->email }}</li>
                </ul>

      </div>
    </div>
  </div> <br><br> <br>
        <div  class="opx">
                <div class="row">
                        <div class="col s12 m12 l12">
                        <p style="color:white;text-align:center;font-size:14px;width:100%;" >
                              {{ \App\FooterCMS::where('id','=','1')->first()->description }}
                        </p>
                        </div>


                    </div>
        </div>




    {{-- </div> --}}

    
