<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraPaymentColumnToTableOrdersInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_info', function (Blueprint $table) {
            $table->float('extra_payment')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      if(Schema::hasColumn('orders_info','extra_payment')){
        Schema::table('table_orders_info', function (Blueprint $table) {
          $table->dropColumn('extra_payment');
        });
      }

    }
}
