<?php

use Illuminate\Database\Seeder;
//use DB;
class FrontendCMSTablerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('frontend_cms')->insert([
          'backgroundColor' => '#A61531'
        ]);
    }
}
